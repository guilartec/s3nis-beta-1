﻿namespace S3NIS_BETA
{
    partial class S3NIS_BETA
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(S3NIS_BETA));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.cancelar = new System.Windows.Forms.Button();
            this.aceptar = new System.Windows.Forms.Button();
            this.tabControl_ls_sensor = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label49 = new System.Windows.Forms.Label();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.Separacion = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.label2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lsQ1_3S = new System.Windows.Forms.RadioButton();
            this.lsQ1_3N = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lsQ1_2S = new System.Windows.Forms.RadioButton();
            this.lsQ1_2N = new System.Windows.Forms.RadioButton();
            this.labelC_lsQ1_3 = new System.Windows.Forms.Label();
            this.labelC_lsQ1_2 = new System.Windows.Forms.Label();
            this.lsQ1_3C = new System.Windows.Forms.TextBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.lsQ1_2C = new System.Windows.Forms.TextBox();
            this.labelC_lsQ1_1 = new System.Windows.Forms.Label();
            this.lsQ1_1C = new System.Windows.Forms.TextBox();
            this.labelN_Q1_3 = new System.Windows.Forms.Label();
            this.label_Q1_2 = new System.Windows.Forms.Label();
            this.labelN_Q1_2 = new System.Windows.Forms.Label();
            this.label_Q1_1 = new System.Windows.Forms.Label();
            this.labelN_Q1_1 = new System.Windows.Forms.Label();
            this.label_Q1_3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lsQ1_1S = new System.Windows.Forms.RadioButton();
            this.lsQ1_1N = new System.Windows.Forms.RadioButton();
            this.Redundancia = new System.Windows.Forms.TabPage();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lsQ2_7C = new System.Windows.Forms.TextBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.lsQ2_7N = new System.Windows.Forms.RadioButton();
            this.lsQ2_7S = new System.Windows.Forms.RadioButton();
            this.label27 = new System.Windows.Forms.Label();
            this.labelC_lsQ2_7 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.lsQ2_6C = new System.Windows.Forms.TextBox();
            this.labelC_lsQ2_6 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.lsQ2_6N = new System.Windows.Forms.RadioButton();
            this.lsQ2_6S = new System.Windows.Forms.RadioButton();
            this.label23 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.lsQ2_5N = new System.Windows.Forms.RadioButton();
            this.lsQ2_5S = new System.Windows.Forms.RadioButton();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.lsQ2_4N = new System.Windows.Forms.RadioButton();
            this.lsQ2_4S = new System.Windows.Forms.RadioButton();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.lsQ2_3S = new System.Windows.Forms.RadioButton();
            this.lsQ2_3N = new System.Windows.Forms.RadioButton();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.lsQ2_2S = new System.Windows.Forms.RadioButton();
            this.lsQ2_2N = new System.Windows.Forms.RadioButton();
            this.labelC_lsQ2_5 = new System.Windows.Forms.Label();
            this.labelC_lsQ2_4 = new System.Windows.Forms.Label();
            this.labelC_lsQ2_3 = new System.Windows.Forms.Label();
            this.labelC_lsQ2_2 = new System.Windows.Forms.Label();
            this.lsQ2_5C = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lsQ2_4C = new System.Windows.Forms.TextBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.lsQ2_3C = new System.Windows.Forms.TextBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.lsQ2_2C = new System.Windows.Forms.TextBox();
            this.labelC_lsQ2_1 = new System.Windows.Forms.Label();
            this.lsQ2_1C = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.lsQ2_1S = new System.Windows.Forms.RadioButton();
            this.lsQ2_1N = new System.Windows.Forms.RadioButton();
            this.Complejidad = new System.Windows.Forms.TabPage();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.label5 = new System.Windows.Forms.Label();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.lsQ3_6C = new System.Windows.Forms.TextBox();
            this.labelC_lsQ3_6 = new System.Windows.Forms.Label();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.lsQ3_6N = new System.Windows.Forms.RadioButton();
            this.lsQ3_6S = new System.Windows.Forms.RadioButton();
            this.label24 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.lsQ3_5N = new System.Windows.Forms.RadioButton();
            this.lsQ3_5S = new System.Windows.Forms.RadioButton();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.lsQ3_4N = new System.Windows.Forms.RadioButton();
            this.lsQ3_4S = new System.Windows.Forms.RadioButton();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.lsQ3_3S = new System.Windows.Forms.RadioButton();
            this.lsQ3_3N = new System.Windows.Forms.RadioButton();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.lsQ3_2S = new System.Windows.Forms.RadioButton();
            this.lsQ3_2N = new System.Windows.Forms.RadioButton();
            this.labelC_lsQ3_5 = new System.Windows.Forms.Label();
            this.labelC_lsQ3_4 = new System.Windows.Forms.Label();
            this.labelC_lsQ3_3 = new System.Windows.Forms.Label();
            this.labelC_lsQ3_2 = new System.Windows.Forms.Label();
            this.lsQ3_5C = new System.Windows.Forms.TextBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.label33 = new System.Windows.Forms.Label();
            this.lsQ3_4C = new System.Windows.Forms.TextBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.lsQ3_3C = new System.Windows.Forms.TextBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.lsQ3_2C = new System.Windows.Forms.TextBox();
            this.labelC_lsQ3_1 = new System.Windows.Forms.Label();
            this.lsQ3_1C = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.lsQ3_1S = new System.Windows.Forms.RadioButton();
            this.lsQ3_1N = new System.Windows.Forms.RadioButton();
            this.Evaluacion = new System.Windows.Forms.TabPage();
            this.splitContainer5 = new System.Windows.Forms.SplitContainer();
            this.label6 = new System.Windows.Forms.Label();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label31 = new System.Windows.Forms.Label();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.lsQ4_3S = new System.Windows.Forms.RadioButton();
            this.lsQ4_3N = new System.Windows.Forms.RadioButton();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.lsQ4_2S = new System.Windows.Forms.RadioButton();
            this.lsQ4_2N = new System.Windows.Forms.RadioButton();
            this.labelC_lsQ4_3 = new System.Windows.Forms.Label();
            this.labelC_lsQ4_2 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.lsQ4_3C = new System.Windows.Forms.TextBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.lsQ4_2C = new System.Windows.Forms.TextBox();
            this.labelC_lsQ4_1 = new System.Windows.Forms.Label();
            this.lsQ4_1C = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.lsQ4_1S = new System.Windows.Forms.RadioButton();
            this.lsQ4_1N = new System.Windows.Forms.RadioButton();
            this.Procedimientos = new System.Windows.Forms.TabPage();
            this.splitContainer6 = new System.Windows.Forms.SplitContainer();
            this.label7 = new System.Windows.Forms.Label();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.lsQ5_7C = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lsQ5_7N = new System.Windows.Forms.RadioButton();
            this.lsQ5_7S = new System.Windows.Forms.RadioButton();
            this.label8 = new System.Windows.Forms.Label();
            this.labelC_lsQ5_7 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.lsQ5_6C = new System.Windows.Forms.TextBox();
            this.labelC_lsQ5_6 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lsQ5_6N = new System.Windows.Forms.RadioButton();
            this.lsQ5_6S = new System.Windows.Forms.RadioButton();
            this.label30 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.lsQ5_5N = new System.Windows.Forms.RadioButton();
            this.lsQ5_5S = new System.Windows.Forms.RadioButton();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.lsQ5_4N = new System.Windows.Forms.RadioButton();
            this.lsQ5_4S = new System.Windows.Forms.RadioButton();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this.lsQ5_3S = new System.Windows.Forms.RadioButton();
            this.lsQ5_3N = new System.Windows.Forms.RadioButton();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this.lsQ5_2S = new System.Windows.Forms.RadioButton();
            this.lsQ5_2N = new System.Windows.Forms.RadioButton();
            this.labelC_lsQ5_5 = new System.Windows.Forms.Label();
            this.labelC_lsQ5_4 = new System.Windows.Forms.Label();
            this.labelC_lsQ5_3 = new System.Windows.Forms.Label();
            this.labelC_lsQ5_2 = new System.Windows.Forms.Label();
            this.lsQ5_5C = new System.Windows.Forms.TextBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.label50 = new System.Windows.Forms.Label();
            this.lsQ5_4C = new System.Windows.Forms.TextBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.lsQ5_3C = new System.Windows.Forms.TextBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.lsQ5_2C = new System.Windows.Forms.TextBox();
            this.labelC_lsQ5_1 = new System.Windows.Forms.Label();
            this.lsQ5_1C = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this.lsQ5_1S = new System.Windows.Forms.RadioButton();
            this.lsQ5_1N = new System.Windows.Forms.RadioButton();
            this.Entrenamiento = new System.Windows.Forms.TabPage();
            this.splitContainer7 = new System.Windows.Forms.SplitContainer();
            this.label10 = new System.Windows.Forms.Label();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.label29 = new System.Windows.Forms.Label();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this.lsQ6_2S = new System.Windows.Forms.RadioButton();
            this.lsQ6_2N = new System.Windows.Forms.RadioButton();
            this.labelC_lsQ6_2 = new System.Windows.Forms.Label();
            this.lsQ6_2C = new System.Windows.Forms.TextBox();
            this.labelC_lsQ6_1 = new System.Windows.Forms.Label();
            this.lsQ6_1C = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.groupBox29 = new System.Windows.Forms.GroupBox();
            this.lsQ6_1S = new System.Windows.Forms.RadioButton();
            this.lsQ6_1N = new System.Windows.Forms.RadioButton();
            this.Ambiente = new System.Windows.Forms.TabPage();
            this.splitContainer15 = new System.Windows.Forms.SplitContainer();
            this.label45 = new System.Windows.Forms.Label();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.label71 = new System.Windows.Forms.Label();
            this.groupBox31 = new System.Windows.Forms.GroupBox();
            this.lsQ7_4N = new System.Windows.Forms.RadioButton();
            this.lsQ7_4S = new System.Windows.Forms.RadioButton();
            this.groupBox32 = new System.Windows.Forms.GroupBox();
            this.lsQ7_3S = new System.Windows.Forms.RadioButton();
            this.lsQ7_3N = new System.Windows.Forms.RadioButton();
            this.groupBox33 = new System.Windows.Forms.GroupBox();
            this.lsQ7_2S = new System.Windows.Forms.RadioButton();
            this.lsQ7_2N = new System.Windows.Forms.RadioButton();
            this.labelC_lsQ7_4 = new System.Windows.Forms.Label();
            this.labelC_lsQ7_3 = new System.Windows.Forms.Label();
            this.labelC_lsQ7_2 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.lsQ7_4C = new System.Windows.Forms.TextBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.lsQ7_3C = new System.Windows.Forms.TextBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.lsQ7_2C = new System.Windows.Forms.TextBox();
            this.labelC_lsQ7_1 = new System.Windows.Forms.Label();
            this.lsQ7_1C = new System.Windows.Forms.TextBox();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.groupBox34 = new System.Windows.Forms.GroupBox();
            this.lsQ7_1S = new System.Windows.Forms.RadioButton();
            this.lsQ7_1N = new System.Windows.Forms.RadioButton();
            this.Datos = new System.Windows.Forms.TabPage();
            this.Resultados = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.splitContainer8 = new System.Windows.Forms.SplitContainer();
            this.label46 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.splitContainer9 = new System.Windows.Forms.SplitContainer();
            this.label48 = new System.Windows.Forms.Label();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.label72 = new System.Windows.Forms.Label();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.sefQ1_3S = new System.Windows.Forms.RadioButton();
            this.sefQ1_3N = new System.Windows.Forms.RadioButton();
            this.groupBox30 = new System.Windows.Forms.GroupBox();
            this.sefQ1_2S = new System.Windows.Forms.RadioButton();
            this.sefQ1_2N = new System.Windows.Forms.RadioButton();
            this.labelC_sefQ1_3 = new System.Windows.Forms.Label();
            this.labelC_sefQ1_2 = new System.Windows.Forms.Label();
            this.sefQ1_3C = new System.Windows.Forms.TextBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.sefQ1_2C = new System.Windows.Forms.TextBox();
            this.labelC_sefQ1_1 = new System.Windows.Forms.Label();
            this.sefQ1_1C = new System.Windows.Forms.TextBox();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.groupBox35 = new System.Windows.Forms.GroupBox();
            this.sefQ1_1S = new System.Windows.Forms.RadioButton();
            this.sefQ1_1N = new System.Windows.Forms.RadioButton();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.splitContainer10 = new System.Windows.Forms.SplitContainer();
            this.label89 = new System.Windows.Forms.Label();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.label96 = new System.Windows.Forms.Label();
            this.groupBox39 = new System.Windows.Forms.GroupBox();
            this.sefQ2_4N = new System.Windows.Forms.RadioButton();
            this.sefQ2_4S = new System.Windows.Forms.RadioButton();
            this.groupBox40 = new System.Windows.Forms.GroupBox();
            this.sefQ2_3S = new System.Windows.Forms.RadioButton();
            this.sefQ2_3N = new System.Windows.Forms.RadioButton();
            this.groupBox41 = new System.Windows.Forms.GroupBox();
            this.sefQ2_2S = new System.Windows.Forms.RadioButton();
            this.sefQ2_2N = new System.Windows.Forms.RadioButton();
            this.labelC_sefQ2_4 = new System.Windows.Forms.Label();
            this.labelC_sefQ2_3 = new System.Windows.Forms.Label();
            this.labelC_sefQ2_2 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.sefQ2_4C = new System.Windows.Forms.TextBox();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.sefQ2_3C = new System.Windows.Forms.TextBox();
            this.checkBox17 = new System.Windows.Forms.CheckBox();
            this.sefQ2_2C = new System.Windows.Forms.TextBox();
            this.labelC_sefQ2_1 = new System.Windows.Forms.Label();
            this.sefQ2_1C = new System.Windows.Forms.TextBox();
            this.label104 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.groupBox42 = new System.Windows.Forms.GroupBox();
            this.sefQ2_1S = new System.Windows.Forms.RadioButton();
            this.sefQ2_1N = new System.Windows.Forms.RadioButton();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.splitContainer11 = new System.Windows.Forms.SplitContainer();
            this.label113 = new System.Windows.Forms.Label();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.label117 = new System.Windows.Forms.Label();
            this.groupBox44 = new System.Windows.Forms.GroupBox();
            this.sefQ3_5N = new System.Windows.Forms.RadioButton();
            this.sefQ3_5S = new System.Windows.Forms.RadioButton();
            this.groupBox45 = new System.Windows.Forms.GroupBox();
            this.sefQ3_4N = new System.Windows.Forms.RadioButton();
            this.sefQ3_4S = new System.Windows.Forms.RadioButton();
            this.groupBox46 = new System.Windows.Forms.GroupBox();
            this.sefQ3_3S = new System.Windows.Forms.RadioButton();
            this.sefQ3_3N = new System.Windows.Forms.RadioButton();
            this.groupBox47 = new System.Windows.Forms.GroupBox();
            this.sefQ3_2S = new System.Windows.Forms.RadioButton();
            this.sefQ3_2N = new System.Windows.Forms.RadioButton();
            this.labelC_sefQ3_5 = new System.Windows.Forms.Label();
            this.labelC_sefQ3_4 = new System.Windows.Forms.Label();
            this.labelC_sefQ3_3 = new System.Windows.Forms.Label();
            this.labelC_sefQ3_2 = new System.Windows.Forms.Label();
            this.sefQ3_5C = new System.Windows.Forms.TextBox();
            this.checkBox18 = new System.Windows.Forms.CheckBox();
            this.label122 = new System.Windows.Forms.Label();
            this.sefQ3_4C = new System.Windows.Forms.TextBox();
            this.checkBox19 = new System.Windows.Forms.CheckBox();
            this.sefQ3_3C = new System.Windows.Forms.TextBox();
            this.checkBox20 = new System.Windows.Forms.CheckBox();
            this.sefQ3_2C = new System.Windows.Forms.TextBox();
            this.labelC_sefQ3_1 = new System.Windows.Forms.Label();
            this.sefQ3_1C = new System.Windows.Forms.TextBox();
            this.label124 = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.label128 = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this.label130 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this.label133 = new System.Windows.Forms.Label();
            this.groupBox48 = new System.Windows.Forms.GroupBox();
            this.sefQ3_1S = new System.Windows.Forms.RadioButton();
            this.sefQ3_1N = new System.Windows.Forms.RadioButton();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.splitContainer12 = new System.Windows.Forms.SplitContainer();
            this.label134 = new System.Windows.Forms.Label();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.label135 = new System.Windows.Forms.Label();
            this.groupBox49 = new System.Windows.Forms.GroupBox();
            this.sefQ4_3S = new System.Windows.Forms.RadioButton();
            this.sefQ4_3N = new System.Windows.Forms.RadioButton();
            this.groupBox50 = new System.Windows.Forms.GroupBox();
            this.sefQ4_2S = new System.Windows.Forms.RadioButton();
            this.sefQ4_2N = new System.Windows.Forms.RadioButton();
            this.labelC_sefQ4_3 = new System.Windows.Forms.Label();
            this.labelC_sefQ4_2 = new System.Windows.Forms.Label();
            this.label138 = new System.Windows.Forms.Label();
            this.sefQ4_3C = new System.Windows.Forms.TextBox();
            this.checkBox21 = new System.Windows.Forms.CheckBox();
            this.sefQ4_2C = new System.Windows.Forms.TextBox();
            this.labelC_sefQ4_1 = new System.Windows.Forms.Label();
            this.sefQ4_1C = new System.Windows.Forms.TextBox();
            this.label140 = new System.Windows.Forms.Label();
            this.label141 = new System.Windows.Forms.Label();
            this.label142 = new System.Windows.Forms.Label();
            this.label143 = new System.Windows.Forms.Label();
            this.label144 = new System.Windows.Forms.Label();
            this.label145 = new System.Windows.Forms.Label();
            this.groupBox51 = new System.Windows.Forms.GroupBox();
            this.sefQ4_1S = new System.Windows.Forms.RadioButton();
            this.sefQ4_1N = new System.Windows.Forms.RadioButton();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.splitContainer13 = new System.Windows.Forms.SplitContainer();
            this.label146 = new System.Windows.Forms.Label();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.label153 = new System.Windows.Forms.Label();
            this.groupBox54 = new System.Windows.Forms.GroupBox();
            this.sefQ5_5N = new System.Windows.Forms.RadioButton();
            this.sefQ5_5S = new System.Windows.Forms.RadioButton();
            this.groupBox55 = new System.Windows.Forms.GroupBox();
            this.sefQ5_4N = new System.Windows.Forms.RadioButton();
            this.sefQ5_4S = new System.Windows.Forms.RadioButton();
            this.groupBox56 = new System.Windows.Forms.GroupBox();
            this.sefQ5_3S = new System.Windows.Forms.RadioButton();
            this.sefQ5_3N = new System.Windows.Forms.RadioButton();
            this.groupBox57 = new System.Windows.Forms.GroupBox();
            this.sefQ5_2S = new System.Windows.Forms.RadioButton();
            this.sefQ5_2N = new System.Windows.Forms.RadioButton();
            this.labelC_sefQ5_5 = new System.Windows.Forms.Label();
            this.labelC_sefQ5_4 = new System.Windows.Forms.Label();
            this.labelC_sefQ5_3 = new System.Windows.Forms.Label();
            this.labelC_sefQ5_2 = new System.Windows.Forms.Label();
            this.sefQ5_5C = new System.Windows.Forms.TextBox();
            this.checkBox22 = new System.Windows.Forms.CheckBox();
            this.label158 = new System.Windows.Forms.Label();
            this.sefQ5_4C = new System.Windows.Forms.TextBox();
            this.checkBox23 = new System.Windows.Forms.CheckBox();
            this.sefQ5_3C = new System.Windows.Forms.TextBox();
            this.checkBox24 = new System.Windows.Forms.CheckBox();
            this.sefQ5_2C = new System.Windows.Forms.TextBox();
            this.labelC_sefQ5_1 = new System.Windows.Forms.Label();
            this.sefQ5_1C = new System.Windows.Forms.TextBox();
            this.label160 = new System.Windows.Forms.Label();
            this.label161 = new System.Windows.Forms.Label();
            this.label162 = new System.Windows.Forms.Label();
            this.label163 = new System.Windows.Forms.Label();
            this.label164 = new System.Windows.Forms.Label();
            this.label165 = new System.Windows.Forms.Label();
            this.label166 = new System.Windows.Forms.Label();
            this.label167 = new System.Windows.Forms.Label();
            this.label168 = new System.Windows.Forms.Label();
            this.label169 = new System.Windows.Forms.Label();
            this.groupBox58 = new System.Windows.Forms.GroupBox();
            this.sefQ5_1S = new System.Windows.Forms.RadioButton();
            this.sefQ5_1N = new System.Windows.Forms.RadioButton();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.splitContainer14 = new System.Windows.Forms.SplitContainer();
            this.label170 = new System.Windows.Forms.Label();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.label171 = new System.Windows.Forms.Label();
            this.groupBox59 = new System.Windows.Forms.GroupBox();
            this.sefQ6_2S = new System.Windows.Forms.RadioButton();
            this.sefQ6_2N = new System.Windows.Forms.RadioButton();
            this.labelC_sefQ6_2 = new System.Windows.Forms.Label();
            this.sefQ6_2C = new System.Windows.Forms.TextBox();
            this.labelC_sefQ6_1 = new System.Windows.Forms.Label();
            this.sefQ6_1C = new System.Windows.Forms.TextBox();
            this.label174 = new System.Windows.Forms.Label();
            this.label175 = new System.Windows.Forms.Label();
            this.label176 = new System.Windows.Forms.Label();
            this.label177 = new System.Windows.Forms.Label();
            this.groupBox60 = new System.Windows.Forms.GroupBox();
            this.sefQ6_1S = new System.Windows.Forms.RadioButton();
            this.sefQ6_1N = new System.Windows.Forms.RadioButton();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.splitContainer16 = new System.Windows.Forms.SplitContainer();
            this.label178 = new System.Windows.Forms.Label();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.label179 = new System.Windows.Forms.Label();
            this.groupBox61 = new System.Windows.Forms.GroupBox();
            this.sefQ7_4N = new System.Windows.Forms.RadioButton();
            this.sefQ7_4S = new System.Windows.Forms.RadioButton();
            this.groupBox62 = new System.Windows.Forms.GroupBox();
            this.sefQ7_3S = new System.Windows.Forms.RadioButton();
            this.sefQ7_3N = new System.Windows.Forms.RadioButton();
            this.groupBox63 = new System.Windows.Forms.GroupBox();
            this.sefQ7_2S = new System.Windows.Forms.RadioButton();
            this.sefQ7_2N = new System.Windows.Forms.RadioButton();
            this.labelC_sefQ7_4 = new System.Windows.Forms.Label();
            this.labelC_sefQ7_3 = new System.Windows.Forms.Label();
            this.labelC_sefQ7_2 = new System.Windows.Forms.Label();
            this.label183 = new System.Windows.Forms.Label();
            this.sefQ7_4C = new System.Windows.Forms.TextBox();
            this.checkBox25 = new System.Windows.Forms.CheckBox();
            this.sefQ7_3C = new System.Windows.Forms.TextBox();
            this.checkBox26 = new System.Windows.Forms.CheckBox();
            this.sefQ7_2C = new System.Windows.Forms.TextBox();
            this.labelC_sefQ7_1 = new System.Windows.Forms.Label();
            this.sefQ7_1C = new System.Windows.Forms.TextBox();
            this.label185 = new System.Windows.Forms.Label();
            this.label186 = new System.Windows.Forms.Label();
            this.label187 = new System.Windows.Forms.Label();
            this.label188 = new System.Windows.Forms.Label();
            this.label189 = new System.Windows.Forms.Label();
            this.label190 = new System.Windows.Forms.Label();
            this.label191 = new System.Windows.Forms.Label();
            this.label192 = new System.Windows.Forms.Label();
            this.groupBox64 = new System.Windows.Forms.GroupBox();
            this.sefQ7_1S = new System.Windows.Forms.RadioButton();
            this.sefQ7_1N = new System.Windows.Forms.RadioButton();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abrirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.guardarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.guardarComoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cerrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.metodoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iSA6150820106ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uPMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pDSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reporteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generarReporteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ayudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.acercaDeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tableLayoutPanel1.SuspendLayout();
            this.tabControl_ls_sensor.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.Separacion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.Redundancia.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.Complejidad.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.Evaluacion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).BeginInit();
            this.splitContainer5.Panel1.SuspendLayout();
            this.splitContainer5.Panel2.SuspendLayout();
            this.splitContainer5.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox22.SuspendLayout();
            this.groupBox23.SuspendLayout();
            this.Procedimientos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer6)).BeginInit();
            this.splitContainer6.Panel1.SuspendLayout();
            this.splitContainer6.Panel2.SuspendLayout();
            this.splitContainer6.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this.groupBox24.SuspendLayout();
            this.groupBox25.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this.Entrenamiento.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer7)).BeginInit();
            this.splitContainer7.Panel1.SuspendLayout();
            this.splitContainer7.Panel2.SuspendLayout();
            this.splitContainer7.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.groupBox28.SuspendLayout();
            this.groupBox29.SuspendLayout();
            this.Ambiente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer15)).BeginInit();
            this.splitContainer15.Panel1.SuspendLayout();
            this.splitContainer15.Panel2.SuspendLayout();
            this.splitContainer15.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.groupBox31.SuspendLayout();
            this.groupBox32.SuspendLayout();
            this.groupBox33.SuspendLayout();
            this.groupBox34.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer8)).BeginInit();
            this.splitContainer8.Panel1.SuspendLayout();
            this.splitContainer8.Panel2.SuspendLayout();
            this.splitContainer8.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer9)).BeginInit();
            this.splitContainer9.Panel1.SuspendLayout();
            this.splitContainer9.Panel2.SuspendLayout();
            this.splitContainer9.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.groupBox27.SuspendLayout();
            this.groupBox30.SuspendLayout();
            this.groupBox35.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer10)).BeginInit();
            this.splitContainer10.Panel1.SuspendLayout();
            this.splitContainer10.Panel2.SuspendLayout();
            this.splitContainer10.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.groupBox39.SuspendLayout();
            this.groupBox40.SuspendLayout();
            this.groupBox41.SuspendLayout();
            this.groupBox42.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer11)).BeginInit();
            this.splitContainer11.Panel1.SuspendLayout();
            this.splitContainer11.Panel2.SuspendLayout();
            this.splitContainer11.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.groupBox44.SuspendLayout();
            this.groupBox45.SuspendLayout();
            this.groupBox46.SuspendLayout();
            this.groupBox47.SuspendLayout();
            this.groupBox48.SuspendLayout();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer12)).BeginInit();
            this.splitContainer12.Panel1.SuspendLayout();
            this.splitContainer12.Panel2.SuspendLayout();
            this.splitContainer12.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.groupBox49.SuspendLayout();
            this.groupBox50.SuspendLayout();
            this.groupBox51.SuspendLayout();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer13)).BeginInit();
            this.splitContainer13.Panel1.SuspendLayout();
            this.splitContainer13.Panel2.SuspendLayout();
            this.splitContainer13.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.groupBox54.SuspendLayout();
            this.groupBox55.SuspendLayout();
            this.groupBox56.SuspendLayout();
            this.groupBox57.SuspendLayout();
            this.groupBox58.SuspendLayout();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer14)).BeginInit();
            this.splitContainer14.Panel1.SuspendLayout();
            this.splitContainer14.Panel2.SuspendLayout();
            this.splitContainer14.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.groupBox59.SuspendLayout();
            this.groupBox60.SuspendLayout();
            this.tabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer16)).BeginInit();
            this.splitContainer16.Panel1.SuspendLayout();
            this.splitContainer16.Panel2.SuspendLayout();
            this.splitContainer16.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.groupBox61.SuspendLayout();
            this.groupBox62.SuspendLayout();
            this.groupBox63.SuspendLayout();
            this.groupBox64.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 8;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 122F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 114F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel1.Controls.Add(this.cancelar, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.aceptar, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.tabControl_ls_sensor, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.menuStrip1, 1, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 56F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(920, 598);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // cancelar
            // 
            this.cancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.cancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cancelar.FlatAppearance.BorderSize = 0;
            this.cancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelar.Font = new System.Drawing.Font("Raleway", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(55)))), ((int)(((byte)(94)))));
            this.cancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cancelar.Location = new System.Drawing.Point(853, 551);
            this.cancelar.Name = "cancelar";
            this.cancelar.Size = new System.Drawing.Size(54, 50);
            this.cancelar.TabIndex = 13;
            this.cancelar.Text = "Cancelar";
            this.cancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.cancelar.UseVisualStyleBackColor = true;
            // 
            // aceptar
            // 
            this.aceptar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.aceptar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.aceptar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.aceptar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.aceptar.FlatAppearance.BorderSize = 0;
            this.aceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.aceptar.Font = new System.Drawing.Font("Raleway", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aceptar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(55)))), ((int)(((byte)(94)))));
            this.aceptar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.aceptar.Location = new System.Drawing.Point(793, 551);
            this.aceptar.Name = "aceptar";
            this.aceptar.Size = new System.Drawing.Size(54, 50);
            this.aceptar.TabIndex = 14;
            this.aceptar.Text = "Aceptar";
            this.aceptar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.aceptar.UseVisualStyleBackColor = true;
            // 
            // tabControl_ls_sensor
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.tabControl_ls_sensor, 4);
            this.tabControl_ls_sensor.Controls.Add(this.tabPage1);
            this.tabControl_ls_sensor.Controls.Add(this.tabPage2);
            this.tabControl_ls_sensor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl_ls_sensor.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.tabControl_ls_sensor.Location = new System.Drawing.Point(3, 40);
            this.tabControl_ls_sensor.Multiline = true;
            this.tabControl_ls_sensor.Name = "tabControl_ls_sensor";
            this.tabControl_ls_sensor.SelectedIndex = 0;
            this.tabControl_ls_sensor.Size = new System.Drawing.Size(774, 505);
            this.tabControl_ls_sensor.TabIndex = 15;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.splitContainer1);
            this.tabPage1.Location = new System.Drawing.Point(4, 27);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(766, 474);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Controlador de Seguridad";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.label49);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabControl2);
            this.splitContainer1.Size = new System.Drawing.Size(760, 468);
            this.splitContainer1.SplitterDistance = 29;
            this.splitContainer1.TabIndex = 0;
            this.splitContainer1.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContainer1_SplitterMoved);
            // 
            // label49
            // 
            this.label49.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label49.Location = new System.Drawing.Point(0, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(760, 29);
            this.label49.TabIndex = 0;
            this.label49.Text = "ESTIMADOR DE FACTOR DE CAUSA COMÚN (β) - CONTROLADOR DE SEGURIDAD";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.Separacion);
            this.tabControl2.Controls.Add(this.Redundancia);
            this.tabControl2.Controls.Add(this.Complejidad);
            this.tabControl2.Controls.Add(this.Evaluacion);
            this.tabControl2.Controls.Add(this.Procedimientos);
            this.tabControl2.Controls.Add(this.Entrenamiento);
            this.tabControl2.Controls.Add(this.Ambiente);
            this.tabControl2.Controls.Add(this.Datos);
            this.tabControl2.Controls.Add(this.Resultados);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Font = new System.Drawing.Font("Raleway", 10.25F, System.Drawing.FontStyle.Bold);
            this.tabControl2.ItemSize = new System.Drawing.Size(80, 18);
            this.tabControl2.Location = new System.Drawing.Point(0, 0);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(760, 435);
            this.tabControl2.TabIndex = 2;
            // 
            // Separacion
            // 
            this.Separacion.AutoScroll = true;
            this.Separacion.CausesValidation = false;
            this.Separacion.Controls.Add(this.splitContainer2);
            this.Separacion.Location = new System.Drawing.Point(4, 22);
            this.Separacion.Name = "Separacion";
            this.Separacion.Padding = new System.Windows.Forms.Padding(3);
            this.Separacion.Size = new System.Drawing.Size(752, 409);
            this.Separacion.TabIndex = 0;
            this.Separacion.Text = "Separación";
            this.Separacion.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 3);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.label2);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.AutoScroll = true;
            this.splitContainer2.Panel2.Controls.Add(this.tableLayoutPanel3);
            this.splitContainer2.Size = new System.Drawing.Size(746, 403);
            this.splitContainer2.SplitterDistance = 44;
            this.splitContainer2.TabIndex = 25;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(828, 42);
            this.label2.TabIndex = 9;
            this.label2.Text = "Para el Logic Solver de la SIF, considere los siguientes factores relacionados co" +
    "n la separación y segregación de canales:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.AutoScroll = true;
            this.tableLayoutPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel3.ColumnCount = 7;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel3.Controls.Add(this.label3, 0, 10);
            this.tableLayoutPanel3.Controls.Add(this.groupBox4, 4, 7);
            this.tableLayoutPanel3.Controls.Add(this.groupBox3, 4, 4);
            this.tableLayoutPanel3.Controls.Add(this.labelC_lsQ1_3, 0, 8);
            this.tableLayoutPanel3.Controls.Add(this.labelC_lsQ1_2, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.lsQ1_3C, 2, 8);
            this.tableLayoutPanel3.Controls.Add(this.checkBox6, 6, 7);
            this.tableLayoutPanel3.Controls.Add(this.lsQ1_2C, 2, 5);
            this.tableLayoutPanel3.Controls.Add(this.labelC_lsQ1_1, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.lsQ1_1C, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.labelN_Q1_3, 0, 7);
            this.tableLayoutPanel3.Controls.Add(this.label_Q1_2, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.labelN_Q1_2, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.label_Q1_1, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.labelN_Q1_1, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label_Q1_3, 1, 7);
            this.tableLayoutPanel3.Controls.Add(this.groupBox2, 4, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.MinimumSize = new System.Drawing.Size(778, 386);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.tableLayoutPanel3.RowCount = 12;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(778, 386);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.tableLayoutPanel3.SetColumnSpan(this.label3, 6);
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(3, 386);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(740, 18);
            this.label3.TabIndex = 86;
            this.label3.Text = "  CSF Consultoría en Seguridad Funcional   Copyright ©  2019";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox4
            // 
            this.tableLayoutPanel3.SetColumnSpan(this.groupBox4, 2);
            this.groupBox4.Controls.Add(this.lsQ1_3S);
            this.groupBox4.Controls.Add(this.lsQ1_3N);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(639, 267);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(104, 30);
            this.groupBox4.TabIndex = 83;
            this.groupBox4.TabStop = false;
            // 
            // lsQ1_3S
            // 
            this.lsQ1_3S.AutoSize = true;
            this.lsQ1_3S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ1_3S.Location = new System.Drawing.Point(0, 8);
            this.lsQ1_3S.Name = "lsQ1_3S";
            this.lsQ1_3S.Size = new System.Drawing.Size(38, 22);
            this.lsQ1_3S.TabIndex = 73;
            this.lsQ1_3S.Text = "Si";
            this.lsQ1_3S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ1_3S.UseVisualStyleBackColor = true;
            this.lsQ1_3S.CheckedChanged += new System.EventHandler(this.updatevisibleLS1);
            // 
            // lsQ1_3N
            // 
            this.lsQ1_3N.AutoSize = true;
            this.lsQ1_3N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ1_3N.Location = new System.Drawing.Point(55, 8);
            this.lsQ1_3N.Name = "lsQ1_3N";
            this.lsQ1_3N.Size = new System.Drawing.Size(47, 22);
            this.lsQ1_3N.TabIndex = 74;
            this.lsQ1_3N.Text = "No";
            this.lsQ1_3N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ1_3N.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.tableLayoutPanel3.SetColumnSpan(this.groupBox3, 2);
            this.groupBox3.Controls.Add(this.lsQ1_2S);
            this.groupBox3.Controls.Add(this.lsQ1_2N);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(639, 145);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(104, 30);
            this.groupBox3.TabIndex = 82;
            this.groupBox3.TabStop = false;
            // 
            // lsQ1_2S
            // 
            this.lsQ1_2S.AutoSize = true;
            this.lsQ1_2S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ1_2S.Location = new System.Drawing.Point(0, 8);
            this.lsQ1_2S.Name = "lsQ1_2S";
            this.lsQ1_2S.Size = new System.Drawing.Size(38, 22);
            this.lsQ1_2S.TabIndex = 69;
            this.lsQ1_2S.Text = "Si";
            this.lsQ1_2S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ1_2S.UseVisualStyleBackColor = true;
            this.lsQ1_2S.CheckedChanged += new System.EventHandler(this.updatevisibleLS1);
            // 
            // lsQ1_2N
            // 
            this.lsQ1_2N.AutoSize = true;
            this.lsQ1_2N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ1_2N.Location = new System.Drawing.Point(55, 8);
            this.lsQ1_2N.Name = "lsQ1_2N";
            this.lsQ1_2N.Size = new System.Drawing.Size(47, 22);
            this.lsQ1_2N.TabIndex = 70;
            this.lsQ1_2N.Text = "No";
            this.lsQ1_2N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ1_2N.UseVisualStyleBackColor = true;
            // 
            // labelC_lsQ1_3
            // 
            this.labelC_lsQ1_3.AutoSize = true;
            this.tableLayoutPanel3.SetColumnSpan(this.labelC_lsQ1_3, 2);
            this.labelC_lsQ1_3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ1_3.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ1_3.Location = new System.Drawing.Point(3, 300);
            this.labelC_lsQ1_3.Name = "labelC_lsQ1_3";
            this.labelC_lsQ1_3.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ1_3.TabIndex = 72;
            this.labelC_lsQ1_3.Text = "Criterios:";
            this.labelC_lsQ1_3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ1_3.Visible = false;
            // 
            // labelC_lsQ1_2
            // 
            this.labelC_lsQ1_2.AutoSize = true;
            this.tableLayoutPanel3.SetColumnSpan(this.labelC_lsQ1_2, 2);
            this.labelC_lsQ1_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ1_2.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ1_2.Location = new System.Drawing.Point(3, 178);
            this.labelC_lsQ1_2.Name = "labelC_lsQ1_2";
            this.labelC_lsQ1_2.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ1_2.TabIndex = 71;
            this.labelC_lsQ1_2.Text = "Criterios:";
            this.labelC_lsQ1_2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ1_2.Visible = false;
            // 
            // lsQ1_3C
            // 
            this.tableLayoutPanel3.SetColumnSpan(this.lsQ1_3C, 3);
            this.lsQ1_3C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ1_3C.Location = new System.Drawing.Point(83, 303);
            this.lsQ1_3C.Multiline = true;
            this.lsQ1_3C.Name = "lsQ1_3C";
            this.lsQ1_3C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ1_3C.Size = new System.Drawing.Size(605, 60);
            this.lsQ1_3C.TabIndex = 54;
            this.lsQ1_3C.Visible = false;
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox6.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBox6.Location = new System.Drawing.Point(749, 267);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(4, 30);
            this.checkBox6.TabIndex = 52;
            this.checkBox6.UseVisualStyleBackColor = true;
            // 
            // lsQ1_2C
            // 
            this.tableLayoutPanel3.SetColumnSpan(this.lsQ1_2C, 3);
            this.lsQ1_2C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ1_2C.Location = new System.Drawing.Point(83, 181);
            this.lsQ1_2C.Multiline = true;
            this.lsQ1_2C.Name = "lsQ1_2C";
            this.lsQ1_2C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ1_2C.Size = new System.Drawing.Size(605, 60);
            this.lsQ1_2C.TabIndex = 48;
            this.lsQ1_2C.Visible = false;
            // 
            // labelC_lsQ1_1
            // 
            this.labelC_lsQ1_1.AutoSize = true;
            this.tableLayoutPanel3.SetColumnSpan(this.labelC_lsQ1_1, 2);
            this.labelC_lsQ1_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ1_1.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ1_1.Location = new System.Drawing.Point(3, 56);
            this.labelC_lsQ1_1.Name = "labelC_lsQ1_1";
            this.labelC_lsQ1_1.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ1_1.TabIndex = 45;
            this.labelC_lsQ1_1.Text = "Criterios:";
            this.labelC_lsQ1_1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ1_1.Visible = false;
            // 
            // lsQ1_1C
            // 
            this.tableLayoutPanel3.SetColumnSpan(this.lsQ1_1C, 3);
            this.lsQ1_1C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ1_1C.Location = new System.Drawing.Point(83, 59);
            this.lsQ1_1C.Multiline = true;
            this.lsQ1_1C.Name = "lsQ1_1C";
            this.lsQ1_1C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ1_1C.Size = new System.Drawing.Size(605, 60);
            this.lsQ1_1C.TabIndex = 32;
            this.lsQ1_1C.Visible = false;
            // 
            // labelN_Q1_3
            // 
            this.labelN_Q1_3.AutoSize = true;
            this.labelN_Q1_3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelN_Q1_3.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelN_Q1_3.Location = new System.Drawing.Point(3, 264);
            this.labelN_Q1_3.Name = "labelN_Q1_3";
            this.labelN_Q1_3.Size = new System.Drawing.Size(34, 36);
            this.labelN_Q1_3.TabIndex = 12;
            this.labelN_Q1_3.Text = "1.3.";
            this.labelN_Q1_3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label_Q1_2
            // 
            this.label_Q1_2.AutoSize = true;
            this.tableLayoutPanel3.SetColumnSpan(this.label_Q1_2, 2);
            this.label_Q1_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_Q1_2.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Q1_2.Location = new System.Drawing.Point(43, 142);
            this.label_Q1_2.Name = "label_Q1_2";
            this.label_Q1_2.Size = new System.Drawing.Size(580, 36);
            this.label_Q1_2.TabIndex = 10;
            this.label_Q1_2.Text = "¿Estan los canales del subsistema lógico en circuitos impresos separados ?";
            // 
            // labelN_Q1_2
            // 
            this.labelN_Q1_2.AutoSize = true;
            this.labelN_Q1_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelN_Q1_2.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelN_Q1_2.Location = new System.Drawing.Point(3, 142);
            this.labelN_Q1_2.Name = "labelN_Q1_2";
            this.labelN_Q1_2.Size = new System.Drawing.Size(34, 36);
            this.labelN_Q1_2.TabIndex = 11;
            this.labelN_Q1_2.Text = "1.2.";
            this.labelN_Q1_2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label_Q1_1
            // 
            this.label_Q1_1.AutoSize = true;
            this.tableLayoutPanel3.SetColumnSpan(this.label_Q1_1, 2);
            this.label_Q1_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_Q1_1.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Q1_1.Location = new System.Drawing.Point(43, 20);
            this.label_Q1_1.Name = "label_Q1_1";
            this.label_Q1_1.Size = new System.Drawing.Size(580, 36);
            this.label_Q1_1.TabIndex = 1;
            this.label_Q1_1.Text = "¿Todos los cables de señal para los canales se enrutan por separado en todas las " +
    "posiciones?";
            // 
            // labelN_Q1_1
            // 
            this.labelN_Q1_1.AutoSize = true;
            this.labelN_Q1_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelN_Q1_1.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelN_Q1_1.Location = new System.Drawing.Point(3, 20);
            this.labelN_Q1_1.Name = "labelN_Q1_1";
            this.labelN_Q1_1.Size = new System.Drawing.Size(34, 36);
            this.labelN_Q1_1.TabIndex = 9;
            this.labelN_Q1_1.Text = "1.1.";
            this.labelN_Q1_1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label_Q1_3
            // 
            this.label_Q1_3.AutoSize = true;
            this.tableLayoutPanel3.SetColumnSpan(this.label_Q1_3, 2);
            this.label_Q1_3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_Q1_3.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label_Q1_3.Location = new System.Drawing.Point(43, 264);
            this.label_Q1_3.Name = "label_Q1_3";
            this.label_Q1_3.Size = new System.Drawing.Size(580, 36);
            this.label_Q1_3.TabIndex = 14;
            this.label_Q1_3.Text = "¿Estan los canales del subsistema lógico físicamentte separados de manera efectiv" +
    "a? Ejemplo: gabinetes separados.";
            // 
            // groupBox2
            // 
            this.tableLayoutPanel3.SetColumnSpan(this.groupBox2, 2);
            this.groupBox2.Controls.Add(this.lsQ1_1S);
            this.groupBox2.Controls.Add(this.lsQ1_1N);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(639, 23);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(104, 30);
            this.groupBox2.TabIndex = 81;
            this.groupBox2.TabStop = false;
            // 
            // lsQ1_1S
            // 
            this.lsQ1_1S.AutoSize = true;
            this.lsQ1_1S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ1_1S.Location = new System.Drawing.Point(0, 8);
            this.lsQ1_1S.Name = "lsQ1_1S";
            this.lsQ1_1S.Size = new System.Drawing.Size(38, 22);
            this.lsQ1_1S.TabIndex = 67;
            this.lsQ1_1S.Text = "Si";
            this.lsQ1_1S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ1_1S.UseVisualStyleBackColor = true;
            this.lsQ1_1S.CheckedChanged += new System.EventHandler(this.updatevisibleLS1);
            // 
            // lsQ1_1N
            // 
            this.lsQ1_1N.AutoSize = true;
            this.lsQ1_1N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ1_1N.Location = new System.Drawing.Point(55, 8);
            this.lsQ1_1N.Name = "lsQ1_1N";
            this.lsQ1_1N.Size = new System.Drawing.Size(47, 22);
            this.lsQ1_1N.TabIndex = 68;
            this.lsQ1_1N.Text = "No";
            this.lsQ1_1N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ1_1N.UseVisualStyleBackColor = true;
            // 
            // Redundancia
            // 
            this.Redundancia.Controls.Add(this.splitContainer3);
            this.Redundancia.Location = new System.Drawing.Point(4, 22);
            this.Redundancia.Name = "Redundancia";
            this.Redundancia.Padding = new System.Windows.Forms.Padding(3);
            this.Redundancia.Size = new System.Drawing.Size(752, 409);
            this.Redundancia.TabIndex = 9;
            this.Redundancia.Text = "Redundancia";
            this.Redundancia.UseVisualStyleBackColor = true;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(3, 3);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.label1);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.AutoScroll = true;
            this.splitContainer3.Panel2.Controls.Add(this.tableLayoutPanel2);
            this.splitContainer3.Size = new System.Drawing.Size(746, 403);
            this.splitContainer3.SplitterDistance = 44;
            this.splitContainer3.TabIndex = 26;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(828, 42);
            this.label1.TabIndex = 9;
            this.label1.Text = "Para el Logic Solver de la SIF, considere los siguientes factores relacionados co" +
    "n la diversidad y redundancia de canales:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoScroll = true;
            this.tableLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel2.ColumnCount = 7;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel2.Controls.Add(this.lsQ2_7C, 2, 20);
            this.tableLayoutPanel2.Controls.Add(this.groupBox13, 4, 19);
            this.tableLayoutPanel2.Controls.Add(this.label27, 1, 19);
            this.tableLayoutPanel2.Controls.Add(this.labelC_lsQ2_7, 0, 20);
            this.tableLayoutPanel2.Controls.Add(this.label25, 0, 19);
            this.tableLayoutPanel2.Controls.Add(this.lsQ2_6C, 2, 17);
            this.tableLayoutPanel2.Controls.Add(this.labelC_lsQ2_6, 0, 17);
            this.tableLayoutPanel2.Controls.Add(this.groupBox12, 4, 16);
            this.tableLayoutPanel2.Controls.Add(this.label23, 1, 16);
            this.tableLayoutPanel2.Controls.Add(this.label21, 0, 16);
            this.tableLayoutPanel2.Controls.Add(this.label4, 0, 22);
            this.tableLayoutPanel2.Controls.Add(this.groupBox7, 4, 13);
            this.tableLayoutPanel2.Controls.Add(this.groupBox8, 4, 10);
            this.tableLayoutPanel2.Controls.Add(this.groupBox9, 4, 7);
            this.tableLayoutPanel2.Controls.Add(this.groupBox10, 4, 4);
            this.tableLayoutPanel2.Controls.Add(this.labelC_lsQ2_5, 0, 14);
            this.tableLayoutPanel2.Controls.Add(this.labelC_lsQ2_4, 0, 11);
            this.tableLayoutPanel2.Controls.Add(this.labelC_lsQ2_3, 0, 8);
            this.tableLayoutPanel2.Controls.Add(this.labelC_lsQ2_2, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.lsQ2_5C, 2, 14);
            this.tableLayoutPanel2.Controls.Add(this.checkBox1, 6, 13);
            this.tableLayoutPanel2.Controls.Add(this.label9, 3, 22);
            this.tableLayoutPanel2.Controls.Add(this.lsQ2_4C, 2, 11);
            this.tableLayoutPanel2.Controls.Add(this.checkBox2, 6, 10);
            this.tableLayoutPanel2.Controls.Add(this.lsQ2_3C, 2, 8);
            this.tableLayoutPanel2.Controls.Add(this.checkBox3, 6, 7);
            this.tableLayoutPanel2.Controls.Add(this.lsQ2_2C, 2, 5);
            this.tableLayoutPanel2.Controls.Add(this.labelC_lsQ2_1, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.lsQ2_1C, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.label11, 1, 13);
            this.tableLayoutPanel2.Controls.Add(this.label12, 1, 10);
            this.tableLayoutPanel2.Controls.Add(this.label13, 0, 10);
            this.tableLayoutPanel2.Controls.Add(this.label14, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.label15, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.label16, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.label17, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label18, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label19, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.label20, 0, 13);
            this.tableLayoutPanel2.Controls.Add(this.groupBox11, 4, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.MinimumSize = new System.Drawing.Size(862, 386);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.tableLayoutPanel2.RowCount = 24;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(862, 386);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // lsQ2_7C
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.lsQ2_7C, 3);
            this.lsQ2_7C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ2_7C.Location = new System.Drawing.Point(83, 791);
            this.lsQ2_7C.Multiline = true;
            this.lsQ2_7C.Name = "lsQ2_7C";
            this.lsQ2_7C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ2_7C.Size = new System.Drawing.Size(689, 60);
            this.lsQ2_7C.TabIndex = 96;
            this.lsQ2_7C.Visible = false;
            // 
            // groupBox13
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.groupBox13, 2);
            this.groupBox13.Controls.Add(this.lsQ2_7N);
            this.groupBox13.Controls.Add(this.lsQ2_7S);
            this.groupBox13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox13.Location = new System.Drawing.Point(723, 755);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(104, 30);
            this.groupBox13.TabIndex = 95;
            this.groupBox13.TabStop = false;
            // 
            // lsQ2_7N
            // 
            this.lsQ2_7N.AutoSize = true;
            this.lsQ2_7N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ2_7N.Location = new System.Drawing.Point(55, 8);
            this.lsQ2_7N.Name = "lsQ2_7N";
            this.lsQ2_7N.Size = new System.Drawing.Size(47, 22);
            this.lsQ2_7N.TabIndex = 80;
            this.lsQ2_7N.Text = "No";
            this.lsQ2_7N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ2_7N.UseVisualStyleBackColor = true;
            // 
            // lsQ2_7S
            // 
            this.lsQ2_7S.AutoSize = true;
            this.lsQ2_7S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ2_7S.Location = new System.Drawing.Point(0, 8);
            this.lsQ2_7S.Name = "lsQ2_7S";
            this.lsQ2_7S.Size = new System.Drawing.Size(38, 22);
            this.lsQ2_7S.TabIndex = 78;
            this.lsQ2_7S.Text = "Si";
            this.lsQ2_7S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ2_7S.UseVisualStyleBackColor = true;
            this.lsQ2_7S.CheckedChanged += new System.EventHandler(this.updatevisibleLS2);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.label27, 2);
            this.label27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label27.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(43, 752);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(664, 36);
            this.label27.TabIndex = 94;
            this.label27.Text = "¿El mantenimiento de cada canal esta siendo llevado a cabo por diferentes persona" +
    "s en diferentes momentos?";
            // 
            // labelC_lsQ2_7
            // 
            this.labelC_lsQ2_7.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.labelC_lsQ2_7, 2);
            this.labelC_lsQ2_7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ2_7.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ2_7.Location = new System.Drawing.Point(3, 788);
            this.labelC_lsQ2_7.Name = "labelC_lsQ2_7";
            this.labelC_lsQ2_7.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ2_7.TabIndex = 93;
            this.labelC_lsQ2_7.Text = "Criterios:";
            this.labelC_lsQ2_7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ2_7.Visible = false;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label25.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label25.Location = new System.Drawing.Point(3, 752);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(34, 36);
            this.label25.TabIndex = 92;
            this.label25.Text = "2.7.";
            this.label25.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lsQ2_6C
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.lsQ2_6C, 3);
            this.lsQ2_6C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ2_6C.Location = new System.Drawing.Point(83, 669);
            this.lsQ2_6C.Multiline = true;
            this.lsQ2_6C.Name = "lsQ2_6C";
            this.lsQ2_6C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ2_6C.Size = new System.Drawing.Size(689, 60);
            this.lsQ2_6C.TabIndex = 91;
            this.lsQ2_6C.Visible = false;
            // 
            // labelC_lsQ2_6
            // 
            this.labelC_lsQ2_6.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.labelC_lsQ2_6, 2);
            this.labelC_lsQ2_6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ2_6.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ2_6.Location = new System.Drawing.Point(3, 666);
            this.labelC_lsQ2_6.Name = "labelC_lsQ2_6";
            this.labelC_lsQ2_6.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ2_6.TabIndex = 90;
            this.labelC_lsQ2_6.Text = "Criterios:";
            this.labelC_lsQ2_6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ2_6.Visible = false;
            // 
            // groupBox12
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.groupBox12, 2);
            this.groupBox12.Controls.Add(this.lsQ2_6N);
            this.groupBox12.Controls.Add(this.lsQ2_6S);
            this.groupBox12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox12.Location = new System.Drawing.Point(723, 633);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(104, 30);
            this.groupBox12.TabIndex = 89;
            this.groupBox12.TabStop = false;
            // 
            // lsQ2_6N
            // 
            this.lsQ2_6N.AutoSize = true;
            this.lsQ2_6N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ2_6N.Location = new System.Drawing.Point(55, 8);
            this.lsQ2_6N.Name = "lsQ2_6N";
            this.lsQ2_6N.Size = new System.Drawing.Size(47, 22);
            this.lsQ2_6N.TabIndex = 80;
            this.lsQ2_6N.Text = "No";
            this.lsQ2_6N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ2_6N.UseVisualStyleBackColor = true;
            // 
            // lsQ2_6S
            // 
            this.lsQ2_6S.AutoSize = true;
            this.lsQ2_6S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ2_6S.Location = new System.Drawing.Point(0, 8);
            this.lsQ2_6S.Name = "lsQ2_6S";
            this.lsQ2_6S.Size = new System.Drawing.Size(38, 22);
            this.lsQ2_6S.TabIndex = 78;
            this.lsQ2_6S.Text = "Si";
            this.lsQ2_6S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ2_6S.UseVisualStyleBackColor = true;
            this.lsQ2_6S.CheckedChanged += new System.EventHandler(this.updatevisibleLS2);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.label23, 2);
            this.label23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label23.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label23.Location = new System.Drawing.Point(43, 630);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(664, 36);
            this.label23.TabIndex = 88;
            this.label23.Text = "¿Estan separados los métodos de prueba para cada canal y las personas utilizada p" +
    "ara la puesta en marcha?";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label21.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(3, 630);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(34, 36);
            this.label21.TabIndex = 87;
            this.label21.Text = "2.6.";
            this.label21.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.label4, 6);
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(3, 874);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(824, 18);
            this.label4.TabIndex = 86;
            this.label4.Text = "  CSF Consultoría en Seguridad Funcional   Copyright ©  2019";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox7
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.groupBox7, 2);
            this.groupBox7.Controls.Add(this.lsQ2_5N);
            this.groupBox7.Controls.Add(this.lsQ2_5S);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox7.Location = new System.Drawing.Point(723, 511);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(104, 30);
            this.groupBox7.TabIndex = 85;
            this.groupBox7.TabStop = false;
            // 
            // lsQ2_5N
            // 
            this.lsQ2_5N.AutoSize = true;
            this.lsQ2_5N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ2_5N.Location = new System.Drawing.Point(55, 8);
            this.lsQ2_5N.Name = "lsQ2_5N";
            this.lsQ2_5N.Size = new System.Drawing.Size(47, 22);
            this.lsQ2_5N.TabIndex = 80;
            this.lsQ2_5N.Text = "No";
            this.lsQ2_5N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ2_5N.UseVisualStyleBackColor = true;
            // 
            // lsQ2_5S
            // 
            this.lsQ2_5S.AutoSize = true;
            this.lsQ2_5S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ2_5S.Location = new System.Drawing.Point(0, 8);
            this.lsQ2_5S.Name = "lsQ2_5S";
            this.lsQ2_5S.Size = new System.Drawing.Size(38, 22);
            this.lsQ2_5S.TabIndex = 78;
            this.lsQ2_5S.Text = "Si";
            this.lsQ2_5S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ2_5S.UseVisualStyleBackColor = true;
            this.lsQ2_5S.CheckedChanged += new System.EventHandler(this.updatevisibleLS2);
            // 
            // groupBox8
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.groupBox8, 2);
            this.groupBox8.Controls.Add(this.lsQ2_4N);
            this.groupBox8.Controls.Add(this.lsQ2_4S);
            this.groupBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox8.Location = new System.Drawing.Point(723, 389);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(104, 30);
            this.groupBox8.TabIndex = 84;
            this.groupBox8.TabStop = false;
            // 
            // lsQ2_4N
            // 
            this.lsQ2_4N.AutoSize = true;
            this.lsQ2_4N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ2_4N.Location = new System.Drawing.Point(55, 8);
            this.lsQ2_4N.Name = "lsQ2_4N";
            this.lsQ2_4N.Size = new System.Drawing.Size(47, 22);
            this.lsQ2_4N.TabIndex = 79;
            this.lsQ2_4N.Text = "No";
            this.lsQ2_4N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ2_4N.UseVisualStyleBackColor = true;
            // 
            // lsQ2_4S
            // 
            this.lsQ2_4S.AutoSize = true;
            this.lsQ2_4S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ2_4S.Location = new System.Drawing.Point(0, 8);
            this.lsQ2_4S.Name = "lsQ2_4S";
            this.lsQ2_4S.Size = new System.Drawing.Size(38, 22);
            this.lsQ2_4S.TabIndex = 77;
            this.lsQ2_4S.Text = "Si";
            this.lsQ2_4S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ2_4S.UseVisualStyleBackColor = true;
            this.lsQ2_4S.CheckedChanged += new System.EventHandler(this.updatevisibleLS2);
            // 
            // groupBox9
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.groupBox9, 2);
            this.groupBox9.Controls.Add(this.lsQ2_3S);
            this.groupBox9.Controls.Add(this.lsQ2_3N);
            this.groupBox9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox9.Location = new System.Drawing.Point(723, 267);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(104, 30);
            this.groupBox9.TabIndex = 83;
            this.groupBox9.TabStop = false;
            // 
            // lsQ2_3S
            // 
            this.lsQ2_3S.AutoSize = true;
            this.lsQ2_3S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ2_3S.Location = new System.Drawing.Point(0, 8);
            this.lsQ2_3S.Name = "lsQ2_3S";
            this.lsQ2_3S.Size = new System.Drawing.Size(38, 22);
            this.lsQ2_3S.TabIndex = 73;
            this.lsQ2_3S.Text = "Si";
            this.lsQ2_3S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ2_3S.UseVisualStyleBackColor = true;
            this.lsQ2_3S.CheckedChanged += new System.EventHandler(this.updatevisibleLS2);
            // 
            // lsQ2_3N
            // 
            this.lsQ2_3N.AutoSize = true;
            this.lsQ2_3N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ2_3N.Location = new System.Drawing.Point(55, 8);
            this.lsQ2_3N.Name = "lsQ2_3N";
            this.lsQ2_3N.Size = new System.Drawing.Size(47, 22);
            this.lsQ2_3N.TabIndex = 74;
            this.lsQ2_3N.Text = "No";
            this.lsQ2_3N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ2_3N.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.groupBox10, 2);
            this.groupBox10.Controls.Add(this.lsQ2_2S);
            this.groupBox10.Controls.Add(this.lsQ2_2N);
            this.groupBox10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox10.Location = new System.Drawing.Point(723, 145);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(104, 30);
            this.groupBox10.TabIndex = 82;
            this.groupBox10.TabStop = false;
            // 
            // lsQ2_2S
            // 
            this.lsQ2_2S.AutoSize = true;
            this.lsQ2_2S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ2_2S.Location = new System.Drawing.Point(0, 8);
            this.lsQ2_2S.Name = "lsQ2_2S";
            this.lsQ2_2S.Size = new System.Drawing.Size(38, 22);
            this.lsQ2_2S.TabIndex = 69;
            this.lsQ2_2S.Text = "Si";
            this.lsQ2_2S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ2_2S.UseVisualStyleBackColor = true;
            this.lsQ2_2S.CheckedChanged += new System.EventHandler(this.updatevisibleLS2);
            // 
            // lsQ2_2N
            // 
            this.lsQ2_2N.AutoSize = true;
            this.lsQ2_2N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ2_2N.Location = new System.Drawing.Point(55, 8);
            this.lsQ2_2N.Name = "lsQ2_2N";
            this.lsQ2_2N.Size = new System.Drawing.Size(47, 22);
            this.lsQ2_2N.TabIndex = 70;
            this.lsQ2_2N.Text = "No";
            this.lsQ2_2N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ2_2N.UseVisualStyleBackColor = true;
            // 
            // labelC_lsQ2_5
            // 
            this.labelC_lsQ2_5.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.labelC_lsQ2_5, 2);
            this.labelC_lsQ2_5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ2_5.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ2_5.Location = new System.Drawing.Point(3, 544);
            this.labelC_lsQ2_5.Name = "labelC_lsQ2_5";
            this.labelC_lsQ2_5.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ2_5.TabIndex = 76;
            this.labelC_lsQ2_5.Text = "Criterios:";
            this.labelC_lsQ2_5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ2_5.Visible = false;
            // 
            // labelC_lsQ2_4
            // 
            this.labelC_lsQ2_4.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.labelC_lsQ2_4, 2);
            this.labelC_lsQ2_4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ2_4.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ2_4.Location = new System.Drawing.Point(3, 422);
            this.labelC_lsQ2_4.Name = "labelC_lsQ2_4";
            this.labelC_lsQ2_4.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ2_4.TabIndex = 75;
            this.labelC_lsQ2_4.Text = "Criterios:";
            this.labelC_lsQ2_4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ2_4.Visible = false;
            // 
            // labelC_lsQ2_3
            // 
            this.labelC_lsQ2_3.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.labelC_lsQ2_3, 2);
            this.labelC_lsQ2_3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ2_3.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ2_3.Location = new System.Drawing.Point(3, 300);
            this.labelC_lsQ2_3.Name = "labelC_lsQ2_3";
            this.labelC_lsQ2_3.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ2_3.TabIndex = 72;
            this.labelC_lsQ2_3.Text = "Criterios:";
            this.labelC_lsQ2_3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ2_3.Visible = false;
            // 
            // labelC_lsQ2_2
            // 
            this.labelC_lsQ2_2.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.labelC_lsQ2_2, 2);
            this.labelC_lsQ2_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ2_2.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ2_2.Location = new System.Drawing.Point(3, 178);
            this.labelC_lsQ2_2.Name = "labelC_lsQ2_2";
            this.labelC_lsQ2_2.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ2_2.TabIndex = 71;
            this.labelC_lsQ2_2.Text = "Criterios:";
            this.labelC_lsQ2_2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ2_2.Visible = false;
            // 
            // lsQ2_5C
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.lsQ2_5C, 3);
            this.lsQ2_5C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ2_5C.Location = new System.Drawing.Point(83, 547);
            this.lsQ2_5C.Multiline = true;
            this.lsQ2_5C.Name = "lsQ2_5C";
            this.lsQ2_5C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ2_5C.Size = new System.Drawing.Size(689, 60);
            this.lsQ2_5C.TabIndex = 66;
            this.lsQ2_5C.Visible = false;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBox1.Location = new System.Drawing.Point(833, 511);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(4, 30);
            this.checkBox1.TabIndex = 64;
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.label9, 4);
            this.label9.Location = new System.Drawing.Point(3, 892);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 16);
            this.label9.TabIndex = 61;
            // 
            // lsQ2_4C
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.lsQ2_4C, 3);
            this.lsQ2_4C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ2_4C.Location = new System.Drawing.Point(83, 425);
            this.lsQ2_4C.Multiline = true;
            this.lsQ2_4C.Name = "lsQ2_4C";
            this.lsQ2_4C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ2_4C.Size = new System.Drawing.Size(689, 60);
            this.lsQ2_4C.TabIndex = 60;
            this.lsQ2_4C.Visible = false;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBox2.Location = new System.Drawing.Point(833, 389);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(4, 30);
            this.checkBox2.TabIndex = 58;
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // lsQ2_3C
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.lsQ2_3C, 3);
            this.lsQ2_3C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ2_3C.Location = new System.Drawing.Point(83, 303);
            this.lsQ2_3C.Multiline = true;
            this.lsQ2_3C.Name = "lsQ2_3C";
            this.lsQ2_3C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ2_3C.Size = new System.Drawing.Size(689, 60);
            this.lsQ2_3C.TabIndex = 54;
            this.lsQ2_3C.Visible = false;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox3.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBox3.Location = new System.Drawing.Point(833, 267);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(4, 30);
            this.checkBox3.TabIndex = 52;
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // lsQ2_2C
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.lsQ2_2C, 3);
            this.lsQ2_2C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ2_2C.Location = new System.Drawing.Point(83, 181);
            this.lsQ2_2C.Multiline = true;
            this.lsQ2_2C.Name = "lsQ2_2C";
            this.lsQ2_2C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ2_2C.Size = new System.Drawing.Size(689, 60);
            this.lsQ2_2C.TabIndex = 48;
            this.lsQ2_2C.Visible = false;
            // 
            // labelC_lsQ2_1
            // 
            this.labelC_lsQ2_1.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.labelC_lsQ2_1, 2);
            this.labelC_lsQ2_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ2_1.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ2_1.Location = new System.Drawing.Point(3, 56);
            this.labelC_lsQ2_1.Name = "labelC_lsQ2_1";
            this.labelC_lsQ2_1.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ2_1.TabIndex = 45;
            this.labelC_lsQ2_1.Text = "Criterios:";
            this.labelC_lsQ2_1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ2_1.Visible = false;
            // 
            // lsQ2_1C
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.lsQ2_1C, 3);
            this.lsQ2_1C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ2_1C.Location = new System.Drawing.Point(83, 59);
            this.lsQ2_1C.Multiline = true;
            this.lsQ2_1C.Name = "lsQ2_1C";
            this.lsQ2_1C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ2_1C.Size = new System.Drawing.Size(689, 60);
            this.lsQ2_1C.TabIndex = 32;
            this.lsQ2_1C.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.label11, 2);
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(43, 508);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(664, 36);
            this.label11.TabIndex = 17;
            this.label11.Text = "¿Fueron diseñados los canales por diferentes proyectistas, sin comunicación entre" +
    " ellos durante las actividades de diseño?";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.label12, 2);
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(43, 386);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(664, 36);
            this.label12.TabIndex = 16;
            this.label12.Text = "¿Es utilizada la diversidad media, por ejemplo pruebas de diagnóstico de hardware" +
    " usando una tecnología diferente?";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(3, 386);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(34, 36);
            this.label13.TabIndex = 13;
            this.label13.Text = "2.4.";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(3, 264);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(34, 36);
            this.label14.TabIndex = 12;
            this.label14.Text = "2.3.";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.label15, 2);
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(43, 142);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(664, 36);
            this.label15.TabIndex = 10;
            this.label15.Text = "¿Los canales emplean diferentes tecnologías electrónicas, por ejemplo, una electr" +
    "ónica y otra electrónica programable?";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label16.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(3, 142);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(34, 36);
            this.label16.TabIndex = 11;
            this.label16.Text = "2.2.";
            this.label16.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.label17, 2);
            this.label17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label17.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(43, 20);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(664, 36);
            this.label17.TabIndex = 1;
            this.label17.Text = "¿Los canales emplean diferentes tecnologías eléctricas, por ejemplo, una electrón" +
    "ica o electrónicos programables y otros relé?";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label18.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(3, 20);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(34, 36);
            this.label18.TabIndex = 9;
            this.label18.Text = "2.1.";
            this.label18.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.label19, 2);
            this.label19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label19.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(43, 264);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(664, 36);
            this.label19.TabIndex = 14;
            this.label19.Text = "¿Es utiliza la diversidad baja, por ejemplo pruebas de diagnóstico de hardware us" +
    "ando la misma tecnología?";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label20.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(3, 508);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(34, 36);
            this.label20.TabIndex = 15;
            this.label20.Text = "2.5.";
            this.label20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // groupBox11
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.groupBox11, 2);
            this.groupBox11.Controls.Add(this.lsQ2_1S);
            this.groupBox11.Controls.Add(this.lsQ2_1N);
            this.groupBox11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox11.Location = new System.Drawing.Point(723, 23);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(104, 30);
            this.groupBox11.TabIndex = 81;
            this.groupBox11.TabStop = false;
            // 
            // lsQ2_1S
            // 
            this.lsQ2_1S.AutoSize = true;
            this.lsQ2_1S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ2_1S.Location = new System.Drawing.Point(0, 8);
            this.lsQ2_1S.Name = "lsQ2_1S";
            this.lsQ2_1S.Size = new System.Drawing.Size(38, 22);
            this.lsQ2_1S.TabIndex = 67;
            this.lsQ2_1S.Text = "Si";
            this.lsQ2_1S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ2_1S.UseVisualStyleBackColor = true;
            this.lsQ2_1S.CheckedChanged += new System.EventHandler(this.updatevisibleLS2);
            // 
            // lsQ2_1N
            // 
            this.lsQ2_1N.AutoSize = true;
            this.lsQ2_1N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ2_1N.Location = new System.Drawing.Point(55, 8);
            this.lsQ2_1N.Name = "lsQ2_1N";
            this.lsQ2_1N.Size = new System.Drawing.Size(47, 22);
            this.lsQ2_1N.TabIndex = 68;
            this.lsQ2_1N.Text = "No";
            this.lsQ2_1N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ2_1N.UseVisualStyleBackColor = true;
            // 
            // Complejidad
            // 
            this.Complejidad.Controls.Add(this.splitContainer4);
            this.Complejidad.Location = new System.Drawing.Point(4, 22);
            this.Complejidad.Name = "Complejidad";
            this.Complejidad.Padding = new System.Windows.Forms.Padding(3);
            this.Complejidad.Size = new System.Drawing.Size(752, 409);
            this.Complejidad.TabIndex = 2;
            this.Complejidad.Text = "Complejidad";
            this.Complejidad.UseVisualStyleBackColor = true;
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(3, 3);
            this.splitContainer4.Name = "splitContainer4";
            this.splitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.label5);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.AutoScroll = true;
            this.splitContainer4.Panel2.Controls.Add(this.tableLayoutPanel4);
            this.splitContainer4.Size = new System.Drawing.Size(746, 403);
            this.splitContainer4.SplitterDistance = 44;
            this.splitContainer4.TabIndex = 27;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(828, 42);
            this.label5.TabIndex = 9;
            this.label5.Text = "Para el Logic Solver de la SIF, considere los siguientes factores relacionados co" +
    "n la Complejidad, el diseño, la aplicación, la madurez y la experiencia en el si" +
    "stema:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.AutoScroll = true;
            this.tableLayoutPanel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel4.ColumnCount = 7;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel4.Controls.Add(this.lsQ3_6C, 2, 17);
            this.tableLayoutPanel4.Controls.Add(this.labelC_lsQ3_6, 0, 17);
            this.tableLayoutPanel4.Controls.Add(this.groupBox14, 4, 16);
            this.tableLayoutPanel4.Controls.Add(this.label24, 1, 16);
            this.tableLayoutPanel4.Controls.Add(this.label26, 0, 16);
            this.tableLayoutPanel4.Controls.Add(this.label28, 0, 19);
            this.tableLayoutPanel4.Controls.Add(this.groupBox15, 4, 13);
            this.tableLayoutPanel4.Controls.Add(this.groupBox16, 4, 10);
            this.tableLayoutPanel4.Controls.Add(this.groupBox17, 4, 7);
            this.tableLayoutPanel4.Controls.Add(this.groupBox18, 4, 4);
            this.tableLayoutPanel4.Controls.Add(this.labelC_lsQ3_5, 0, 14);
            this.tableLayoutPanel4.Controls.Add(this.labelC_lsQ3_4, 0, 11);
            this.tableLayoutPanel4.Controls.Add(this.labelC_lsQ3_3, 0, 8);
            this.tableLayoutPanel4.Controls.Add(this.labelC_lsQ3_2, 0, 5);
            this.tableLayoutPanel4.Controls.Add(this.lsQ3_5C, 2, 14);
            this.tableLayoutPanel4.Controls.Add(this.checkBox4, 6, 13);
            this.tableLayoutPanel4.Controls.Add(this.label33, 3, 19);
            this.tableLayoutPanel4.Controls.Add(this.lsQ3_4C, 2, 11);
            this.tableLayoutPanel4.Controls.Add(this.checkBox5, 6, 10);
            this.tableLayoutPanel4.Controls.Add(this.lsQ3_3C, 2, 8);
            this.tableLayoutPanel4.Controls.Add(this.checkBox7, 6, 7);
            this.tableLayoutPanel4.Controls.Add(this.lsQ3_2C, 2, 5);
            this.tableLayoutPanel4.Controls.Add(this.labelC_lsQ3_1, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.lsQ3_1C, 2, 2);
            this.tableLayoutPanel4.Controls.Add(this.label35, 1, 13);
            this.tableLayoutPanel4.Controls.Add(this.label36, 1, 10);
            this.tableLayoutPanel4.Controls.Add(this.label37, 0, 10);
            this.tableLayoutPanel4.Controls.Add(this.label38, 0, 7);
            this.tableLayoutPanel4.Controls.Add(this.label39, 1, 4);
            this.tableLayoutPanel4.Controls.Add(this.label40, 0, 4);
            this.tableLayoutPanel4.Controls.Add(this.label41, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.label42, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.label43, 1, 7);
            this.tableLayoutPanel4.Controls.Add(this.label44, 0, 13);
            this.tableLayoutPanel4.Controls.Add(this.groupBox19, 4, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.MinimumSize = new System.Drawing.Size(862, 386);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.tableLayoutPanel4.RowCount = 21;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(862, 386);
            this.tableLayoutPanel4.TabIndex = 2;
            // 
            // lsQ3_6C
            // 
            this.tableLayoutPanel4.SetColumnSpan(this.lsQ3_6C, 3);
            this.lsQ3_6C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ3_6C.Location = new System.Drawing.Point(83, 669);
            this.lsQ3_6C.Multiline = true;
            this.lsQ3_6C.Name = "lsQ3_6C";
            this.lsQ3_6C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ3_6C.Size = new System.Drawing.Size(689, 60);
            this.lsQ3_6C.TabIndex = 91;
            this.lsQ3_6C.Visible = false;
            // 
            // labelC_lsQ3_6
            // 
            this.labelC_lsQ3_6.AutoSize = true;
            this.tableLayoutPanel4.SetColumnSpan(this.labelC_lsQ3_6, 2);
            this.labelC_lsQ3_6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ3_6.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ3_6.Location = new System.Drawing.Point(3, 666);
            this.labelC_lsQ3_6.Name = "labelC_lsQ3_6";
            this.labelC_lsQ3_6.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ3_6.TabIndex = 90;
            this.labelC_lsQ3_6.Text = "Criterios:";
            this.labelC_lsQ3_6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ3_6.Visible = false;
            // 
            // groupBox14
            // 
            this.tableLayoutPanel4.SetColumnSpan(this.groupBox14, 2);
            this.groupBox14.Controls.Add(this.lsQ3_6N);
            this.groupBox14.Controls.Add(this.lsQ3_6S);
            this.groupBox14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox14.Location = new System.Drawing.Point(723, 633);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(104, 30);
            this.groupBox14.TabIndex = 89;
            this.groupBox14.TabStop = false;
            // 
            // lsQ3_6N
            // 
            this.lsQ3_6N.AutoSize = true;
            this.lsQ3_6N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ3_6N.Location = new System.Drawing.Point(55, 8);
            this.lsQ3_6N.Name = "lsQ3_6N";
            this.lsQ3_6N.Size = new System.Drawing.Size(47, 22);
            this.lsQ3_6N.TabIndex = 80;
            this.lsQ3_6N.Text = "No";
            this.lsQ3_6N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ3_6N.UseVisualStyleBackColor = true;
            // 
            // lsQ3_6S
            // 
            this.lsQ3_6S.AutoSize = true;
            this.lsQ3_6S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ3_6S.Location = new System.Drawing.Point(0, 8);
            this.lsQ3_6S.Name = "lsQ3_6S";
            this.lsQ3_6S.Size = new System.Drawing.Size(38, 22);
            this.lsQ3_6S.TabIndex = 78;
            this.lsQ3_6S.Text = "Si";
            this.lsQ3_6S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ3_6S.UseVisualStyleBackColor = true;
            this.lsQ3_6S.CheckedChanged += new System.EventHandler(this.updatevisibleLS3);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.tableLayoutPanel4.SetColumnSpan(this.label24, 2);
            this.label24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label24.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(43, 630);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(664, 36);
            this.label24.TabIndex = 88;
            this.label24.Text = "¿Estan todos los componentes/dispositivos conservadoramente evaluado (por ejemplo" +
    ", por un factor de 2 o mas)?";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label26.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label26.Location = new System.Drawing.Point(3, 630);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(34, 36);
            this.label26.TabIndex = 87;
            this.label26.Text = "3.6.";
            this.label26.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.tableLayoutPanel4.SetColumnSpan(this.label28, 6);
            this.label28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label28.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label28.Location = new System.Drawing.Point(3, 752);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(824, 18);
            this.label28.TabIndex = 86;
            this.label28.Text = "  CSF Consultoría en Seguridad Funcional   Copyright ©  2019";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox15
            // 
            this.tableLayoutPanel4.SetColumnSpan(this.groupBox15, 2);
            this.groupBox15.Controls.Add(this.lsQ3_5N);
            this.groupBox15.Controls.Add(this.lsQ3_5S);
            this.groupBox15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox15.Location = new System.Drawing.Point(723, 511);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(104, 30);
            this.groupBox15.TabIndex = 85;
            this.groupBox15.TabStop = false;
            // 
            // lsQ3_5N
            // 
            this.lsQ3_5N.AutoSize = true;
            this.lsQ3_5N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ3_5N.Location = new System.Drawing.Point(55, 8);
            this.lsQ3_5N.Name = "lsQ3_5N";
            this.lsQ3_5N.Size = new System.Drawing.Size(47, 22);
            this.lsQ3_5N.TabIndex = 80;
            this.lsQ3_5N.Text = "No";
            this.lsQ3_5N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ3_5N.UseVisualStyleBackColor = true;
            // 
            // lsQ3_5S
            // 
            this.lsQ3_5S.AutoSize = true;
            this.lsQ3_5S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ3_5S.Location = new System.Drawing.Point(0, 8);
            this.lsQ3_5S.Name = "lsQ3_5S";
            this.lsQ3_5S.Size = new System.Drawing.Size(38, 22);
            this.lsQ3_5S.TabIndex = 78;
            this.lsQ3_5S.Text = "Si";
            this.lsQ3_5S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ3_5S.UseVisualStyleBackColor = true;
            this.lsQ3_5S.CheckedChanged += new System.EventHandler(this.updatevisibleLS3);
            // 
            // groupBox16
            // 
            this.tableLayoutPanel4.SetColumnSpan(this.groupBox16, 2);
            this.groupBox16.Controls.Add(this.lsQ3_4N);
            this.groupBox16.Controls.Add(this.lsQ3_4S);
            this.groupBox16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox16.Location = new System.Drawing.Point(723, 389);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(104, 30);
            this.groupBox16.TabIndex = 84;
            this.groupBox16.TabStop = false;
            // 
            // lsQ3_4N
            // 
            this.lsQ3_4N.AutoSize = true;
            this.lsQ3_4N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ3_4N.Location = new System.Drawing.Point(55, 8);
            this.lsQ3_4N.Name = "lsQ3_4N";
            this.lsQ3_4N.Size = new System.Drawing.Size(47, 22);
            this.lsQ3_4N.TabIndex = 79;
            this.lsQ3_4N.Text = "No";
            this.lsQ3_4N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ3_4N.UseVisualStyleBackColor = true;
            // 
            // lsQ3_4S
            // 
            this.lsQ3_4S.AutoSize = true;
            this.lsQ3_4S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ3_4S.Location = new System.Drawing.Point(0, 8);
            this.lsQ3_4S.Name = "lsQ3_4S";
            this.lsQ3_4S.Size = new System.Drawing.Size(38, 22);
            this.lsQ3_4S.TabIndex = 77;
            this.lsQ3_4S.Text = "Si";
            this.lsQ3_4S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ3_4S.UseVisualStyleBackColor = true;
            this.lsQ3_4S.CheckedChanged += new System.EventHandler(this.updatevisibleLS3);
            // 
            // groupBox17
            // 
            this.tableLayoutPanel4.SetColumnSpan(this.groupBox17, 2);
            this.groupBox17.Controls.Add(this.lsQ3_3S);
            this.groupBox17.Controls.Add(this.lsQ3_3N);
            this.groupBox17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox17.Location = new System.Drawing.Point(723, 267);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(104, 30);
            this.groupBox17.TabIndex = 83;
            this.groupBox17.TabStop = false;
            // 
            // lsQ3_3S
            // 
            this.lsQ3_3S.AutoSize = true;
            this.lsQ3_3S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ3_3S.Location = new System.Drawing.Point(0, 8);
            this.lsQ3_3S.Name = "lsQ3_3S";
            this.lsQ3_3S.Size = new System.Drawing.Size(38, 22);
            this.lsQ3_3S.TabIndex = 73;
            this.lsQ3_3S.Text = "Si";
            this.lsQ3_3S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ3_3S.UseVisualStyleBackColor = true;
            this.lsQ3_3S.CheckedChanged += new System.EventHandler(this.updatevisibleLS3);
            // 
            // lsQ3_3N
            // 
            this.lsQ3_3N.AutoSize = true;
            this.lsQ3_3N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ3_3N.Location = new System.Drawing.Point(55, 8);
            this.lsQ3_3N.Name = "lsQ3_3N";
            this.lsQ3_3N.Size = new System.Drawing.Size(47, 22);
            this.lsQ3_3N.TabIndex = 74;
            this.lsQ3_3N.Text = "No";
            this.lsQ3_3N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ3_3N.UseVisualStyleBackColor = true;
            // 
            // groupBox18
            // 
            this.tableLayoutPanel4.SetColumnSpan(this.groupBox18, 2);
            this.groupBox18.Controls.Add(this.lsQ3_2S);
            this.groupBox18.Controls.Add(this.lsQ3_2N);
            this.groupBox18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox18.Location = new System.Drawing.Point(723, 145);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(104, 30);
            this.groupBox18.TabIndex = 82;
            this.groupBox18.TabStop = false;
            // 
            // lsQ3_2S
            // 
            this.lsQ3_2S.AutoSize = true;
            this.lsQ3_2S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ3_2S.Location = new System.Drawing.Point(0, 8);
            this.lsQ3_2S.Name = "lsQ3_2S";
            this.lsQ3_2S.Size = new System.Drawing.Size(38, 22);
            this.lsQ3_2S.TabIndex = 69;
            this.lsQ3_2S.Text = "Si";
            this.lsQ3_2S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ3_2S.UseVisualStyleBackColor = true;
            this.lsQ3_2S.CheckedChanged += new System.EventHandler(this.updatevisibleLS3);
            // 
            // lsQ3_2N
            // 
            this.lsQ3_2N.AutoSize = true;
            this.lsQ3_2N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ3_2N.Location = new System.Drawing.Point(55, 8);
            this.lsQ3_2N.Name = "lsQ3_2N";
            this.lsQ3_2N.Size = new System.Drawing.Size(47, 22);
            this.lsQ3_2N.TabIndex = 70;
            this.lsQ3_2N.Text = "No";
            this.lsQ3_2N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ3_2N.UseVisualStyleBackColor = true;
            // 
            // labelC_lsQ3_5
            // 
            this.labelC_lsQ3_5.AutoSize = true;
            this.tableLayoutPanel4.SetColumnSpan(this.labelC_lsQ3_5, 2);
            this.labelC_lsQ3_5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ3_5.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ3_5.Location = new System.Drawing.Point(3, 544);
            this.labelC_lsQ3_5.Name = "labelC_lsQ3_5";
            this.labelC_lsQ3_5.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ3_5.TabIndex = 76;
            this.labelC_lsQ3_5.Text = "Criterios:";
            this.labelC_lsQ3_5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ3_5.Visible = false;
            // 
            // labelC_lsQ3_4
            // 
            this.labelC_lsQ3_4.AutoSize = true;
            this.tableLayoutPanel4.SetColumnSpan(this.labelC_lsQ3_4, 2);
            this.labelC_lsQ3_4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ3_4.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ3_4.Location = new System.Drawing.Point(3, 422);
            this.labelC_lsQ3_4.Name = "labelC_lsQ3_4";
            this.labelC_lsQ3_4.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ3_4.TabIndex = 75;
            this.labelC_lsQ3_4.Text = "Criterios:";
            this.labelC_lsQ3_4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ3_4.Visible = false;
            // 
            // labelC_lsQ3_3
            // 
            this.labelC_lsQ3_3.AutoSize = true;
            this.tableLayoutPanel4.SetColumnSpan(this.labelC_lsQ3_3, 2);
            this.labelC_lsQ3_3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ3_3.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ3_3.Location = new System.Drawing.Point(3, 300);
            this.labelC_lsQ3_3.Name = "labelC_lsQ3_3";
            this.labelC_lsQ3_3.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ3_3.TabIndex = 72;
            this.labelC_lsQ3_3.Text = "Criterios:";
            this.labelC_lsQ3_3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ3_3.Visible = false;
            // 
            // labelC_lsQ3_2
            // 
            this.labelC_lsQ3_2.AutoSize = true;
            this.tableLayoutPanel4.SetColumnSpan(this.labelC_lsQ3_2, 2);
            this.labelC_lsQ3_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ3_2.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ3_2.Location = new System.Drawing.Point(3, 178);
            this.labelC_lsQ3_2.Name = "labelC_lsQ3_2";
            this.labelC_lsQ3_2.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ3_2.TabIndex = 71;
            this.labelC_lsQ3_2.Text = "Criterios:";
            this.labelC_lsQ3_2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ3_2.Visible = false;
            // 
            // lsQ3_5C
            // 
            this.tableLayoutPanel4.SetColumnSpan(this.lsQ3_5C, 3);
            this.lsQ3_5C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ3_5C.Location = new System.Drawing.Point(83, 547);
            this.lsQ3_5C.Multiline = true;
            this.lsQ3_5C.Name = "lsQ3_5C";
            this.lsQ3_5C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ3_5C.Size = new System.Drawing.Size(689, 60);
            this.lsQ3_5C.TabIndex = 66;
            this.lsQ3_5C.Visible = false;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox4.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBox4.Location = new System.Drawing.Point(833, 511);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(4, 30);
            this.checkBox4.TabIndex = 64;
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.tableLayoutPanel4.SetColumnSpan(this.label33, 4);
            this.label33.Location = new System.Drawing.Point(3, 770);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(0, 16);
            this.label33.TabIndex = 61;
            // 
            // lsQ3_4C
            // 
            this.tableLayoutPanel4.SetColumnSpan(this.lsQ3_4C, 3);
            this.lsQ3_4C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ3_4C.Location = new System.Drawing.Point(83, 425);
            this.lsQ3_4C.Multiline = true;
            this.lsQ3_4C.Name = "lsQ3_4C";
            this.lsQ3_4C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ3_4C.Size = new System.Drawing.Size(689, 60);
            this.lsQ3_4C.TabIndex = 60;
            this.lsQ3_4C.Visible = false;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox5.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBox5.Location = new System.Drawing.Point(833, 389);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(4, 30);
            this.checkBox5.TabIndex = 58;
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // lsQ3_3C
            // 
            this.tableLayoutPanel4.SetColumnSpan(this.lsQ3_3C, 3);
            this.lsQ3_3C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ3_3C.Location = new System.Drawing.Point(83, 303);
            this.lsQ3_3C.Multiline = true;
            this.lsQ3_3C.Name = "lsQ3_3C";
            this.lsQ3_3C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ3_3C.Size = new System.Drawing.Size(689, 60);
            this.lsQ3_3C.TabIndex = 54;
            this.lsQ3_3C.Visible = false;
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox7.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBox7.Location = new System.Drawing.Point(833, 267);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(4, 30);
            this.checkBox7.TabIndex = 52;
            this.checkBox7.UseVisualStyleBackColor = true;
            // 
            // lsQ3_2C
            // 
            this.tableLayoutPanel4.SetColumnSpan(this.lsQ3_2C, 3);
            this.lsQ3_2C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ3_2C.Location = new System.Drawing.Point(83, 181);
            this.lsQ3_2C.Multiline = true;
            this.lsQ3_2C.Name = "lsQ3_2C";
            this.lsQ3_2C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ3_2C.Size = new System.Drawing.Size(689, 60);
            this.lsQ3_2C.TabIndex = 48;
            this.lsQ3_2C.Visible = false;
            // 
            // labelC_lsQ3_1
            // 
            this.labelC_lsQ3_1.AutoSize = true;
            this.tableLayoutPanel4.SetColumnSpan(this.labelC_lsQ3_1, 2);
            this.labelC_lsQ3_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ3_1.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ3_1.Location = new System.Drawing.Point(3, 56);
            this.labelC_lsQ3_1.Name = "labelC_lsQ3_1";
            this.labelC_lsQ3_1.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ3_1.TabIndex = 45;
            this.labelC_lsQ3_1.Text = "Criterios:";
            this.labelC_lsQ3_1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ3_1.Visible = false;
            // 
            // lsQ3_1C
            // 
            this.tableLayoutPanel4.SetColumnSpan(this.lsQ3_1C, 3);
            this.lsQ3_1C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ3_1C.Location = new System.Drawing.Point(83, 59);
            this.lsQ3_1C.Multiline = true;
            this.lsQ3_1C.Name = "lsQ3_1C";
            this.lsQ3_1C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ3_1C.Size = new System.Drawing.Size(689, 60);
            this.lsQ3_1C.TabIndex = 32;
            this.lsQ3_1C.Visible = false;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.tableLayoutPanel4.SetColumnSpan(this.label35, 2);
            this.label35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label35.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label35.Location = new System.Drawing.Point(43, 508);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(664, 36);
            this.label35.TabIndex = 17;
            this.label35.Text = "¿Las entradas y salidas estan protegidas de los posibles niveles de sobre-tensión" +
    " y sobre-corriente?";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.tableLayoutPanel4.SetColumnSpan(this.label36, 2);
            this.label36.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label36.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label36.Location = new System.Drawing.Point(43, 386);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(664, 36);
            this.label36.TabIndex = 16;
            this.label36.Text = "¿Es el sistema sencillo, por ejemplo no más de 10 entradas o salidas por canal?";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label37.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label37.Location = new System.Drawing.Point(3, 386);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(34, 36);
            this.label37.TabIndex = 13;
            this.label37.Text = "3.4.";
            this.label37.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label38.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label38.Location = new System.Drawing.Point(3, 264);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(34, 36);
            this.label38.TabIndex = 12;
            this.label38.Text = "3.3.";
            this.label38.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.tableLayoutPanel4.SetColumnSpan(this.label39, 2);
            this.label39.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label39.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(43, 142);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(664, 36);
            this.label39.TabIndex = 10;
            this.label39.Text = "¿El diseño se basa en tecnicas usadas en equipos que se han utilizado exitosament" +
    "e en el campo por mas de 5 años?";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label40.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label40.Location = new System.Drawing.Point(3, 142);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(34, 36);
            this.label40.TabIndex = 11;
            this.label40.Text = "3.2.";
            this.label40.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.tableLayoutPanel4.SetColumnSpan(this.label41, 2);
            this.label41.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label41.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(43, 20);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(664, 36);
            this.label41.TabIndex = 1;
            this.label41.Text = "¿Tiene conexión transversal entre los canales para el intercambio de cualquier in" +
    "formación distinta de la utilizada para las pruebas diagnósticas o fines de la v" +
    "otación?";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label42.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label42.Location = new System.Drawing.Point(3, 20);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(34, 36);
            this.label42.TabIndex = 9;
            this.label42.Text = "3.1.";
            this.label42.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.tableLayoutPanel4.SetColumnSpan(this.label43, 2);
            this.label43.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label43.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label43.Location = new System.Drawing.Point(43, 264);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(664, 36);
            this.label43.TabIndex = 14;
            this.label43.Text = "¿Hay experiencia de mas de 5 años con el mismo hardware utilizado en condiciones " +
    "similares?";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label44.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label44.Location = new System.Drawing.Point(3, 508);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(34, 36);
            this.label44.TabIndex = 15;
            this.label44.Text = "3.5.";
            this.label44.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // groupBox19
            // 
            this.tableLayoutPanel4.SetColumnSpan(this.groupBox19, 2);
            this.groupBox19.Controls.Add(this.lsQ3_1S);
            this.groupBox19.Controls.Add(this.lsQ3_1N);
            this.groupBox19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox19.Location = new System.Drawing.Point(723, 23);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(104, 30);
            this.groupBox19.TabIndex = 81;
            this.groupBox19.TabStop = false;
            // 
            // lsQ3_1S
            // 
            this.lsQ3_1S.AutoSize = true;
            this.lsQ3_1S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ3_1S.Location = new System.Drawing.Point(0, 8);
            this.lsQ3_1S.Name = "lsQ3_1S";
            this.lsQ3_1S.Size = new System.Drawing.Size(38, 22);
            this.lsQ3_1S.TabIndex = 67;
            this.lsQ3_1S.Text = "Si";
            this.lsQ3_1S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ3_1S.UseVisualStyleBackColor = true;
            this.lsQ3_1S.CheckedChanged += new System.EventHandler(this.updatevisibleLS3);
            // 
            // lsQ3_1N
            // 
            this.lsQ3_1N.AutoSize = true;
            this.lsQ3_1N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ3_1N.Location = new System.Drawing.Point(55, 8);
            this.lsQ3_1N.Name = "lsQ3_1N";
            this.lsQ3_1N.Size = new System.Drawing.Size(47, 22);
            this.lsQ3_1N.TabIndex = 68;
            this.lsQ3_1N.Text = "No";
            this.lsQ3_1N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ3_1N.UseVisualStyleBackColor = true;
            // 
            // Evaluacion
            // 
            this.Evaluacion.Controls.Add(this.splitContainer5);
            this.Evaluacion.Location = new System.Drawing.Point(4, 22);
            this.Evaluacion.Name = "Evaluacion";
            this.Evaluacion.Padding = new System.Windows.Forms.Padding(3);
            this.Evaluacion.Size = new System.Drawing.Size(752, 409);
            this.Evaluacion.TabIndex = 3;
            this.Evaluacion.Text = "Evaluación";
            this.Evaluacion.UseVisualStyleBackColor = true;
            // 
            // splitContainer5
            // 
            this.splitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer5.Location = new System.Drawing.Point(3, 3);
            this.splitContainer5.Name = "splitContainer5";
            this.splitContainer5.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer5.Panel1
            // 
            this.splitContainer5.Panel1.Controls.Add(this.label6);
            // 
            // splitContainer5.Panel2
            // 
            this.splitContainer5.Panel2.AutoScroll = true;
            this.splitContainer5.Panel2.Controls.Add(this.tableLayoutPanel5);
            this.splitContainer5.Size = new System.Drawing.Size(746, 403);
            this.splitContainer5.SplitterDistance = 44;
            this.splitContainer5.TabIndex = 27;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 4);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(828, 42);
            this.label6.TabIndex = 9;
            this.label6.Text = "Para el Logic Solver de la SIF, considere los siguientes factores relacionados co" +
    "n la evaluación, análisis y comentarios de los datos:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.AutoScroll = true;
            this.tableLayoutPanel5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel5.ColumnCount = 7;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel5.Controls.Add(this.label31, 0, 10);
            this.tableLayoutPanel5.Controls.Add(this.groupBox21, 4, 7);
            this.tableLayoutPanel5.Controls.Add(this.groupBox22, 4, 4);
            this.tableLayoutPanel5.Controls.Add(this.labelC_lsQ4_3, 0, 8);
            this.tableLayoutPanel5.Controls.Add(this.labelC_lsQ4_2, 0, 5);
            this.tableLayoutPanel5.Controls.Add(this.label47, 3, 10);
            this.tableLayoutPanel5.Controls.Add(this.lsQ4_3C, 2, 8);
            this.tableLayoutPanel5.Controls.Add(this.checkBox10, 6, 7);
            this.tableLayoutPanel5.Controls.Add(this.lsQ4_2C, 2, 5);
            this.tableLayoutPanel5.Controls.Add(this.labelC_lsQ4_1, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.lsQ4_1C, 2, 2);
            this.tableLayoutPanel5.Controls.Add(this.label52, 0, 7);
            this.tableLayoutPanel5.Controls.Add(this.label53, 1, 4);
            this.tableLayoutPanel5.Controls.Add(this.label54, 0, 4);
            this.tableLayoutPanel5.Controls.Add(this.label55, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.label56, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.label57, 1, 7);
            this.tableLayoutPanel5.Controls.Add(this.groupBox23, 4, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel5.MinimumSize = new System.Drawing.Size(862, 386);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.tableLayoutPanel5.RowCount = 12;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(862, 386);
            this.tableLayoutPanel5.TabIndex = 2;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.tableLayoutPanel5.SetColumnSpan(this.label31, 6);
            this.label31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label31.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label31.Location = new System.Drawing.Point(3, 404);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(824, 18);
            this.label31.TabIndex = 86;
            this.label31.Text = "  CSF Consultoría en Seguridad Funcional   Copyright ©  2019";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox21
            // 
            this.tableLayoutPanel5.SetColumnSpan(this.groupBox21, 2);
            this.groupBox21.Controls.Add(this.lsQ4_3S);
            this.groupBox21.Controls.Add(this.lsQ4_3N);
            this.groupBox21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox21.Location = new System.Drawing.Point(723, 285);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(104, 30);
            this.groupBox21.TabIndex = 83;
            this.groupBox21.TabStop = false;
            // 
            // lsQ4_3S
            // 
            this.lsQ4_3S.AutoSize = true;
            this.lsQ4_3S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ4_3S.Location = new System.Drawing.Point(0, 8);
            this.lsQ4_3S.Name = "lsQ4_3S";
            this.lsQ4_3S.Size = new System.Drawing.Size(38, 22);
            this.lsQ4_3S.TabIndex = 73;
            this.lsQ4_3S.Text = "Si";
            this.lsQ4_3S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ4_3S.UseVisualStyleBackColor = true;
            this.lsQ4_3S.CheckedChanged += new System.EventHandler(this.updatevisibleLS4);
            // 
            // lsQ4_3N
            // 
            this.lsQ4_3N.AutoSize = true;
            this.lsQ4_3N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ4_3N.Location = new System.Drawing.Point(55, 8);
            this.lsQ4_3N.Name = "lsQ4_3N";
            this.lsQ4_3N.Size = new System.Drawing.Size(47, 22);
            this.lsQ4_3N.TabIndex = 74;
            this.lsQ4_3N.Text = "No";
            this.lsQ4_3N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ4_3N.UseVisualStyleBackColor = true;
            // 
            // groupBox22
            // 
            this.tableLayoutPanel5.SetColumnSpan(this.groupBox22, 2);
            this.groupBox22.Controls.Add(this.lsQ4_2S);
            this.groupBox22.Controls.Add(this.lsQ4_2N);
            this.groupBox22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox22.Location = new System.Drawing.Point(723, 145);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(104, 48);
            this.groupBox22.TabIndex = 82;
            this.groupBox22.TabStop = false;
            // 
            // lsQ4_2S
            // 
            this.lsQ4_2S.AutoSize = true;
            this.lsQ4_2S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ4_2S.Location = new System.Drawing.Point(0, 8);
            this.lsQ4_2S.Name = "lsQ4_2S";
            this.lsQ4_2S.Size = new System.Drawing.Size(38, 22);
            this.lsQ4_2S.TabIndex = 69;
            this.lsQ4_2S.Text = "Si";
            this.lsQ4_2S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ4_2S.UseVisualStyleBackColor = true;
            this.lsQ4_2S.CheckedChanged += new System.EventHandler(this.updatevisibleLS4);
            // 
            // lsQ4_2N
            // 
            this.lsQ4_2N.AutoSize = true;
            this.lsQ4_2N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ4_2N.Location = new System.Drawing.Point(55, 8);
            this.lsQ4_2N.Name = "lsQ4_2N";
            this.lsQ4_2N.Size = new System.Drawing.Size(47, 22);
            this.lsQ4_2N.TabIndex = 70;
            this.lsQ4_2N.Text = "No";
            this.lsQ4_2N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ4_2N.UseVisualStyleBackColor = true;
            // 
            // labelC_lsQ4_3
            // 
            this.labelC_lsQ4_3.AutoSize = true;
            this.tableLayoutPanel5.SetColumnSpan(this.labelC_lsQ4_3, 2);
            this.labelC_lsQ4_3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ4_3.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ4_3.Location = new System.Drawing.Point(3, 318);
            this.labelC_lsQ4_3.Name = "labelC_lsQ4_3";
            this.labelC_lsQ4_3.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ4_3.TabIndex = 72;
            this.labelC_lsQ4_3.Text = "Criterios:";
            this.labelC_lsQ4_3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ4_3.Visible = false;
            // 
            // labelC_lsQ4_2
            // 
            this.labelC_lsQ4_2.AutoSize = true;
            this.tableLayoutPanel5.SetColumnSpan(this.labelC_lsQ4_2, 2);
            this.labelC_lsQ4_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ4_2.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ4_2.Location = new System.Drawing.Point(3, 196);
            this.labelC_lsQ4_2.Name = "labelC_lsQ4_2";
            this.labelC_lsQ4_2.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ4_2.TabIndex = 71;
            this.labelC_lsQ4_2.Text = "Criterios:";
            this.labelC_lsQ4_2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ4_2.Visible = false;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.tableLayoutPanel5.SetColumnSpan(this.label47, 4);
            this.label47.Location = new System.Drawing.Point(3, 422);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(0, 16);
            this.label47.TabIndex = 61;
            // 
            // lsQ4_3C
            // 
            this.tableLayoutPanel5.SetColumnSpan(this.lsQ4_3C, 3);
            this.lsQ4_3C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ4_3C.Location = new System.Drawing.Point(83, 321);
            this.lsQ4_3C.Multiline = true;
            this.lsQ4_3C.Name = "lsQ4_3C";
            this.lsQ4_3C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ4_3C.Size = new System.Drawing.Size(689, 60);
            this.lsQ4_3C.TabIndex = 54;
            this.lsQ4_3C.Visible = false;
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox10.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBox10.Location = new System.Drawing.Point(833, 285);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(4, 30);
            this.checkBox10.TabIndex = 52;
            this.checkBox10.UseVisualStyleBackColor = true;
            // 
            // lsQ4_2C
            // 
            this.tableLayoutPanel5.SetColumnSpan(this.lsQ4_2C, 3);
            this.lsQ4_2C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ4_2C.Location = new System.Drawing.Point(83, 199);
            this.lsQ4_2C.Multiline = true;
            this.lsQ4_2C.Name = "lsQ4_2C";
            this.lsQ4_2C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ4_2C.Size = new System.Drawing.Size(689, 60);
            this.lsQ4_2C.TabIndex = 48;
            this.lsQ4_2C.Visible = false;
            // 
            // labelC_lsQ4_1
            // 
            this.labelC_lsQ4_1.AutoSize = true;
            this.tableLayoutPanel5.SetColumnSpan(this.labelC_lsQ4_1, 2);
            this.labelC_lsQ4_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ4_1.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ4_1.Location = new System.Drawing.Point(3, 56);
            this.labelC_lsQ4_1.Name = "labelC_lsQ4_1";
            this.labelC_lsQ4_1.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ4_1.TabIndex = 45;
            this.labelC_lsQ4_1.Text = "Criterios:";
            this.labelC_lsQ4_1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ4_1.Visible = false;
            // 
            // lsQ4_1C
            // 
            this.tableLayoutPanel5.SetColumnSpan(this.lsQ4_1C, 3);
            this.lsQ4_1C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ4_1C.Location = new System.Drawing.Point(83, 59);
            this.lsQ4_1C.Multiline = true;
            this.lsQ4_1C.Name = "lsQ4_1C";
            this.lsQ4_1C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ4_1C.Size = new System.Drawing.Size(689, 60);
            this.lsQ4_1C.TabIndex = 32;
            this.lsQ4_1C.Visible = false;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label52.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label52.Location = new System.Drawing.Point(3, 282);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(34, 36);
            this.label52.TabIndex = 12;
            this.label52.Text = "4.3.";
            this.label52.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.tableLayoutPanel5.SetColumnSpan(this.label53, 2);
            this.label53.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label53.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(43, 142);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(664, 54);
            this.label53.TabIndex = 10;
            this.label53.Text = "¿Fueron consideradas las CCF en las revisiones del diseño con los resultados real" +
    "imentado en el diseño? (Se requieren las pruebas documentales de la actividad de" +
    " revisión de diseño.)";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label54.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label54.Location = new System.Drawing.Point(3, 142);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(34, 54);
            this.label54.TabIndex = 11;
            this.label54.Text = "4.2.";
            this.label54.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.tableLayoutPanel5.SetColumnSpan(this.label55, 2);
            this.label55.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label55.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(43, 20);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(664, 36);
            this.label55.TabIndex = 1;
            this.label55.Text = "¿Han sido examinados los resultados de la FMEA o FTA para establecer las fuentes " +
    "de CCF y se han determinado las fuentes de CCF que han sido eliminadas por el di" +
    "seño?";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label56.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label56.Location = new System.Drawing.Point(3, 20);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(34, 36);
            this.label56.TabIndex = 9;
            this.label56.Text = "4.1.";
            this.label56.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.tableLayoutPanel5.SetColumnSpan(this.label57, 2);
            this.label57.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label57.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label57.Location = new System.Drawing.Point(43, 282);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(664, 36);
            this.label57.TabIndex = 14;
            this.label57.Text = "¿Estan todas las fallas de campo completamente analizadas con comentarios en el d" +
    "iseño? (Se requieren las pruebas del procedimiento)";
            // 
            // groupBox23
            // 
            this.tableLayoutPanel5.SetColumnSpan(this.groupBox23, 2);
            this.groupBox23.Controls.Add(this.lsQ4_1S);
            this.groupBox23.Controls.Add(this.lsQ4_1N);
            this.groupBox23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox23.Location = new System.Drawing.Point(723, 23);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(104, 30);
            this.groupBox23.TabIndex = 81;
            this.groupBox23.TabStop = false;
            // 
            // lsQ4_1S
            // 
            this.lsQ4_1S.AutoSize = true;
            this.lsQ4_1S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ4_1S.Location = new System.Drawing.Point(0, 8);
            this.lsQ4_1S.Name = "lsQ4_1S";
            this.lsQ4_1S.Size = new System.Drawing.Size(38, 22);
            this.lsQ4_1S.TabIndex = 67;
            this.lsQ4_1S.Text = "Si";
            this.lsQ4_1S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ4_1S.UseVisualStyleBackColor = true;
            this.lsQ4_1S.CheckedChanged += new System.EventHandler(this.updatevisibleLS4);
            // 
            // lsQ4_1N
            // 
            this.lsQ4_1N.AutoSize = true;
            this.lsQ4_1N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ4_1N.Location = new System.Drawing.Point(55, 8);
            this.lsQ4_1N.Name = "lsQ4_1N";
            this.lsQ4_1N.Size = new System.Drawing.Size(47, 22);
            this.lsQ4_1N.TabIndex = 68;
            this.lsQ4_1N.Text = "No";
            this.lsQ4_1N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ4_1N.UseVisualStyleBackColor = true;
            // 
            // Procedimientos
            // 
            this.Procedimientos.Controls.Add(this.splitContainer6);
            this.Procedimientos.Location = new System.Drawing.Point(4, 22);
            this.Procedimientos.Name = "Procedimientos";
            this.Procedimientos.Padding = new System.Windows.Forms.Padding(3);
            this.Procedimientos.Size = new System.Drawing.Size(752, 409);
            this.Procedimientos.TabIndex = 4;
            this.Procedimientos.Text = "Procedimientos";
            this.Procedimientos.UseVisualStyleBackColor = true;
            // 
            // splitContainer6
            // 
            this.splitContainer6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer6.Location = new System.Drawing.Point(3, 3);
            this.splitContainer6.Name = "splitContainer6";
            this.splitContainer6.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer6.Panel1
            // 
            this.splitContainer6.Panel1.Controls.Add(this.label7);
            // 
            // splitContainer6.Panel2
            // 
            this.splitContainer6.Panel2.AutoScroll = true;
            this.splitContainer6.Panel2.Controls.Add(this.tableLayoutPanel6);
            this.splitContainer6.Size = new System.Drawing.Size(746, 403);
            this.splitContainer6.SplitterDistance = 44;
            this.splitContainer6.TabIndex = 27;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 4);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(828, 42);
            this.label7.TabIndex = 9;
            this.label7.Text = "Para el Logic Solver de la SIF, considere los siguientes factores relacionados co" +
    "n los procedimientos y el error humano:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.AutoScroll = true;
            this.tableLayoutPanel6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel6.ColumnCount = 7;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel6.Controls.Add(this.lsQ5_7C, 2, 20);
            this.tableLayoutPanel6.Controls.Add(this.groupBox1, 4, 19);
            this.tableLayoutPanel6.Controls.Add(this.label8, 1, 19);
            this.tableLayoutPanel6.Controls.Add(this.labelC_lsQ5_7, 0, 20);
            this.tableLayoutPanel6.Controls.Add(this.label22, 0, 19);
            this.tableLayoutPanel6.Controls.Add(this.lsQ5_6C, 2, 17);
            this.tableLayoutPanel6.Controls.Add(this.labelC_lsQ5_6, 0, 17);
            this.tableLayoutPanel6.Controls.Add(this.groupBox5, 4, 16);
            this.tableLayoutPanel6.Controls.Add(this.label30, 1, 16);
            this.tableLayoutPanel6.Controls.Add(this.label32, 0, 16);
            this.tableLayoutPanel6.Controls.Add(this.label34, 0, 22);
            this.tableLayoutPanel6.Controls.Add(this.groupBox6, 4, 13);
            this.tableLayoutPanel6.Controls.Add(this.groupBox20, 4, 10);
            this.tableLayoutPanel6.Controls.Add(this.groupBox24, 4, 7);
            this.tableLayoutPanel6.Controls.Add(this.groupBox25, 4, 4);
            this.tableLayoutPanel6.Controls.Add(this.labelC_lsQ5_5, 0, 14);
            this.tableLayoutPanel6.Controls.Add(this.labelC_lsQ5_4, 0, 11);
            this.tableLayoutPanel6.Controls.Add(this.labelC_lsQ5_3, 0, 8);
            this.tableLayoutPanel6.Controls.Add(this.labelC_lsQ5_2, 0, 5);
            this.tableLayoutPanel6.Controls.Add(this.lsQ5_5C, 2, 14);
            this.tableLayoutPanel6.Controls.Add(this.checkBox8, 6, 13);
            this.tableLayoutPanel6.Controls.Add(this.label50, 3, 22);
            this.tableLayoutPanel6.Controls.Add(this.lsQ5_4C, 2, 11);
            this.tableLayoutPanel6.Controls.Add(this.checkBox9, 6, 10);
            this.tableLayoutPanel6.Controls.Add(this.lsQ5_3C, 2, 8);
            this.tableLayoutPanel6.Controls.Add(this.checkBox11, 6, 7);
            this.tableLayoutPanel6.Controls.Add(this.lsQ5_2C, 2, 5);
            this.tableLayoutPanel6.Controls.Add(this.labelC_lsQ5_1, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.lsQ5_1C, 2, 2);
            this.tableLayoutPanel6.Controls.Add(this.label58, 1, 13);
            this.tableLayoutPanel6.Controls.Add(this.label59, 1, 10);
            this.tableLayoutPanel6.Controls.Add(this.label60, 0, 10);
            this.tableLayoutPanel6.Controls.Add(this.label61, 0, 7);
            this.tableLayoutPanel6.Controls.Add(this.label62, 1, 4);
            this.tableLayoutPanel6.Controls.Add(this.label63, 0, 4);
            this.tableLayoutPanel6.Controls.Add(this.label64, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.label65, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.label66, 1, 7);
            this.tableLayoutPanel6.Controls.Add(this.label67, 0, 13);
            this.tableLayoutPanel6.Controls.Add(this.groupBox26, 4, 1);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel6.MinimumSize = new System.Drawing.Size(862, 386);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.tableLayoutPanel6.RowCount = 24;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(862, 386);
            this.tableLayoutPanel6.TabIndex = 2;
            // 
            // lsQ5_7C
            // 
            this.tableLayoutPanel6.SetColumnSpan(this.lsQ5_7C, 3);
            this.lsQ5_7C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ5_7C.Location = new System.Drawing.Point(83, 899);
            this.lsQ5_7C.Multiline = true;
            this.lsQ5_7C.Name = "lsQ5_7C";
            this.lsQ5_7C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ5_7C.Size = new System.Drawing.Size(689, 60);
            this.lsQ5_7C.TabIndex = 96;
            this.lsQ5_7C.Visible = false;
            // 
            // groupBox1
            // 
            this.tableLayoutPanel6.SetColumnSpan(this.groupBox1, 2);
            this.groupBox1.Controls.Add(this.lsQ5_7N);
            this.groupBox1.Controls.Add(this.lsQ5_7S);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(723, 863);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(104, 30);
            this.groupBox1.TabIndex = 95;
            this.groupBox1.TabStop = false;
            // 
            // lsQ5_7N
            // 
            this.lsQ5_7N.AutoSize = true;
            this.lsQ5_7N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ5_7N.Location = new System.Drawing.Point(55, 8);
            this.lsQ5_7N.Name = "lsQ5_7N";
            this.lsQ5_7N.Size = new System.Drawing.Size(47, 22);
            this.lsQ5_7N.TabIndex = 80;
            this.lsQ5_7N.Text = "No";
            this.lsQ5_7N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ5_7N.UseVisualStyleBackColor = true;
            // 
            // lsQ5_7S
            // 
            this.lsQ5_7S.AutoSize = true;
            this.lsQ5_7S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ5_7S.Location = new System.Drawing.Point(0, 8);
            this.lsQ5_7S.Name = "lsQ5_7S";
            this.lsQ5_7S.Size = new System.Drawing.Size(38, 22);
            this.lsQ5_7S.TabIndex = 78;
            this.lsQ5_7S.Text = "Si";
            this.lsQ5_7S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ5_7S.UseVisualStyleBackColor = true;
            this.lsQ5_7S.CheckedChanged += new System.EventHandler(this.updatevisibleLS5);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.tableLayoutPanel6.SetColumnSpan(this.label8, 2);
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(43, 860);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(664, 36);
            this.label8.TabIndex = 94;
            this.label8.Text = "¿El sistema tiene cobertura de diagnóstico alto (>99 %) y los informes de fallas " +
    "al nivel de un módulo reemplazable en campo?";
            // 
            // labelC_lsQ5_7
            // 
            this.labelC_lsQ5_7.AutoSize = true;
            this.tableLayoutPanel6.SetColumnSpan(this.labelC_lsQ5_7, 2);
            this.labelC_lsQ5_7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ5_7.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ5_7.Location = new System.Drawing.Point(3, 896);
            this.labelC_lsQ5_7.Name = "labelC_lsQ5_7";
            this.labelC_lsQ5_7.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ5_7.TabIndex = 93;
            this.labelC_lsQ5_7.Text = "Criterios:";
            this.labelC_lsQ5_7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ5_7.Visible = false;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label22.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label22.Location = new System.Drawing.Point(3, 860);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(34, 36);
            this.label22.TabIndex = 92;
            this.label22.Text = "5.7.";
            this.label22.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lsQ5_6C
            // 
            this.tableLayoutPanel6.SetColumnSpan(this.lsQ5_6C, 3);
            this.lsQ5_6C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ5_6C.Location = new System.Drawing.Point(83, 777);
            this.lsQ5_6C.Multiline = true;
            this.lsQ5_6C.Name = "lsQ5_6C";
            this.lsQ5_6C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ5_6C.Size = new System.Drawing.Size(689, 60);
            this.lsQ5_6C.TabIndex = 91;
            this.lsQ5_6C.Visible = false;
            // 
            // labelC_lsQ5_6
            // 
            this.labelC_lsQ5_6.AutoSize = true;
            this.tableLayoutPanel6.SetColumnSpan(this.labelC_lsQ5_6, 2);
            this.labelC_lsQ5_6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ5_6.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ5_6.Location = new System.Drawing.Point(3, 774);
            this.labelC_lsQ5_6.Name = "labelC_lsQ5_6";
            this.labelC_lsQ5_6.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ5_6.TabIndex = 90;
            this.labelC_lsQ5_6.Text = "Criterios:";
            this.labelC_lsQ5_6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ5_6.Visible = false;
            // 
            // groupBox5
            // 
            this.tableLayoutPanel6.SetColumnSpan(this.groupBox5, 2);
            this.groupBox5.Controls.Add(this.lsQ5_6N);
            this.groupBox5.Controls.Add(this.lsQ5_6S);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(723, 741);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(104, 30);
            this.groupBox5.TabIndex = 89;
            this.groupBox5.TabStop = false;
            // 
            // lsQ5_6N
            // 
            this.lsQ5_6N.AutoSize = true;
            this.lsQ5_6N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ5_6N.Location = new System.Drawing.Point(55, 8);
            this.lsQ5_6N.Name = "lsQ5_6N";
            this.lsQ5_6N.Size = new System.Drawing.Size(47, 22);
            this.lsQ5_6N.TabIndex = 80;
            this.lsQ5_6N.Text = "No";
            this.lsQ5_6N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ5_6N.UseVisualStyleBackColor = true;
            // 
            // lsQ5_6S
            // 
            this.lsQ5_6S.AutoSize = true;
            this.lsQ5_6S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ5_6S.Location = new System.Drawing.Point(0, 8);
            this.lsQ5_6S.Name = "lsQ5_6S";
            this.lsQ5_6S.Size = new System.Drawing.Size(38, 22);
            this.lsQ5_6S.TabIndex = 78;
            this.lsQ5_6S.Text = "Si";
            this.lsQ5_6S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ5_6S.UseVisualStyleBackColor = true;
            this.lsQ5_6S.CheckedChanged += new System.EventHandler(this.updatevisibleLS5);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.tableLayoutPanel6.SetColumnSpan(this.label30, 2);
            this.label30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label30.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label30.Location = new System.Drawing.Point(43, 738);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(664, 36);
            this.label30.TabIndex = 88;
            this.label30.Text = "¿El sistema tiene cobertura de diagnóstico medio (90 % a 99 %) y los informes de " +
    "fallas al nivel de un módulo reempazable en campo?";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label32.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label32.Location = new System.Drawing.Point(3, 738);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(34, 36);
            this.label32.TabIndex = 87;
            this.label32.Text = "5.6.";
            this.label32.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.tableLayoutPanel6.SetColumnSpan(this.label34, 6);
            this.label34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label34.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label34.Location = new System.Drawing.Point(3, 982);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(824, 18);
            this.label34.TabIndex = 86;
            this.label34.Text = "  CSF Consultoría en Seguridad Funcional   Copyright ©  2019";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox6
            // 
            this.tableLayoutPanel6.SetColumnSpan(this.groupBox6, 2);
            this.groupBox6.Controls.Add(this.lsQ5_5N);
            this.groupBox6.Controls.Add(this.lsQ5_5S);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox6.Location = new System.Drawing.Point(723, 619);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(104, 30);
            this.groupBox6.TabIndex = 85;
            this.groupBox6.TabStop = false;
            // 
            // lsQ5_5N
            // 
            this.lsQ5_5N.AutoSize = true;
            this.lsQ5_5N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ5_5N.Location = new System.Drawing.Point(55, 8);
            this.lsQ5_5N.Name = "lsQ5_5N";
            this.lsQ5_5N.Size = new System.Drawing.Size(47, 22);
            this.lsQ5_5N.TabIndex = 80;
            this.lsQ5_5N.Text = "No";
            this.lsQ5_5N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ5_5N.UseVisualStyleBackColor = true;
            // 
            // lsQ5_5S
            // 
            this.lsQ5_5S.AutoSize = true;
            this.lsQ5_5S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ5_5S.Location = new System.Drawing.Point(0, 8);
            this.lsQ5_5S.Name = "lsQ5_5S";
            this.lsQ5_5S.Size = new System.Drawing.Size(38, 22);
            this.lsQ5_5S.TabIndex = 78;
            this.lsQ5_5S.Text = "Si";
            this.lsQ5_5S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ5_5S.UseVisualStyleBackColor = true;
            this.lsQ5_5S.CheckedChanged += new System.EventHandler(this.updatevisibleLS5);
            // 
            // groupBox20
            // 
            this.tableLayoutPanel6.SetColumnSpan(this.groupBox20, 2);
            this.groupBox20.Controls.Add(this.lsQ5_4N);
            this.groupBox20.Controls.Add(this.lsQ5_4S);
            this.groupBox20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox20.Location = new System.Drawing.Point(723, 479);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(104, 48);
            this.groupBox20.TabIndex = 84;
            this.groupBox20.TabStop = false;
            // 
            // lsQ5_4N
            // 
            this.lsQ5_4N.AutoSize = true;
            this.lsQ5_4N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ5_4N.Location = new System.Drawing.Point(55, 8);
            this.lsQ5_4N.Name = "lsQ5_4N";
            this.lsQ5_4N.Size = new System.Drawing.Size(47, 22);
            this.lsQ5_4N.TabIndex = 79;
            this.lsQ5_4N.Text = "No";
            this.lsQ5_4N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ5_4N.UseVisualStyleBackColor = true;
            // 
            // lsQ5_4S
            // 
            this.lsQ5_4S.AutoSize = true;
            this.lsQ5_4S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ5_4S.Location = new System.Drawing.Point(0, 8);
            this.lsQ5_4S.Name = "lsQ5_4S";
            this.lsQ5_4S.Size = new System.Drawing.Size(38, 22);
            this.lsQ5_4S.TabIndex = 77;
            this.lsQ5_4S.Text = "Si";
            this.lsQ5_4S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ5_4S.UseVisualStyleBackColor = true;
            this.lsQ5_4S.CheckedChanged += new System.EventHandler(this.updatevisibleLS5);
            // 
            // groupBox24
            // 
            this.tableLayoutPanel6.SetColumnSpan(this.groupBox24, 2);
            this.groupBox24.Controls.Add(this.lsQ5_3S);
            this.groupBox24.Controls.Add(this.lsQ5_3N);
            this.groupBox24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox24.Location = new System.Drawing.Point(723, 339);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(104, 48);
            this.groupBox24.TabIndex = 83;
            this.groupBox24.TabStop = false;
            // 
            // lsQ5_3S
            // 
            this.lsQ5_3S.AutoSize = true;
            this.lsQ5_3S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ5_3S.Location = new System.Drawing.Point(0, 8);
            this.lsQ5_3S.Name = "lsQ5_3S";
            this.lsQ5_3S.Size = new System.Drawing.Size(38, 22);
            this.lsQ5_3S.TabIndex = 73;
            this.lsQ5_3S.Text = "Si";
            this.lsQ5_3S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ5_3S.UseVisualStyleBackColor = true;
            this.lsQ5_3S.CheckedChanged += new System.EventHandler(this.updatevisibleLS5);
            // 
            // lsQ5_3N
            // 
            this.lsQ5_3N.AutoSize = true;
            this.lsQ5_3N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ5_3N.Location = new System.Drawing.Point(55, 8);
            this.lsQ5_3N.Name = "lsQ5_3N";
            this.lsQ5_3N.Size = new System.Drawing.Size(47, 22);
            this.lsQ5_3N.TabIndex = 74;
            this.lsQ5_3N.Text = "No";
            this.lsQ5_3N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ5_3N.UseVisualStyleBackColor = true;
            // 
            // groupBox25
            // 
            this.tableLayoutPanel6.SetColumnSpan(this.groupBox25, 2);
            this.groupBox25.Controls.Add(this.lsQ5_2S);
            this.groupBox25.Controls.Add(this.lsQ5_2N);
            this.groupBox25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox25.Location = new System.Drawing.Point(723, 163);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(104, 84);
            this.groupBox25.TabIndex = 82;
            this.groupBox25.TabStop = false;
            // 
            // lsQ5_2S
            // 
            this.lsQ5_2S.AutoSize = true;
            this.lsQ5_2S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ5_2S.Location = new System.Drawing.Point(0, 8);
            this.lsQ5_2S.Name = "lsQ5_2S";
            this.lsQ5_2S.Size = new System.Drawing.Size(38, 22);
            this.lsQ5_2S.TabIndex = 69;
            this.lsQ5_2S.Text = "Si";
            this.lsQ5_2S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ5_2S.UseVisualStyleBackColor = true;
            this.lsQ5_2S.CheckedChanged += new System.EventHandler(this.updatevisibleLS5);
            // 
            // lsQ5_2N
            // 
            this.lsQ5_2N.AutoSize = true;
            this.lsQ5_2N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ5_2N.Location = new System.Drawing.Point(55, 8);
            this.lsQ5_2N.Name = "lsQ5_2N";
            this.lsQ5_2N.Size = new System.Drawing.Size(47, 22);
            this.lsQ5_2N.TabIndex = 70;
            this.lsQ5_2N.Text = "No";
            this.lsQ5_2N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ5_2N.UseVisualStyleBackColor = true;
            // 
            // labelC_lsQ5_5
            // 
            this.labelC_lsQ5_5.AutoSize = true;
            this.tableLayoutPanel6.SetColumnSpan(this.labelC_lsQ5_5, 2);
            this.labelC_lsQ5_5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ5_5.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ5_5.Location = new System.Drawing.Point(3, 652);
            this.labelC_lsQ5_5.Name = "labelC_lsQ5_5";
            this.labelC_lsQ5_5.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ5_5.TabIndex = 76;
            this.labelC_lsQ5_5.Text = "Criterios:";
            this.labelC_lsQ5_5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ5_5.Visible = false;
            // 
            // labelC_lsQ5_4
            // 
            this.labelC_lsQ5_4.AutoSize = true;
            this.tableLayoutPanel6.SetColumnSpan(this.labelC_lsQ5_4, 2);
            this.labelC_lsQ5_4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ5_4.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ5_4.Location = new System.Drawing.Point(3, 530);
            this.labelC_lsQ5_4.Name = "labelC_lsQ5_4";
            this.labelC_lsQ5_4.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ5_4.TabIndex = 75;
            this.labelC_lsQ5_4.Text = "Criterios:";
            this.labelC_lsQ5_4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ5_4.Visible = false;
            // 
            // labelC_lsQ5_3
            // 
            this.labelC_lsQ5_3.AutoSize = true;
            this.tableLayoutPanel6.SetColumnSpan(this.labelC_lsQ5_3, 2);
            this.labelC_lsQ5_3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ5_3.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ5_3.Location = new System.Drawing.Point(3, 390);
            this.labelC_lsQ5_3.Name = "labelC_lsQ5_3";
            this.labelC_lsQ5_3.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ5_3.TabIndex = 72;
            this.labelC_lsQ5_3.Text = "Criterios:";
            this.labelC_lsQ5_3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ5_3.Visible = false;
            // 
            // labelC_lsQ5_2
            // 
            this.labelC_lsQ5_2.AutoSize = true;
            this.tableLayoutPanel6.SetColumnSpan(this.labelC_lsQ5_2, 2);
            this.labelC_lsQ5_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ5_2.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ5_2.Location = new System.Drawing.Point(3, 250);
            this.labelC_lsQ5_2.Name = "labelC_lsQ5_2";
            this.labelC_lsQ5_2.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ5_2.TabIndex = 71;
            this.labelC_lsQ5_2.Text = "Criterios:";
            this.labelC_lsQ5_2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ5_2.Visible = false;
            // 
            // lsQ5_5C
            // 
            this.tableLayoutPanel6.SetColumnSpan(this.lsQ5_5C, 3);
            this.lsQ5_5C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ5_5C.Location = new System.Drawing.Point(83, 655);
            this.lsQ5_5C.Multiline = true;
            this.lsQ5_5C.Name = "lsQ5_5C";
            this.lsQ5_5C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ5_5C.Size = new System.Drawing.Size(689, 60);
            this.lsQ5_5C.TabIndex = 66;
            this.lsQ5_5C.Visible = false;
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox8.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBox8.Location = new System.Drawing.Point(833, 619);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(4, 30);
            this.checkBox8.TabIndex = 64;
            this.checkBox8.UseVisualStyleBackColor = true;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.tableLayoutPanel6.SetColumnSpan(this.label50, 4);
            this.label50.Location = new System.Drawing.Point(3, 1000);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(0, 16);
            this.label50.TabIndex = 61;
            // 
            // lsQ5_4C
            // 
            this.tableLayoutPanel6.SetColumnSpan(this.lsQ5_4C, 3);
            this.lsQ5_4C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ5_4C.Location = new System.Drawing.Point(83, 533);
            this.lsQ5_4C.Multiline = true;
            this.lsQ5_4C.Name = "lsQ5_4C";
            this.lsQ5_4C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ5_4C.Size = new System.Drawing.Size(689, 60);
            this.lsQ5_4C.TabIndex = 60;
            this.lsQ5_4C.Visible = false;
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox9.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBox9.Location = new System.Drawing.Point(833, 479);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(4, 48);
            this.checkBox9.TabIndex = 58;
            this.checkBox9.UseVisualStyleBackColor = true;
            // 
            // lsQ5_3C
            // 
            this.tableLayoutPanel6.SetColumnSpan(this.lsQ5_3C, 3);
            this.lsQ5_3C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ5_3C.Location = new System.Drawing.Point(83, 393);
            this.lsQ5_3C.Multiline = true;
            this.lsQ5_3C.Name = "lsQ5_3C";
            this.lsQ5_3C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ5_3C.Size = new System.Drawing.Size(689, 60);
            this.lsQ5_3C.TabIndex = 54;
            this.lsQ5_3C.Visible = false;
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox11.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBox11.Location = new System.Drawing.Point(833, 339);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(4, 48);
            this.checkBox11.TabIndex = 52;
            this.checkBox11.UseVisualStyleBackColor = true;
            // 
            // lsQ5_2C
            // 
            this.tableLayoutPanel6.SetColumnSpan(this.lsQ5_2C, 3);
            this.lsQ5_2C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ5_2C.Location = new System.Drawing.Point(83, 253);
            this.lsQ5_2C.Multiline = true;
            this.lsQ5_2C.Name = "lsQ5_2C";
            this.lsQ5_2C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ5_2C.Size = new System.Drawing.Size(689, 60);
            this.lsQ5_2C.TabIndex = 48;
            this.lsQ5_2C.Visible = false;
            // 
            // labelC_lsQ5_1
            // 
            this.labelC_lsQ5_1.AutoSize = true;
            this.tableLayoutPanel6.SetColumnSpan(this.labelC_lsQ5_1, 2);
            this.labelC_lsQ5_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ5_1.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ5_1.Location = new System.Drawing.Point(3, 74);
            this.labelC_lsQ5_1.Name = "labelC_lsQ5_1";
            this.labelC_lsQ5_1.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ5_1.TabIndex = 45;
            this.labelC_lsQ5_1.Text = "Criterios:";
            this.labelC_lsQ5_1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ5_1.Visible = false;
            // 
            // lsQ5_1C
            // 
            this.tableLayoutPanel6.SetColumnSpan(this.lsQ5_1C, 3);
            this.lsQ5_1C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ5_1C.Location = new System.Drawing.Point(83, 77);
            this.lsQ5_1C.Multiline = true;
            this.lsQ5_1C.Name = "lsQ5_1C";
            this.lsQ5_1C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ5_1C.Size = new System.Drawing.Size(689, 60);
            this.lsQ5_1C.TabIndex = 32;
            this.lsQ5_1C.Visible = false;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.tableLayoutPanel6.SetColumnSpan(this.label58, 2);
            this.label58.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label58.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label58.Location = new System.Drawing.Point(43, 616);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(664, 36);
            this.label58.TabIndex = 17;
            this.label58.Text = "¿El sistema tiene cobertura de diagnóstico bajo (60%  a 90%) y los informes de fa" +
    "llas al nivel de un módulo reemplazable en campo?";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.tableLayoutPanel6.SetColumnSpan(this.label59, 2);
            this.label59.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label59.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label59.Location = new System.Drawing.Point(43, 476);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(664, 54);
            this.label59.TabIndex = 16;
            this.label59.Text = resources.GetString("label59.Text");
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label60.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label60.Location = new System.Drawing.Point(3, 476);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(34, 54);
            this.label60.TabIndex = 13;
            this.label60.Text = "5.4.";
            this.label60.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label61.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label61.Location = new System.Drawing.Point(3, 336);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(34, 54);
            this.label61.TabIndex = 12;
            this.label61.Text = "5.3.";
            this.label61.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.tableLayoutPanel6.SetColumnSpan(this.label62, 2);
            this.label62.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label62.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(43, 160);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(664, 90);
            this.label62.TabIndex = 10;
            this.label62.Text = resources.GetString("label62.Text");
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label63.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label63.Location = new System.Drawing.Point(3, 160);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(34, 90);
            this.label63.TabIndex = 11;
            this.label63.Text = "5.2.";
            this.label63.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.tableLayoutPanel6.SetColumnSpan(this.label64, 2);
            this.label64.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label64.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(43, 20);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(664, 54);
            this.label64.TabIndex = 1;
            this.label64.Text = resources.GetString("label64.Text");
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label65.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label65.Location = new System.Drawing.Point(3, 20);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(34, 54);
            this.label65.TabIndex = 9;
            this.label65.Text = "5.1.";
            this.label65.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.tableLayoutPanel6.SetColumnSpan(this.label66, 2);
            this.label66.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label66.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label66.Location = new System.Drawing.Point(43, 336);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(664, 54);
            this.label66.TabIndex = 14;
            this.label66.Text = resources.GetString("label66.Text");
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label67.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label67.Location = new System.Drawing.Point(3, 616);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(34, 36);
            this.label67.TabIndex = 15;
            this.label67.Text = "5.5.";
            this.label67.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // groupBox26
            // 
            this.tableLayoutPanel6.SetColumnSpan(this.groupBox26, 2);
            this.groupBox26.Controls.Add(this.lsQ5_1S);
            this.groupBox26.Controls.Add(this.lsQ5_1N);
            this.groupBox26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox26.Location = new System.Drawing.Point(723, 23);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(104, 48);
            this.groupBox26.TabIndex = 81;
            this.groupBox26.TabStop = false;
            // 
            // lsQ5_1S
            // 
            this.lsQ5_1S.AutoSize = true;
            this.lsQ5_1S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ5_1S.Location = new System.Drawing.Point(0, 8);
            this.lsQ5_1S.Name = "lsQ5_1S";
            this.lsQ5_1S.Size = new System.Drawing.Size(38, 22);
            this.lsQ5_1S.TabIndex = 67;
            this.lsQ5_1S.Text = "Si";
            this.lsQ5_1S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ5_1S.UseVisualStyleBackColor = true;
            this.lsQ5_1S.CheckedChanged += new System.EventHandler(this.updatevisibleLS5);
            // 
            // lsQ5_1N
            // 
            this.lsQ5_1N.AutoSize = true;
            this.lsQ5_1N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ5_1N.Location = new System.Drawing.Point(55, 8);
            this.lsQ5_1N.Name = "lsQ5_1N";
            this.lsQ5_1N.Size = new System.Drawing.Size(47, 22);
            this.lsQ5_1N.TabIndex = 68;
            this.lsQ5_1N.Text = "No";
            this.lsQ5_1N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ5_1N.UseVisualStyleBackColor = true;
            // 
            // Entrenamiento
            // 
            this.Entrenamiento.Controls.Add(this.splitContainer7);
            this.Entrenamiento.Location = new System.Drawing.Point(4, 22);
            this.Entrenamiento.Name = "Entrenamiento";
            this.Entrenamiento.Padding = new System.Windows.Forms.Padding(3);
            this.Entrenamiento.Size = new System.Drawing.Size(752, 409);
            this.Entrenamiento.TabIndex = 5;
            this.Entrenamiento.Text = "Entrenamiento";
            this.Entrenamiento.UseVisualStyleBackColor = true;
            // 
            // splitContainer7
            // 
            this.splitContainer7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer7.Location = new System.Drawing.Point(3, 3);
            this.splitContainer7.Name = "splitContainer7";
            this.splitContainer7.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer7.Panel1
            // 
            this.splitContainer7.Panel1.Controls.Add(this.label10);
            // 
            // splitContainer7.Panel2
            // 
            this.splitContainer7.Panel2.AutoScroll = true;
            this.splitContainer7.Panel2.Controls.Add(this.tableLayoutPanel7);
            this.splitContainer7.Size = new System.Drawing.Size(746, 403);
            this.splitContainer7.SplitterDistance = 44;
            this.splitContainer7.TabIndex = 26;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(3, 4);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(828, 42);
            this.label10.TabIndex = 9;
            this.label10.Text = "Para el Logic Solver de la SIF, considere los siguientes factores relacionados co" +
    "n las competencias del personal, su capacitación y la cultura de seguridad de la" +
    " empresa:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.AutoScroll = true;
            this.tableLayoutPanel7.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel7.ColumnCount = 7;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel7.Controls.Add(this.label29, 0, 7);
            this.tableLayoutPanel7.Controls.Add(this.groupBox28, 4, 4);
            this.tableLayoutPanel7.Controls.Add(this.labelC_lsQ6_2, 0, 5);
            this.tableLayoutPanel7.Controls.Add(this.lsQ6_2C, 2, 5);
            this.tableLayoutPanel7.Controls.Add(this.labelC_lsQ6_1, 0, 2);
            this.tableLayoutPanel7.Controls.Add(this.lsQ6_1C, 2, 2);
            this.tableLayoutPanel7.Controls.Add(this.label51, 1, 4);
            this.tableLayoutPanel7.Controls.Add(this.label68, 0, 4);
            this.tableLayoutPanel7.Controls.Add(this.label69, 1, 1);
            this.tableLayoutPanel7.Controls.Add(this.label70, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.groupBox29, 4, 1);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel7.MinimumSize = new System.Drawing.Size(862, 386);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.tableLayoutPanel7.RowCount = 9;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(862, 386);
            this.tableLayoutPanel7.TabIndex = 2;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.tableLayoutPanel7.SetColumnSpan(this.label29, 6);
            this.label29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label29.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label29.Location = new System.Drawing.Point(3, 264);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(841, 18);
            this.label29.TabIndex = 86;
            this.label29.Text = "  CSF Consultoría en Seguridad Funcional   Copyright ©  2019";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox28
            // 
            this.tableLayoutPanel7.SetColumnSpan(this.groupBox28, 2);
            this.groupBox28.Controls.Add(this.lsQ6_2S);
            this.groupBox28.Controls.Add(this.lsQ6_2N);
            this.groupBox28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox28.Location = new System.Drawing.Point(740, 145);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Size = new System.Drawing.Size(104, 30);
            this.groupBox28.TabIndex = 82;
            this.groupBox28.TabStop = false;
            // 
            // lsQ6_2S
            // 
            this.lsQ6_2S.AutoSize = true;
            this.lsQ6_2S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ6_2S.Location = new System.Drawing.Point(0, 8);
            this.lsQ6_2S.Name = "lsQ6_2S";
            this.lsQ6_2S.Size = new System.Drawing.Size(38, 22);
            this.lsQ6_2S.TabIndex = 69;
            this.lsQ6_2S.Text = "Si";
            this.lsQ6_2S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ6_2S.UseVisualStyleBackColor = true;
            this.lsQ6_2S.CheckedChanged += new System.EventHandler(this.updatevisibleLS6);
            // 
            // lsQ6_2N
            // 
            this.lsQ6_2N.AutoSize = true;
            this.lsQ6_2N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ6_2N.Location = new System.Drawing.Point(55, 8);
            this.lsQ6_2N.Name = "lsQ6_2N";
            this.lsQ6_2N.Size = new System.Drawing.Size(47, 22);
            this.lsQ6_2N.TabIndex = 70;
            this.lsQ6_2N.Text = "No";
            this.lsQ6_2N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ6_2N.UseVisualStyleBackColor = true;
            // 
            // labelC_lsQ6_2
            // 
            this.labelC_lsQ6_2.AutoSize = true;
            this.tableLayoutPanel7.SetColumnSpan(this.labelC_lsQ6_2, 2);
            this.labelC_lsQ6_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ6_2.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ6_2.Location = new System.Drawing.Point(3, 178);
            this.labelC_lsQ6_2.Name = "labelC_lsQ6_2";
            this.labelC_lsQ6_2.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ6_2.TabIndex = 71;
            this.labelC_lsQ6_2.Text = "Criterios:";
            this.labelC_lsQ6_2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ6_2.Visible = false;
            // 
            // lsQ6_2C
            // 
            this.tableLayoutPanel7.SetColumnSpan(this.lsQ6_2C, 3);
            this.lsQ6_2C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ6_2C.Location = new System.Drawing.Point(83, 181);
            this.lsQ6_2C.Multiline = true;
            this.lsQ6_2C.Name = "lsQ6_2C";
            this.lsQ6_2C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ6_2C.Size = new System.Drawing.Size(706, 60);
            this.lsQ6_2C.TabIndex = 48;
            this.lsQ6_2C.Visible = false;
            // 
            // labelC_lsQ6_1
            // 
            this.labelC_lsQ6_1.AutoSize = true;
            this.tableLayoutPanel7.SetColumnSpan(this.labelC_lsQ6_1, 2);
            this.labelC_lsQ6_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ6_1.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ6_1.Location = new System.Drawing.Point(3, 56);
            this.labelC_lsQ6_1.Name = "labelC_lsQ6_1";
            this.labelC_lsQ6_1.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ6_1.TabIndex = 45;
            this.labelC_lsQ6_1.Text = "Criterios:";
            this.labelC_lsQ6_1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ6_1.Visible = false;
            // 
            // lsQ6_1C
            // 
            this.tableLayoutPanel7.SetColumnSpan(this.lsQ6_1C, 3);
            this.lsQ6_1C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ6_1C.Location = new System.Drawing.Point(83, 59);
            this.lsQ6_1C.Multiline = true;
            this.lsQ6_1C.Name = "lsQ6_1C";
            this.lsQ6_1C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ6_1C.Size = new System.Drawing.Size(706, 60);
            this.lsQ6_1C.TabIndex = 32;
            this.lsQ6_1C.Visible = false;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.tableLayoutPanel7.SetColumnSpan(this.label51, 2);
            this.label51.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label51.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(43, 142);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(681, 36);
            this.label51.TabIndex = 10;
            this.label51.Text = "¿Ha sido entrenado el personal de mantenimiento (con la documentación de formació" +
    "n) para comprender las causas y consecuencias de las fallas de causa común?";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label68.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label68.Location = new System.Drawing.Point(3, 142);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(34, 36);
            this.label68.TabIndex = 11;
            this.label68.Text = "6.2.";
            this.label68.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.tableLayoutPanel7.SetColumnSpan(this.label69, 2);
            this.label69.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label69.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.Location = new System.Drawing.Point(43, 20);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(681, 36);
            this.label69.TabIndex = 1;
            this.label69.Text = "¿Han sido entrenados los diseñadores (con la documentación de formación) para com" +
    "prender las causas y consecuencias de las fallas de causa común?";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label70.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label70.Location = new System.Drawing.Point(3, 20);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(34, 36);
            this.label70.TabIndex = 9;
            this.label70.Text = "6.1.";
            this.label70.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // groupBox29
            // 
            this.tableLayoutPanel7.SetColumnSpan(this.groupBox29, 2);
            this.groupBox29.Controls.Add(this.lsQ6_1S);
            this.groupBox29.Controls.Add(this.lsQ6_1N);
            this.groupBox29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox29.Location = new System.Drawing.Point(740, 23);
            this.groupBox29.Name = "groupBox29";
            this.groupBox29.Size = new System.Drawing.Size(104, 30);
            this.groupBox29.TabIndex = 81;
            this.groupBox29.TabStop = false;
            // 
            // lsQ6_1S
            // 
            this.lsQ6_1S.AutoSize = true;
            this.lsQ6_1S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ6_1S.Location = new System.Drawing.Point(0, 8);
            this.lsQ6_1S.Name = "lsQ6_1S";
            this.lsQ6_1S.Size = new System.Drawing.Size(38, 22);
            this.lsQ6_1S.TabIndex = 67;
            this.lsQ6_1S.Text = "Si";
            this.lsQ6_1S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ6_1S.UseVisualStyleBackColor = true;
            this.lsQ6_1S.CheckedChanged += new System.EventHandler(this.updatevisibleLS6);
            // 
            // lsQ6_1N
            // 
            this.lsQ6_1N.AutoSize = true;
            this.lsQ6_1N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ6_1N.Location = new System.Drawing.Point(55, 8);
            this.lsQ6_1N.Name = "lsQ6_1N";
            this.lsQ6_1N.Size = new System.Drawing.Size(47, 22);
            this.lsQ6_1N.TabIndex = 68;
            this.lsQ6_1N.Text = "No";
            this.lsQ6_1N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ6_1N.UseVisualStyleBackColor = true;
            // 
            // Ambiente
            // 
            this.Ambiente.Controls.Add(this.splitContainer15);
            this.Ambiente.Location = new System.Drawing.Point(4, 22);
            this.Ambiente.Name = "Ambiente";
            this.Ambiente.Padding = new System.Windows.Forms.Padding(3);
            this.Ambiente.Size = new System.Drawing.Size(752, 409);
            this.Ambiente.TabIndex = 6;
            this.Ambiente.Text = "Ambiente";
            this.Ambiente.UseVisualStyleBackColor = true;
            // 
            // splitContainer15
            // 
            this.splitContainer15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer15.Location = new System.Drawing.Point(3, 3);
            this.splitContainer15.Name = "splitContainer15";
            this.splitContainer15.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer15.Panel1
            // 
            this.splitContainer15.Panel1.Controls.Add(this.label45);
            // 
            // splitContainer15.Panel2
            // 
            this.splitContainer15.Panel2.AutoScroll = true;
            this.splitContainer15.Panel2.Controls.Add(this.tableLayoutPanel8);
            this.splitContainer15.Size = new System.Drawing.Size(746, 403);
            this.splitContainer15.SplitterDistance = 44;
            this.splitContainer15.TabIndex = 28;
            // 
            // label45
            // 
            this.label45.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(3, 4);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(828, 42);
            this.label45.TabIndex = 9;
            this.label45.Text = "Para el Logic Solver de la SIF, considere los siguientes factores relacionados el" +
    " control y pruebas ambientales del sistema:";
            this.label45.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.AutoScroll = true;
            this.tableLayoutPanel8.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel8.ColumnCount = 7;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel8.Controls.Add(this.label71, 0, 13);
            this.tableLayoutPanel8.Controls.Add(this.groupBox31, 4, 10);
            this.tableLayoutPanel8.Controls.Add(this.groupBox32, 4, 7);
            this.tableLayoutPanel8.Controls.Add(this.groupBox33, 4, 4);
            this.tableLayoutPanel8.Controls.Add(this.labelC_lsQ7_4, 0, 11);
            this.tableLayoutPanel8.Controls.Add(this.labelC_lsQ7_3, 0, 8);
            this.tableLayoutPanel8.Controls.Add(this.labelC_lsQ7_2, 0, 5);
            this.tableLayoutPanel8.Controls.Add(this.label76, 3, 13);
            this.tableLayoutPanel8.Controls.Add(this.lsQ7_4C, 2, 11);
            this.tableLayoutPanel8.Controls.Add(this.checkBox13, 6, 10);
            this.tableLayoutPanel8.Controls.Add(this.lsQ7_3C, 2, 8);
            this.tableLayoutPanel8.Controls.Add(this.checkBox14, 6, 7);
            this.tableLayoutPanel8.Controls.Add(this.lsQ7_2C, 2, 5);
            this.tableLayoutPanel8.Controls.Add(this.labelC_lsQ7_1, 0, 2);
            this.tableLayoutPanel8.Controls.Add(this.lsQ7_1C, 2, 2);
            this.tableLayoutPanel8.Controls.Add(this.label79, 1, 10);
            this.tableLayoutPanel8.Controls.Add(this.label80, 0, 10);
            this.tableLayoutPanel8.Controls.Add(this.label81, 0, 7);
            this.tableLayoutPanel8.Controls.Add(this.label82, 1, 4);
            this.tableLayoutPanel8.Controls.Add(this.label83, 0, 4);
            this.tableLayoutPanel8.Controls.Add(this.label84, 1, 1);
            this.tableLayoutPanel8.Controls.Add(this.label85, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.label86, 1, 7);
            this.tableLayoutPanel8.Controls.Add(this.groupBox34, 4, 1);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel8.MinimumSize = new System.Drawing.Size(862, 386);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.tableLayoutPanel8.RowCount = 15;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(862, 386);
            this.tableLayoutPanel8.TabIndex = 2;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.tableLayoutPanel8.SetColumnSpan(this.label71, 6);
            this.label71.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label71.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label71.Location = new System.Drawing.Point(3, 544);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(824, 18);
            this.label71.TabIndex = 86;
            this.label71.Text = "  CSF Consultoría en Seguridad Funcional   Copyright ©  2019";
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox31
            // 
            this.tableLayoutPanel8.SetColumnSpan(this.groupBox31, 2);
            this.groupBox31.Controls.Add(this.lsQ7_4N);
            this.groupBox31.Controls.Add(this.lsQ7_4S);
            this.groupBox31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox31.Location = new System.Drawing.Point(723, 407);
            this.groupBox31.Name = "groupBox31";
            this.groupBox31.Size = new System.Drawing.Size(104, 48);
            this.groupBox31.TabIndex = 84;
            this.groupBox31.TabStop = false;
            // 
            // lsQ7_4N
            // 
            this.lsQ7_4N.AutoSize = true;
            this.lsQ7_4N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ7_4N.Location = new System.Drawing.Point(55, 8);
            this.lsQ7_4N.Name = "lsQ7_4N";
            this.lsQ7_4N.Size = new System.Drawing.Size(47, 22);
            this.lsQ7_4N.TabIndex = 79;
            this.lsQ7_4N.Text = "No";
            this.lsQ7_4N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ7_4N.UseVisualStyleBackColor = true;
            // 
            // lsQ7_4S
            // 
            this.lsQ7_4S.AutoSize = true;
            this.lsQ7_4S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ7_4S.Location = new System.Drawing.Point(0, 8);
            this.lsQ7_4S.Name = "lsQ7_4S";
            this.lsQ7_4S.Size = new System.Drawing.Size(38, 22);
            this.lsQ7_4S.TabIndex = 77;
            this.lsQ7_4S.Text = "Si";
            this.lsQ7_4S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ7_4S.UseVisualStyleBackColor = true;
            this.lsQ7_4S.CheckedChanged += new System.EventHandler(this.updatevisibleLS7);
            // 
            // groupBox32
            // 
            this.tableLayoutPanel8.SetColumnSpan(this.groupBox32, 2);
            this.groupBox32.Controls.Add(this.lsQ7_3S);
            this.groupBox32.Controls.Add(this.lsQ7_3N);
            this.groupBox32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox32.Location = new System.Drawing.Point(723, 285);
            this.groupBox32.Name = "groupBox32";
            this.groupBox32.Size = new System.Drawing.Size(104, 30);
            this.groupBox32.TabIndex = 83;
            this.groupBox32.TabStop = false;
            // 
            // lsQ7_3S
            // 
            this.lsQ7_3S.AutoSize = true;
            this.lsQ7_3S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ7_3S.Location = new System.Drawing.Point(0, 8);
            this.lsQ7_3S.Name = "lsQ7_3S";
            this.lsQ7_3S.Size = new System.Drawing.Size(38, 22);
            this.lsQ7_3S.TabIndex = 73;
            this.lsQ7_3S.Text = "Si";
            this.lsQ7_3S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ7_3S.UseVisualStyleBackColor = true;
            this.lsQ7_3S.CheckedChanged += new System.EventHandler(this.updatevisibleLS7);
            // 
            // lsQ7_3N
            // 
            this.lsQ7_3N.AutoSize = true;
            this.lsQ7_3N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ7_3N.Location = new System.Drawing.Point(55, 8);
            this.lsQ7_3N.Name = "lsQ7_3N";
            this.lsQ7_3N.Size = new System.Drawing.Size(47, 22);
            this.lsQ7_3N.TabIndex = 74;
            this.lsQ7_3N.Text = "No";
            this.lsQ7_3N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ7_3N.UseVisualStyleBackColor = true;
            // 
            // groupBox33
            // 
            this.tableLayoutPanel8.SetColumnSpan(this.groupBox33, 2);
            this.groupBox33.Controls.Add(this.lsQ7_2S);
            this.groupBox33.Controls.Add(this.lsQ7_2N);
            this.groupBox33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox33.Location = new System.Drawing.Point(723, 145);
            this.groupBox33.Name = "groupBox33";
            this.groupBox33.Size = new System.Drawing.Size(104, 48);
            this.groupBox33.TabIndex = 82;
            this.groupBox33.TabStop = false;
            // 
            // lsQ7_2S
            // 
            this.lsQ7_2S.AutoSize = true;
            this.lsQ7_2S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ7_2S.Location = new System.Drawing.Point(0, 8);
            this.lsQ7_2S.Name = "lsQ7_2S";
            this.lsQ7_2S.Size = new System.Drawing.Size(38, 22);
            this.lsQ7_2S.TabIndex = 69;
            this.lsQ7_2S.Text = "Si";
            this.lsQ7_2S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ7_2S.UseVisualStyleBackColor = true;
            this.lsQ7_2S.CheckedChanged += new System.EventHandler(this.updatevisibleLS7);
            // 
            // lsQ7_2N
            // 
            this.lsQ7_2N.AutoSize = true;
            this.lsQ7_2N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ7_2N.Location = new System.Drawing.Point(55, 8);
            this.lsQ7_2N.Name = "lsQ7_2N";
            this.lsQ7_2N.Size = new System.Drawing.Size(47, 22);
            this.lsQ7_2N.TabIndex = 70;
            this.lsQ7_2N.Text = "No";
            this.lsQ7_2N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ7_2N.UseVisualStyleBackColor = true;
            // 
            // labelC_lsQ7_4
            // 
            this.labelC_lsQ7_4.AutoSize = true;
            this.tableLayoutPanel8.SetColumnSpan(this.labelC_lsQ7_4, 2);
            this.labelC_lsQ7_4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ7_4.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ7_4.Location = new System.Drawing.Point(3, 458);
            this.labelC_lsQ7_4.Name = "labelC_lsQ7_4";
            this.labelC_lsQ7_4.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ7_4.TabIndex = 75;
            this.labelC_lsQ7_4.Text = "Criterios:";
            this.labelC_lsQ7_4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ7_4.Visible = false;
            // 
            // labelC_lsQ7_3
            // 
            this.labelC_lsQ7_3.AutoSize = true;
            this.tableLayoutPanel8.SetColumnSpan(this.labelC_lsQ7_3, 2);
            this.labelC_lsQ7_3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ7_3.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ7_3.Location = new System.Drawing.Point(3, 318);
            this.labelC_lsQ7_3.Name = "labelC_lsQ7_3";
            this.labelC_lsQ7_3.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ7_3.TabIndex = 72;
            this.labelC_lsQ7_3.Text = "Criterios:";
            this.labelC_lsQ7_3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ7_3.Visible = false;
            // 
            // labelC_lsQ7_2
            // 
            this.labelC_lsQ7_2.AutoSize = true;
            this.tableLayoutPanel8.SetColumnSpan(this.labelC_lsQ7_2, 2);
            this.labelC_lsQ7_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ7_2.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ7_2.Location = new System.Drawing.Point(3, 196);
            this.labelC_lsQ7_2.Name = "labelC_lsQ7_2";
            this.labelC_lsQ7_2.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ7_2.TabIndex = 71;
            this.labelC_lsQ7_2.Text = "Criterios:";
            this.labelC_lsQ7_2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ7_2.Visible = false;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.tableLayoutPanel8.SetColumnSpan(this.label76, 4);
            this.label76.Location = new System.Drawing.Point(3, 562);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(0, 16);
            this.label76.TabIndex = 61;
            // 
            // lsQ7_4C
            // 
            this.tableLayoutPanel8.SetColumnSpan(this.lsQ7_4C, 3);
            this.lsQ7_4C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ7_4C.Location = new System.Drawing.Point(83, 461);
            this.lsQ7_4C.Multiline = true;
            this.lsQ7_4C.Name = "lsQ7_4C";
            this.lsQ7_4C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ7_4C.Size = new System.Drawing.Size(689, 60);
            this.lsQ7_4C.TabIndex = 60;
            this.lsQ7_4C.Visible = false;
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox13.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBox13.Location = new System.Drawing.Point(833, 407);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(4, 48);
            this.checkBox13.TabIndex = 58;
            this.checkBox13.UseVisualStyleBackColor = true;
            // 
            // lsQ7_3C
            // 
            this.tableLayoutPanel8.SetColumnSpan(this.lsQ7_3C, 3);
            this.lsQ7_3C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ7_3C.Location = new System.Drawing.Point(83, 321);
            this.lsQ7_3C.Multiline = true;
            this.lsQ7_3C.Name = "lsQ7_3C";
            this.lsQ7_3C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ7_3C.Size = new System.Drawing.Size(689, 60);
            this.lsQ7_3C.TabIndex = 54;
            this.lsQ7_3C.Visible = false;
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox14.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBox14.Location = new System.Drawing.Point(833, 285);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(4, 30);
            this.checkBox14.TabIndex = 52;
            this.checkBox14.UseVisualStyleBackColor = true;
            // 
            // lsQ7_2C
            // 
            this.tableLayoutPanel8.SetColumnSpan(this.lsQ7_2C, 3);
            this.lsQ7_2C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ7_2C.Location = new System.Drawing.Point(83, 199);
            this.lsQ7_2C.Multiline = true;
            this.lsQ7_2C.Name = "lsQ7_2C";
            this.lsQ7_2C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ7_2C.Size = new System.Drawing.Size(689, 60);
            this.lsQ7_2C.TabIndex = 48;
            this.lsQ7_2C.Visible = false;
            // 
            // labelC_lsQ7_1
            // 
            this.labelC_lsQ7_1.AutoSize = true;
            this.tableLayoutPanel8.SetColumnSpan(this.labelC_lsQ7_1, 2);
            this.labelC_lsQ7_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_lsQ7_1.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_lsQ7_1.Location = new System.Drawing.Point(3, 57);
            this.labelC_lsQ7_1.Name = "labelC_lsQ7_1";
            this.labelC_lsQ7_1.Size = new System.Drawing.Size(74, 66);
            this.labelC_lsQ7_1.TabIndex = 45;
            this.labelC_lsQ7_1.Text = "Criterios:";
            this.labelC_lsQ7_1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_lsQ7_1.Visible = false;
            // 
            // lsQ7_1C
            // 
            this.tableLayoutPanel8.SetColumnSpan(this.lsQ7_1C, 3);
            this.lsQ7_1C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsQ7_1C.Location = new System.Drawing.Point(83, 60);
            this.lsQ7_1C.Multiline = true;
            this.lsQ7_1C.Name = "lsQ7_1C";
            this.lsQ7_1C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.lsQ7_1C.Size = new System.Drawing.Size(689, 60);
            this.lsQ7_1C.TabIndex = 32;
            this.lsQ7_1C.Visible = false;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.tableLayoutPanel8.SetColumnSpan(this.label79, 2);
            this.label79.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label79.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label79.Location = new System.Drawing.Point(43, 404);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(664, 54);
            this.label79.TabIndex = 16;
            this.label79.Text = resources.GetString("label79.Text");
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label80.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label80.Location = new System.Drawing.Point(3, 404);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(34, 54);
            this.label80.TabIndex = 13;
            this.label80.Text = "7.4.";
            this.label80.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label81.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label81.Location = new System.Drawing.Point(3, 282);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(34, 36);
            this.label81.TabIndex = 12;
            this.label81.Text = "7.3.";
            this.label81.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.tableLayoutPanel8.SetColumnSpan(this.label82, 2);
            this.label82.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label82.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.Location = new System.Drawing.Point(43, 142);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(664, 54);
            this.label82.TabIndex = 10;
            this.label82.Text = "¿El sistema puede operar siempre dentro del rango de temperatura, humedad, corros" +
    "ión, polvo, vibraciones, etc, sobre el cual ha sido probado sin el uso del contr" +
    "ol del medio ambiente externo?";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label83.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label83.Location = new System.Drawing.Point(3, 142);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(34, 54);
            this.label83.TabIndex = 11;
            this.label83.Text = "7.2.";
            this.label83.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.tableLayoutPanel8.SetColumnSpan(this.label84, 2);
            this.label84.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label84.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.Location = new System.Drawing.Point(43, 21);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(664, 36);
            this.label84.TabIndex = 1;
            this.label84.Text = "¿Esta limitado el acceso del personal (por ejemplo, gabinetes cerrados, posición " +
    "inaccesible)?";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label85.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label85.Location = new System.Drawing.Point(3, 21);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(34, 36);
            this.label85.TabIndex = 9;
            this.label85.Text = "7.1.";
            this.label85.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.tableLayoutPanel8.SetColumnSpan(this.label86, 2);
            this.label86.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label86.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label86.Location = new System.Drawing.Point(43, 282);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(664, 36);
            this.label86.TabIndex = 14;
            this.label86.Text = "¿Estan todos los cables de señal y alimentación separados en todas las posiciones" +
    "?";
            // 
            // groupBox34
            // 
            this.tableLayoutPanel8.SetColumnSpan(this.groupBox34, 2);
            this.groupBox34.Controls.Add(this.lsQ7_1S);
            this.groupBox34.Controls.Add(this.lsQ7_1N);
            this.groupBox34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox34.Location = new System.Drawing.Point(723, 24);
            this.groupBox34.Name = "groupBox34";
            this.groupBox34.Size = new System.Drawing.Size(104, 30);
            this.groupBox34.TabIndex = 81;
            this.groupBox34.TabStop = false;
            // 
            // lsQ7_1S
            // 
            this.lsQ7_1S.AutoSize = true;
            this.lsQ7_1S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ7_1S.Location = new System.Drawing.Point(0, 8);
            this.lsQ7_1S.Name = "lsQ7_1S";
            this.lsQ7_1S.Size = new System.Drawing.Size(38, 22);
            this.lsQ7_1S.TabIndex = 67;
            this.lsQ7_1S.Text = "Si";
            this.lsQ7_1S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ7_1S.UseVisualStyleBackColor = true;
            this.lsQ7_1S.CheckedChanged += new System.EventHandler(this.updatevisibleLS7);
            // 
            // lsQ7_1N
            // 
            this.lsQ7_1N.AutoSize = true;
            this.lsQ7_1N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.lsQ7_1N.Location = new System.Drawing.Point(55, 8);
            this.lsQ7_1N.Name = "lsQ7_1N";
            this.lsQ7_1N.Size = new System.Drawing.Size(47, 22);
            this.lsQ7_1N.TabIndex = 68;
            this.lsQ7_1N.Text = "No";
            this.lsQ7_1N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lsQ7_1N.UseVisualStyleBackColor = true;
            // 
            // Datos
            // 
            this.Datos.Location = new System.Drawing.Point(4, 22);
            this.Datos.Name = "Datos";
            this.Datos.Size = new System.Drawing.Size(752, 409);
            this.Datos.TabIndex = 7;
            this.Datos.Text = "Datos";
            this.Datos.UseVisualStyleBackColor = true;
            // 
            // Resultados
            // 
            this.Resultados.Location = new System.Drawing.Point(4, 22);
            this.Resultados.Name = "Resultados";
            this.Resultados.Size = new System.Drawing.Size(752, 409);
            this.Resultados.TabIndex = 8;
            this.Resultados.Text = "Resultados";
            this.Resultados.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.splitContainer8);
            this.tabPage2.Location = new System.Drawing.Point(4, 27);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(766, 474);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Sensor/Elemento Final";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // splitContainer8
            // 
            this.splitContainer8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer8.Location = new System.Drawing.Point(3, 3);
            this.splitContainer8.Name = "splitContainer8";
            this.splitContainer8.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer8.Panel1
            // 
            this.splitContainer8.Panel1.Controls.Add(this.label46);
            // 
            // splitContainer8.Panel2
            // 
            this.splitContainer8.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer8.Size = new System.Drawing.Size(760, 468);
            this.splitContainer8.SplitterDistance = 29;
            this.splitContainer8.TabIndex = 0;
            // 
            // label46
            // 
            this.label46.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label46.Location = new System.Drawing.Point(0, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(760, 29);
            this.label46.TabIndex = 0;
            this.label46.Text = "ESTIMADOR DE FACTOR DE CAUSA COMÚN (β) - SENSOR/ELEMENTO FINAL";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Controls.Add(this.tabPage9);
            this.tabControl1.Controls.Add(this.tabPage10);
            this.tabControl1.Controls.Add(this.tabPage11);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Raleway", 10.25F, System.Drawing.FontStyle.Bold);
            this.tabControl1.ItemSize = new System.Drawing.Size(80, 18);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(760, 435);
            this.tabControl1.TabIndex = 3;
            // 
            // tabPage3
            // 
            this.tabPage3.AutoScroll = true;
            this.tabPage3.CausesValidation = false;
            this.tabPage3.Controls.Add(this.splitContainer9);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(752, 409);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Separación";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // splitContainer9
            // 
            this.splitContainer9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer9.Location = new System.Drawing.Point(3, 3);
            this.splitContainer9.Name = "splitContainer9";
            this.splitContainer9.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer9.Panel1
            // 
            this.splitContainer9.Panel1.Controls.Add(this.label48);
            // 
            // splitContainer9.Panel2
            // 
            this.splitContainer9.Panel2.AutoScroll = true;
            this.splitContainer9.Panel2.Controls.Add(this.tableLayoutPanel9);
            this.splitContainer9.Size = new System.Drawing.Size(746, 403);
            this.splitContainer9.SplitterDistance = 44;
            this.splitContainer9.TabIndex = 25;
            // 
            // label48
            // 
            this.label48.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(3, 4);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(828, 42);
            this.label48.TabIndex = 9;
            this.label48.Text = "Para los Sensores y Elementos finales de Control de la SIF, considere los siguien" +
    "tes factores relacionados con la separación y segregación de canales:";
            this.label48.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.AutoScroll = true;
            this.tableLayoutPanel9.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel9.ColumnCount = 7;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel9.Controls.Add(this.label72, 0, 10);
            this.tableLayoutPanel9.Controls.Add(this.groupBox27, 4, 7);
            this.tableLayoutPanel9.Controls.Add(this.groupBox30, 4, 4);
            this.tableLayoutPanel9.Controls.Add(this.labelC_sefQ1_3, 0, 8);
            this.tableLayoutPanel9.Controls.Add(this.labelC_sefQ1_2, 0, 5);
            this.tableLayoutPanel9.Controls.Add(this.sefQ1_3C, 2, 8);
            this.tableLayoutPanel9.Controls.Add(this.checkBox12, 6, 7);
            this.tableLayoutPanel9.Controls.Add(this.sefQ1_2C, 2, 5);
            this.tableLayoutPanel9.Controls.Add(this.labelC_sefQ1_1, 0, 2);
            this.tableLayoutPanel9.Controls.Add(this.sefQ1_1C, 2, 2);
            this.tableLayoutPanel9.Controls.Add(this.label74, 0, 7);
            this.tableLayoutPanel9.Controls.Add(this.label75, 1, 4);
            this.tableLayoutPanel9.Controls.Add(this.label77, 0, 4);
            this.tableLayoutPanel9.Controls.Add(this.label78, 1, 1);
            this.tableLayoutPanel9.Controls.Add(this.label87, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.label88, 1, 7);
            this.tableLayoutPanel9.Controls.Add(this.groupBox35, 4, 1);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel9.MinimumSize = new System.Drawing.Size(862, 386);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.tableLayoutPanel9.RowCount = 12;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(862, 386);
            this.tableLayoutPanel9.TabIndex = 2;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.tableLayoutPanel9.SetColumnSpan(this.label72, 6);
            this.label72.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label72.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label72.Location = new System.Drawing.Point(3, 386);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(824, 18);
            this.label72.TabIndex = 86;
            this.label72.Text = "  CSF Consultoría en Seguridad Funcional   Copyright ©  2019";
            this.label72.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox27
            // 
            this.tableLayoutPanel9.SetColumnSpan(this.groupBox27, 2);
            this.groupBox27.Controls.Add(this.sefQ1_3S);
            this.groupBox27.Controls.Add(this.sefQ1_3N);
            this.groupBox27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox27.Location = new System.Drawing.Point(723, 267);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(104, 30);
            this.groupBox27.TabIndex = 83;
            this.groupBox27.TabStop = false;
            // 
            // sefQ1_3S
            // 
            this.sefQ1_3S.AutoSize = true;
            this.sefQ1_3S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ1_3S.Location = new System.Drawing.Point(0, 8);
            this.sefQ1_3S.Name = "sefQ1_3S";
            this.sefQ1_3S.Size = new System.Drawing.Size(38, 22);
            this.sefQ1_3S.TabIndex = 73;
            this.sefQ1_3S.Text = "Si";
            this.sefQ1_3S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ1_3S.UseVisualStyleBackColor = true;
            this.sefQ1_3S.CheckedChanged += new System.EventHandler(this.updatevisibleSEF1);
            // 
            // sefQ1_3N
            // 
            this.sefQ1_3N.AutoSize = true;
            this.sefQ1_3N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ1_3N.Location = new System.Drawing.Point(55, 8);
            this.sefQ1_3N.Name = "sefQ1_3N";
            this.sefQ1_3N.Size = new System.Drawing.Size(47, 22);
            this.sefQ1_3N.TabIndex = 74;
            this.sefQ1_3N.Text = "No";
            this.sefQ1_3N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ1_3N.UseVisualStyleBackColor = true;
            // 
            // groupBox30
            // 
            this.tableLayoutPanel9.SetColumnSpan(this.groupBox30, 2);
            this.groupBox30.Controls.Add(this.sefQ1_2S);
            this.groupBox30.Controls.Add(this.sefQ1_2N);
            this.groupBox30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox30.Location = new System.Drawing.Point(723, 145);
            this.groupBox30.Name = "groupBox30";
            this.groupBox30.Size = new System.Drawing.Size(104, 30);
            this.groupBox30.TabIndex = 82;
            this.groupBox30.TabStop = false;
            // 
            // sefQ1_2S
            // 
            this.sefQ1_2S.AutoSize = true;
            this.sefQ1_2S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ1_2S.Location = new System.Drawing.Point(0, 8);
            this.sefQ1_2S.Name = "sefQ1_2S";
            this.sefQ1_2S.Size = new System.Drawing.Size(38, 22);
            this.sefQ1_2S.TabIndex = 69;
            this.sefQ1_2S.Text = "Si";
            this.sefQ1_2S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ1_2S.UseVisualStyleBackColor = true;
            this.sefQ1_2S.CheckedChanged += new System.EventHandler(this.updatevisibleSEF1);
            // 
            // sefQ1_2N
            // 
            this.sefQ1_2N.AutoSize = true;
            this.sefQ1_2N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ1_2N.Location = new System.Drawing.Point(55, 8);
            this.sefQ1_2N.Name = "sefQ1_2N";
            this.sefQ1_2N.Size = new System.Drawing.Size(47, 22);
            this.sefQ1_2N.TabIndex = 70;
            this.sefQ1_2N.Text = "No";
            this.sefQ1_2N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ1_2N.UseVisualStyleBackColor = true;
            // 
            // labelC_sefQ1_3
            // 
            this.labelC_sefQ1_3.AutoSize = true;
            this.tableLayoutPanel9.SetColumnSpan(this.labelC_sefQ1_3, 2);
            this.labelC_sefQ1_3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_sefQ1_3.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_sefQ1_3.Location = new System.Drawing.Point(3, 300);
            this.labelC_sefQ1_3.Name = "labelC_sefQ1_3";
            this.labelC_sefQ1_3.Size = new System.Drawing.Size(74, 66);
            this.labelC_sefQ1_3.TabIndex = 72;
            this.labelC_sefQ1_3.Text = "Criterios:";
            this.labelC_sefQ1_3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_sefQ1_3.Visible = false;
            // 
            // labelC_sefQ1_2
            // 
            this.labelC_sefQ1_2.AutoSize = true;
            this.tableLayoutPanel9.SetColumnSpan(this.labelC_sefQ1_2, 2);
            this.labelC_sefQ1_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_sefQ1_2.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_sefQ1_2.Location = new System.Drawing.Point(3, 178);
            this.labelC_sefQ1_2.Name = "labelC_sefQ1_2";
            this.labelC_sefQ1_2.Size = new System.Drawing.Size(74, 66);
            this.labelC_sefQ1_2.TabIndex = 71;
            this.labelC_sefQ1_2.Text = "Criterios:";
            this.labelC_sefQ1_2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_sefQ1_2.Visible = false;
            // 
            // sefQ1_3C
            // 
            this.tableLayoutPanel9.SetColumnSpan(this.sefQ1_3C, 3);
            this.sefQ1_3C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sefQ1_3C.Location = new System.Drawing.Point(83, 303);
            this.sefQ1_3C.Multiline = true;
            this.sefQ1_3C.Name = "sefQ1_3C";
            this.sefQ1_3C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sefQ1_3C.Size = new System.Drawing.Size(689, 60);
            this.sefQ1_3C.TabIndex = 54;
            this.sefQ1_3C.Visible = false;
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox12.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBox12.Location = new System.Drawing.Point(833, 267);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(4, 30);
            this.checkBox12.TabIndex = 52;
            this.checkBox12.UseVisualStyleBackColor = true;
            // 
            // sefQ1_2C
            // 
            this.tableLayoutPanel9.SetColumnSpan(this.sefQ1_2C, 3);
            this.sefQ1_2C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sefQ1_2C.Location = new System.Drawing.Point(83, 181);
            this.sefQ1_2C.Multiline = true;
            this.sefQ1_2C.Name = "sefQ1_2C";
            this.sefQ1_2C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sefQ1_2C.Size = new System.Drawing.Size(689, 60);
            this.sefQ1_2C.TabIndex = 48;
            this.sefQ1_2C.Visible = false;
            // 
            // labelC_sefQ1_1
            // 
            this.labelC_sefQ1_1.AutoSize = true;
            this.tableLayoutPanel9.SetColumnSpan(this.labelC_sefQ1_1, 2);
            this.labelC_sefQ1_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_sefQ1_1.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_sefQ1_1.Location = new System.Drawing.Point(3, 56);
            this.labelC_sefQ1_1.Name = "labelC_sefQ1_1";
            this.labelC_sefQ1_1.Size = new System.Drawing.Size(74, 66);
            this.labelC_sefQ1_1.TabIndex = 45;
            this.labelC_sefQ1_1.Text = "Criterios:";
            this.labelC_sefQ1_1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_sefQ1_1.Visible = false;
            // 
            // sefQ1_1C
            // 
            this.tableLayoutPanel9.SetColumnSpan(this.sefQ1_1C, 3);
            this.sefQ1_1C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sefQ1_1C.Location = new System.Drawing.Point(83, 59);
            this.sefQ1_1C.Multiline = true;
            this.sefQ1_1C.Name = "sefQ1_1C";
            this.sefQ1_1C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sefQ1_1C.Size = new System.Drawing.Size(689, 60);
            this.sefQ1_1C.TabIndex = 32;
            this.sefQ1_1C.Visible = false;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label74.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label74.Location = new System.Drawing.Point(3, 264);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(34, 36);
            this.label74.TabIndex = 12;
            this.label74.Text = "1.3.";
            this.label74.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.tableLayoutPanel9.SetColumnSpan(this.label75, 2);
            this.label75.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label75.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.Location = new System.Drawing.Point(43, 142);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(664, 36);
            this.label75.TabIndex = 10;
            this.label75.Text = "Si los sensores/elementos final han dedicado la electrónica de control, ¿Está la " +
    "electrónica para cada canal por separado en los tableros de circuitos impresos?";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label77.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label77.Location = new System.Drawing.Point(3, 142);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(34, 36);
            this.label77.TabIndex = 11;
            this.label77.Text = "1.2.";
            this.label77.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.tableLayoutPanel9.SetColumnSpan(this.label78, 2);
            this.label78.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label78.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.Location = new System.Drawing.Point(43, 20);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(664, 36);
            this.label78.TabIndex = 1;
            this.label78.Text = "¿Todos los cables de señal para los canales se enrutan por separado en todas las " +
    "posiciones?";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label87.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label87.Location = new System.Drawing.Point(3, 20);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(34, 36);
            this.label87.TabIndex = 9;
            this.label87.Text = "1.1.";
            this.label87.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.tableLayoutPanel9.SetColumnSpan(this.label88, 2);
            this.label88.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label88.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label88.Location = new System.Drawing.Point(43, 264);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(664, 36);
            this.label88.TabIndex = 14;
            this.label88.Text = "Si los sensores/elementos final han dedicado la electrónica de control, ?¿Está la" +
    " electrónica para cada canal dentro y en gabinetes separados?";
            // 
            // groupBox35
            // 
            this.tableLayoutPanel9.SetColumnSpan(this.groupBox35, 2);
            this.groupBox35.Controls.Add(this.sefQ1_1S);
            this.groupBox35.Controls.Add(this.sefQ1_1N);
            this.groupBox35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox35.Location = new System.Drawing.Point(723, 23);
            this.groupBox35.Name = "groupBox35";
            this.groupBox35.Size = new System.Drawing.Size(104, 30);
            this.groupBox35.TabIndex = 81;
            this.groupBox35.TabStop = false;
            // 
            // sefQ1_1S
            // 
            this.sefQ1_1S.AutoSize = true;
            this.sefQ1_1S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ1_1S.Location = new System.Drawing.Point(0, 8);
            this.sefQ1_1S.Name = "sefQ1_1S";
            this.sefQ1_1S.Size = new System.Drawing.Size(38, 22);
            this.sefQ1_1S.TabIndex = 67;
            this.sefQ1_1S.Text = "Si";
            this.sefQ1_1S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ1_1S.UseVisualStyleBackColor = true;
            this.sefQ1_1S.CheckedChanged += new System.EventHandler(this.updatevisibleSEF1);
            // 
            // sefQ1_1N
            // 
            this.sefQ1_1N.AutoSize = true;
            this.sefQ1_1N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ1_1N.Location = new System.Drawing.Point(55, 8);
            this.sefQ1_1N.Name = "sefQ1_1N";
            this.sefQ1_1N.Size = new System.Drawing.Size(47, 22);
            this.sefQ1_1N.TabIndex = 68;
            this.sefQ1_1N.Text = "No";
            this.sefQ1_1N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ1_1N.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.splitContainer10);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(752, 409);
            this.tabPage4.TabIndex = 9;
            this.tabPage4.Text = "Redundancia";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // splitContainer10
            // 
            this.splitContainer10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer10.Location = new System.Drawing.Point(3, 3);
            this.splitContainer10.Name = "splitContainer10";
            this.splitContainer10.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer10.Panel1
            // 
            this.splitContainer10.Panel1.Controls.Add(this.label89);
            // 
            // splitContainer10.Panel2
            // 
            this.splitContainer10.Panel2.AutoScroll = true;
            this.splitContainer10.Panel2.Controls.Add(this.tableLayoutPanel10);
            this.splitContainer10.Size = new System.Drawing.Size(746, 403);
            this.splitContainer10.SplitterDistance = 44;
            this.splitContainer10.TabIndex = 26;
            // 
            // label89
            // 
            this.label89.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.Location = new System.Drawing.Point(3, 4);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(828, 42);
            this.label89.TabIndex = 9;
            this.label89.Text = "Para los Sensores y Elementos finales de Control de la SIF, considere los siguien" +
    "tes factores relacionados con la diversidad y redundancia de canales:";
            this.label89.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.AutoScroll = true;
            this.tableLayoutPanel10.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel10.ColumnCount = 7;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel10.Controls.Add(this.label96, 0, 13);
            this.tableLayoutPanel10.Controls.Add(this.groupBox39, 4, 10);
            this.tableLayoutPanel10.Controls.Add(this.groupBox40, 4, 7);
            this.tableLayoutPanel10.Controls.Add(this.groupBox41, 4, 4);
            this.tableLayoutPanel10.Controls.Add(this.labelC_sefQ2_4, 0, 11);
            this.tableLayoutPanel10.Controls.Add(this.labelC_sefQ2_3, 0, 8);
            this.tableLayoutPanel10.Controls.Add(this.labelC_sefQ2_2, 0, 5);
            this.tableLayoutPanel10.Controls.Add(this.label101, 3, 13);
            this.tableLayoutPanel10.Controls.Add(this.sefQ2_4C, 2, 11);
            this.tableLayoutPanel10.Controls.Add(this.checkBox16, 6, 10);
            this.tableLayoutPanel10.Controls.Add(this.sefQ2_3C, 2, 8);
            this.tableLayoutPanel10.Controls.Add(this.checkBox17, 6, 7);
            this.tableLayoutPanel10.Controls.Add(this.sefQ2_2C, 2, 5);
            this.tableLayoutPanel10.Controls.Add(this.labelC_sefQ2_1, 0, 2);
            this.tableLayoutPanel10.Controls.Add(this.sefQ2_1C, 2, 2);
            this.tableLayoutPanel10.Controls.Add(this.label104, 1, 10);
            this.tableLayoutPanel10.Controls.Add(this.label105, 0, 10);
            this.tableLayoutPanel10.Controls.Add(this.label106, 0, 7);
            this.tableLayoutPanel10.Controls.Add(this.label107, 1, 4);
            this.tableLayoutPanel10.Controls.Add(this.label108, 0, 4);
            this.tableLayoutPanel10.Controls.Add(this.label109, 1, 1);
            this.tableLayoutPanel10.Controls.Add(this.label110, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.label111, 1, 7);
            this.tableLayoutPanel10.Controls.Add(this.groupBox42, 4, 1);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel10.MinimumSize = new System.Drawing.Size(862, 386);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.tableLayoutPanel10.RowCount = 15;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(862, 386);
            this.tableLayoutPanel10.TabIndex = 2;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.tableLayoutPanel10.SetColumnSpan(this.label96, 6);
            this.label96.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label96.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label96.Location = new System.Drawing.Point(3, 544);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(824, 18);
            this.label96.TabIndex = 86;
            this.label96.Text = "  CSF Consultoría en Seguridad Funcional   Copyright ©  2019";
            this.label96.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox39
            // 
            this.tableLayoutPanel10.SetColumnSpan(this.groupBox39, 2);
            this.groupBox39.Controls.Add(this.sefQ2_4N);
            this.groupBox39.Controls.Add(this.sefQ2_4S);
            this.groupBox39.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox39.Location = new System.Drawing.Point(723, 425);
            this.groupBox39.Name = "groupBox39";
            this.groupBox39.Size = new System.Drawing.Size(104, 30);
            this.groupBox39.TabIndex = 84;
            this.groupBox39.TabStop = false;
            // 
            // sefQ2_4N
            // 
            this.sefQ2_4N.AutoSize = true;
            this.sefQ2_4N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ2_4N.Location = new System.Drawing.Point(55, 8);
            this.sefQ2_4N.Name = "sefQ2_4N";
            this.sefQ2_4N.Size = new System.Drawing.Size(47, 22);
            this.sefQ2_4N.TabIndex = 79;
            this.sefQ2_4N.Text = "No";
            this.sefQ2_4N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ2_4N.UseVisualStyleBackColor = true;
            // 
            // sefQ2_4S
            // 
            this.sefQ2_4S.AutoSize = true;
            this.sefQ2_4S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ2_4S.Location = new System.Drawing.Point(0, 8);
            this.sefQ2_4S.Name = "sefQ2_4S";
            this.sefQ2_4S.Size = new System.Drawing.Size(38, 22);
            this.sefQ2_4S.TabIndex = 77;
            this.sefQ2_4S.Text = "Si";
            this.sefQ2_4S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ2_4S.UseVisualStyleBackColor = true;
            this.sefQ2_4S.CheckedChanged += new System.EventHandler(this.updatevisibleSEF2);
            // 
            // groupBox40
            // 
            this.tableLayoutPanel10.SetColumnSpan(this.groupBox40, 2);
            this.groupBox40.Controls.Add(this.sefQ2_3S);
            this.groupBox40.Controls.Add(this.sefQ2_3N);
            this.groupBox40.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox40.Location = new System.Drawing.Point(723, 303);
            this.groupBox40.Name = "groupBox40";
            this.groupBox40.Size = new System.Drawing.Size(104, 30);
            this.groupBox40.TabIndex = 83;
            this.groupBox40.TabStop = false;
            // 
            // sefQ2_3S
            // 
            this.sefQ2_3S.AutoSize = true;
            this.sefQ2_3S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ2_3S.Location = new System.Drawing.Point(-2, 8);
            this.sefQ2_3S.Name = "sefQ2_3S";
            this.sefQ2_3S.Size = new System.Drawing.Size(38, 22);
            this.sefQ2_3S.TabIndex = 73;
            this.sefQ2_3S.Text = "Si";
            this.sefQ2_3S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ2_3S.UseVisualStyleBackColor = true;
            this.sefQ2_3S.CheckedChanged += new System.EventHandler(this.updatevisibleSEF2);
            // 
            // sefQ2_3N
            // 
            this.sefQ2_3N.AutoSize = true;
            this.sefQ2_3N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ2_3N.Location = new System.Drawing.Point(55, 8);
            this.sefQ2_3N.Name = "sefQ2_3N";
            this.sefQ2_3N.Size = new System.Drawing.Size(47, 22);
            this.sefQ2_3N.TabIndex = 74;
            this.sefQ2_3N.Text = "No";
            this.sefQ2_3N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ2_3N.UseVisualStyleBackColor = true;
            // 
            // groupBox41
            // 
            this.tableLayoutPanel10.SetColumnSpan(this.groupBox41, 2);
            this.groupBox41.Controls.Add(this.sefQ2_2S);
            this.groupBox41.Controls.Add(this.sefQ2_2N);
            this.groupBox41.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox41.Location = new System.Drawing.Point(723, 163);
            this.groupBox41.Name = "groupBox41";
            this.groupBox41.Size = new System.Drawing.Size(104, 48);
            this.groupBox41.TabIndex = 82;
            this.groupBox41.TabStop = false;
            // 
            // sefQ2_2S
            // 
            this.sefQ2_2S.AutoSize = true;
            this.sefQ2_2S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ2_2S.Location = new System.Drawing.Point(0, 8);
            this.sefQ2_2S.Name = "sefQ2_2S";
            this.sefQ2_2S.Size = new System.Drawing.Size(38, 22);
            this.sefQ2_2S.TabIndex = 69;
            this.sefQ2_2S.Text = "Si";
            this.sefQ2_2S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ2_2S.UseVisualStyleBackColor = true;
            this.sefQ2_2S.CheckedChanged += new System.EventHandler(this.updatevisibleSEF2);
            // 
            // sefQ2_2N
            // 
            this.sefQ2_2N.AutoSize = true;
            this.sefQ2_2N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ2_2N.Location = new System.Drawing.Point(55, 8);
            this.sefQ2_2N.Name = "sefQ2_2N";
            this.sefQ2_2N.Size = new System.Drawing.Size(47, 22);
            this.sefQ2_2N.TabIndex = 70;
            this.sefQ2_2N.Text = "No";
            this.sefQ2_2N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ2_2N.UseVisualStyleBackColor = true;
            // 
            // labelC_sefQ2_4
            // 
            this.labelC_sefQ2_4.AutoSize = true;
            this.tableLayoutPanel10.SetColumnSpan(this.labelC_sefQ2_4, 2);
            this.labelC_sefQ2_4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_sefQ2_4.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_sefQ2_4.Location = new System.Drawing.Point(3, 458);
            this.labelC_sefQ2_4.Name = "labelC_sefQ2_4";
            this.labelC_sefQ2_4.Size = new System.Drawing.Size(74, 66);
            this.labelC_sefQ2_4.TabIndex = 75;
            this.labelC_sefQ2_4.Text = "Criterios:";
            this.labelC_sefQ2_4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_sefQ2_4.Visible = false;
            // 
            // labelC_sefQ2_3
            // 
            this.labelC_sefQ2_3.AutoSize = true;
            this.tableLayoutPanel10.SetColumnSpan(this.labelC_sefQ2_3, 2);
            this.labelC_sefQ2_3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_sefQ2_3.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_sefQ2_3.Location = new System.Drawing.Point(3, 336);
            this.labelC_sefQ2_3.Name = "labelC_sefQ2_3";
            this.labelC_sefQ2_3.Size = new System.Drawing.Size(74, 66);
            this.labelC_sefQ2_3.TabIndex = 72;
            this.labelC_sefQ2_3.Text = "Criterios:";
            this.labelC_sefQ2_3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_sefQ2_3.Visible = false;
            // 
            // labelC_sefQ2_2
            // 
            this.labelC_sefQ2_2.AutoSize = true;
            this.tableLayoutPanel10.SetColumnSpan(this.labelC_sefQ2_2, 2);
            this.labelC_sefQ2_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_sefQ2_2.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_sefQ2_2.Location = new System.Drawing.Point(3, 214);
            this.labelC_sefQ2_2.Name = "labelC_sefQ2_2";
            this.labelC_sefQ2_2.Size = new System.Drawing.Size(74, 66);
            this.labelC_sefQ2_2.TabIndex = 71;
            this.labelC_sefQ2_2.Text = "Criterios:";
            this.labelC_sefQ2_2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_sefQ2_2.Visible = false;
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.tableLayoutPanel10.SetColumnSpan(this.label101, 4);
            this.label101.Location = new System.Drawing.Point(3, 562);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(0, 16);
            this.label101.TabIndex = 61;
            // 
            // sefQ2_4C
            // 
            this.tableLayoutPanel10.SetColumnSpan(this.sefQ2_4C, 3);
            this.sefQ2_4C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sefQ2_4C.Location = new System.Drawing.Point(83, 461);
            this.sefQ2_4C.Multiline = true;
            this.sefQ2_4C.Name = "sefQ2_4C";
            this.sefQ2_4C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sefQ2_4C.Size = new System.Drawing.Size(689, 60);
            this.sefQ2_4C.TabIndex = 60;
            this.sefQ2_4C.Visible = false;
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox16.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBox16.Location = new System.Drawing.Point(833, 425);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(4, 30);
            this.checkBox16.TabIndex = 58;
            this.checkBox16.UseVisualStyleBackColor = true;
            // 
            // sefQ2_3C
            // 
            this.tableLayoutPanel10.SetColumnSpan(this.sefQ2_3C, 3);
            this.sefQ2_3C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sefQ2_3C.Location = new System.Drawing.Point(83, 339);
            this.sefQ2_3C.Multiline = true;
            this.sefQ2_3C.Name = "sefQ2_3C";
            this.sefQ2_3C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sefQ2_3C.Size = new System.Drawing.Size(689, 60);
            this.sefQ2_3C.TabIndex = 54;
            this.sefQ2_3C.Visible = false;
            // 
            // checkBox17
            // 
            this.checkBox17.AutoSize = true;
            this.checkBox17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox17.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBox17.Location = new System.Drawing.Point(833, 303);
            this.checkBox17.Name = "checkBox17";
            this.checkBox17.Size = new System.Drawing.Size(4, 30);
            this.checkBox17.TabIndex = 52;
            this.checkBox17.UseVisualStyleBackColor = true;
            // 
            // sefQ2_2C
            // 
            this.tableLayoutPanel10.SetColumnSpan(this.sefQ2_2C, 3);
            this.sefQ2_2C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sefQ2_2C.Location = new System.Drawing.Point(83, 217);
            this.sefQ2_2C.Multiline = true;
            this.sefQ2_2C.Name = "sefQ2_2C";
            this.sefQ2_2C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sefQ2_2C.Size = new System.Drawing.Size(689, 60);
            this.sefQ2_2C.TabIndex = 48;
            this.sefQ2_2C.Visible = false;
            // 
            // labelC_sefQ2_1
            // 
            this.labelC_sefQ2_1.AutoSize = true;
            this.tableLayoutPanel10.SetColumnSpan(this.labelC_sefQ2_1, 2);
            this.labelC_sefQ2_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_sefQ2_1.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_sefQ2_1.Location = new System.Drawing.Point(3, 74);
            this.labelC_sefQ2_1.Name = "labelC_sefQ2_1";
            this.labelC_sefQ2_1.Size = new System.Drawing.Size(74, 66);
            this.labelC_sefQ2_1.TabIndex = 45;
            this.labelC_sefQ2_1.Text = "Criterios:";
            this.labelC_sefQ2_1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_sefQ2_1.Visible = false;
            // 
            // sefQ2_1C
            // 
            this.tableLayoutPanel10.SetColumnSpan(this.sefQ2_1C, 3);
            this.sefQ2_1C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sefQ2_1C.Location = new System.Drawing.Point(83, 77);
            this.sefQ2_1C.Multiline = true;
            this.sefQ2_1C.Name = "sefQ2_1C";
            this.sefQ2_1C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sefQ2_1C.Size = new System.Drawing.Size(689, 60);
            this.sefQ2_1C.TabIndex = 32;
            this.sefQ2_1C.Visible = false;
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.tableLayoutPanel10.SetColumnSpan(this.label104, 2);
            this.label104.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label104.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label104.Location = new System.Drawing.Point(43, 422);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(664, 36);
            this.label104.TabIndex = 16;
            this.label104.Text = "¿El mantenimiento de cada canal esta siendo llevado a cabo por diferentes persona" +
    "s en diferentes momentos?";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label105.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label105.Location = new System.Drawing.Point(3, 422);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(34, 36);
            this.label105.TabIndex = 13;
            this.label105.Text = "2.4.";
            this.label105.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label106.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label106.Location = new System.Drawing.Point(3, 300);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(34, 36);
            this.label106.TabIndex = 12;
            this.label106.Text = "2.3.";
            this.label106.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.tableLayoutPanel10.SetColumnSpan(this.label107, 2);
            this.label107.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label107.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label107.Location = new System.Drawing.Point(43, 160);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(664, 54);
            this.label107.TabIndex = 10;
            this.label107.Text = "¿Los dispositivos (sensores/elementos finales) emplean diferentes principios eléc" +
    "tricos/diseños, por ejemplo, digital y analógica, fabricante diferente (no rebau" +
    "tizado) o tecnología diferente?";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label108.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label108.Location = new System.Drawing.Point(3, 160);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(34, 54);
            this.label108.TabIndex = 11;
            this.label108.Text = "2.2.";
            this.label108.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.tableLayoutPanel10.SetColumnSpan(this.label109, 2);
            this.label109.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label109.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label109.Location = new System.Drawing.Point(43, 20);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(664, 54);
            this.label109.TabIndex = 1;
            this.label109.Text = resources.GetString("label109.Text");
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label110.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label110.Location = new System.Drawing.Point(3, 20);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(34, 54);
            this.label110.TabIndex = 9;
            this.label110.Text = "2.1.";
            this.label110.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.tableLayoutPanel10.SetColumnSpan(this.label111, 2);
            this.label111.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label111.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label111.Location = new System.Drawing.Point(43, 300);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(664, 36);
            this.label111.TabIndex = 14;
            this.label111.Text = "¿Estan separados los métodos de prueba para cada canal y las personas utilizada p" +
    "ara la puesta en marcha?";
            // 
            // groupBox42
            // 
            this.tableLayoutPanel10.SetColumnSpan(this.groupBox42, 2);
            this.groupBox42.Controls.Add(this.sefQ2_1S);
            this.groupBox42.Controls.Add(this.sefQ2_1N);
            this.groupBox42.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox42.Location = new System.Drawing.Point(723, 23);
            this.groupBox42.Name = "groupBox42";
            this.groupBox42.Size = new System.Drawing.Size(104, 48);
            this.groupBox42.TabIndex = 81;
            this.groupBox42.TabStop = false;
            // 
            // sefQ2_1S
            // 
            this.sefQ2_1S.AutoSize = true;
            this.sefQ2_1S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ2_1S.Location = new System.Drawing.Point(0, 8);
            this.sefQ2_1S.Name = "sefQ2_1S";
            this.sefQ2_1S.Size = new System.Drawing.Size(38, 22);
            this.sefQ2_1S.TabIndex = 67;
            this.sefQ2_1S.Text = "Si";
            this.sefQ2_1S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ2_1S.UseVisualStyleBackColor = true;
            this.sefQ2_1S.CheckedChanged += new System.EventHandler(this.updatevisibleSEF2);
            // 
            // sefQ2_1N
            // 
            this.sefQ2_1N.AutoSize = true;
            this.sefQ2_1N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ2_1N.Location = new System.Drawing.Point(55, 8);
            this.sefQ2_1N.Name = "sefQ2_1N";
            this.sefQ2_1N.Size = new System.Drawing.Size(47, 22);
            this.sefQ2_1N.TabIndex = 68;
            this.sefQ2_1N.Text = "No";
            this.sefQ2_1N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ2_1N.UseVisualStyleBackColor = true;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.splitContainer11);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(752, 409);
            this.tabPage5.TabIndex = 2;
            this.tabPage5.Text = "Complejidad";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // splitContainer11
            // 
            this.splitContainer11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer11.Location = new System.Drawing.Point(3, 3);
            this.splitContainer11.Name = "splitContainer11";
            this.splitContainer11.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer11.Panel1
            // 
            this.splitContainer11.Panel1.Controls.Add(this.label113);
            // 
            // splitContainer11.Panel2
            // 
            this.splitContainer11.Panel2.AutoScroll = true;
            this.splitContainer11.Panel2.Controls.Add(this.tableLayoutPanel11);
            this.splitContainer11.Size = new System.Drawing.Size(746, 403);
            this.splitContainer11.SplitterDistance = 44;
            this.splitContainer11.TabIndex = 27;
            // 
            // label113
            // 
            this.label113.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label113.Location = new System.Drawing.Point(3, 4);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(828, 42);
            this.label113.TabIndex = 9;
            this.label113.Text = "Para los Sensores y Elementos finales de Control de la SIF, considere los siguien" +
    "tes factores relacionados con la Complejidad, el diseño, la aplicación, la madur" +
    "ez y la experiencia en el sistema:";
            this.label113.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.AutoScroll = true;
            this.tableLayoutPanel11.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel11.ColumnCount = 7;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel11.Controls.Add(this.label117, 0, 16);
            this.tableLayoutPanel11.Controls.Add(this.groupBox44, 4, 13);
            this.tableLayoutPanel11.Controls.Add(this.groupBox45, 4, 10);
            this.tableLayoutPanel11.Controls.Add(this.groupBox46, 4, 7);
            this.tableLayoutPanel11.Controls.Add(this.groupBox47, 4, 4);
            this.tableLayoutPanel11.Controls.Add(this.labelC_sefQ3_5, 0, 14);
            this.tableLayoutPanel11.Controls.Add(this.labelC_sefQ3_4, 0, 11);
            this.tableLayoutPanel11.Controls.Add(this.labelC_sefQ3_3, 0, 8);
            this.tableLayoutPanel11.Controls.Add(this.labelC_sefQ3_2, 0, 5);
            this.tableLayoutPanel11.Controls.Add(this.sefQ3_5C, 2, 14);
            this.tableLayoutPanel11.Controls.Add(this.checkBox18, 6, 13);
            this.tableLayoutPanel11.Controls.Add(this.label122, 3, 16);
            this.tableLayoutPanel11.Controls.Add(this.sefQ3_4C, 2, 11);
            this.tableLayoutPanel11.Controls.Add(this.checkBox19, 6, 10);
            this.tableLayoutPanel11.Controls.Add(this.sefQ3_3C, 2, 8);
            this.tableLayoutPanel11.Controls.Add(this.checkBox20, 6, 7);
            this.tableLayoutPanel11.Controls.Add(this.sefQ3_2C, 2, 5);
            this.tableLayoutPanel11.Controls.Add(this.labelC_sefQ3_1, 0, 2);
            this.tableLayoutPanel11.Controls.Add(this.sefQ3_1C, 2, 2);
            this.tableLayoutPanel11.Controls.Add(this.label124, 1, 13);
            this.tableLayoutPanel11.Controls.Add(this.label125, 1, 10);
            this.tableLayoutPanel11.Controls.Add(this.label126, 0, 10);
            this.tableLayoutPanel11.Controls.Add(this.label127, 0, 7);
            this.tableLayoutPanel11.Controls.Add(this.label128, 1, 4);
            this.tableLayoutPanel11.Controls.Add(this.label129, 0, 4);
            this.tableLayoutPanel11.Controls.Add(this.label130, 1, 1);
            this.tableLayoutPanel11.Controls.Add(this.label131, 0, 1);
            this.tableLayoutPanel11.Controls.Add(this.label132, 1, 7);
            this.tableLayoutPanel11.Controls.Add(this.label133, 0, 13);
            this.tableLayoutPanel11.Controls.Add(this.groupBox48, 4, 1);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel11.MinimumSize = new System.Drawing.Size(862, 386);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.tableLayoutPanel11.RowCount = 18;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(862, 386);
            this.tableLayoutPanel11.TabIndex = 2;
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.tableLayoutPanel11.SetColumnSpan(this.label117, 6);
            this.label117.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label117.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label117.Location = new System.Drawing.Point(3, 630);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(824, 18);
            this.label117.TabIndex = 86;
            this.label117.Text = "  CSF Consultoría en Seguridad Funcional   Copyright ©  2019";
            this.label117.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox44
            // 
            this.tableLayoutPanel11.SetColumnSpan(this.groupBox44, 2);
            this.groupBox44.Controls.Add(this.sefQ3_5N);
            this.groupBox44.Controls.Add(this.sefQ3_5S);
            this.groupBox44.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox44.Location = new System.Drawing.Point(723, 511);
            this.groupBox44.Name = "groupBox44";
            this.groupBox44.Size = new System.Drawing.Size(104, 30);
            this.groupBox44.TabIndex = 85;
            this.groupBox44.TabStop = false;
            // 
            // sefQ3_5N
            // 
            this.sefQ3_5N.AutoSize = true;
            this.sefQ3_5N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ3_5N.Location = new System.Drawing.Point(55, 8);
            this.sefQ3_5N.Name = "sefQ3_5N";
            this.sefQ3_5N.Size = new System.Drawing.Size(47, 22);
            this.sefQ3_5N.TabIndex = 80;
            this.sefQ3_5N.Text = "No";
            this.sefQ3_5N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ3_5N.UseVisualStyleBackColor = true;
            // 
            // sefQ3_5S
            // 
            this.sefQ3_5S.AutoSize = true;
            this.sefQ3_5S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ3_5S.Location = new System.Drawing.Point(0, 8);
            this.sefQ3_5S.Name = "sefQ3_5S";
            this.sefQ3_5S.Size = new System.Drawing.Size(38, 22);
            this.sefQ3_5S.TabIndex = 78;
            this.sefQ3_5S.Text = "Si";
            this.sefQ3_5S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ3_5S.UseVisualStyleBackColor = true;
            this.sefQ3_5S.CheckedChanged += new System.EventHandler(this.updatevisibleSEF3);
            // 
            // groupBox45
            // 
            this.tableLayoutPanel11.SetColumnSpan(this.groupBox45, 2);
            this.groupBox45.Controls.Add(this.sefQ3_4N);
            this.groupBox45.Controls.Add(this.sefQ3_4S);
            this.groupBox45.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox45.Location = new System.Drawing.Point(723, 389);
            this.groupBox45.Name = "groupBox45";
            this.groupBox45.Size = new System.Drawing.Size(104, 30);
            this.groupBox45.TabIndex = 84;
            this.groupBox45.TabStop = false;
            // 
            // sefQ3_4N
            // 
            this.sefQ3_4N.AutoSize = true;
            this.sefQ3_4N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ3_4N.Location = new System.Drawing.Point(55, 8);
            this.sefQ3_4N.Name = "sefQ3_4N";
            this.sefQ3_4N.Size = new System.Drawing.Size(47, 22);
            this.sefQ3_4N.TabIndex = 79;
            this.sefQ3_4N.Text = "No";
            this.sefQ3_4N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ3_4N.UseVisualStyleBackColor = true;
            // 
            // sefQ3_4S
            // 
            this.sefQ3_4S.AutoSize = true;
            this.sefQ3_4S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ3_4S.Location = new System.Drawing.Point(0, 8);
            this.sefQ3_4S.Name = "sefQ3_4S";
            this.sefQ3_4S.Size = new System.Drawing.Size(38, 22);
            this.sefQ3_4S.TabIndex = 77;
            this.sefQ3_4S.Text = "Si";
            this.sefQ3_4S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ3_4S.UseVisualStyleBackColor = true;
            this.sefQ3_4S.CheckedChanged += new System.EventHandler(this.updatevisibleSEF3);
            // 
            // groupBox46
            // 
            this.tableLayoutPanel11.SetColumnSpan(this.groupBox46, 2);
            this.groupBox46.Controls.Add(this.sefQ3_3S);
            this.groupBox46.Controls.Add(this.sefQ3_3N);
            this.groupBox46.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox46.Location = new System.Drawing.Point(723, 267);
            this.groupBox46.Name = "groupBox46";
            this.groupBox46.Size = new System.Drawing.Size(104, 30);
            this.groupBox46.TabIndex = 83;
            this.groupBox46.TabStop = false;
            // 
            // sefQ3_3S
            // 
            this.sefQ3_3S.AutoSize = true;
            this.sefQ3_3S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ3_3S.Location = new System.Drawing.Point(0, 8);
            this.sefQ3_3S.Name = "sefQ3_3S";
            this.sefQ3_3S.Size = new System.Drawing.Size(38, 22);
            this.sefQ3_3S.TabIndex = 73;
            this.sefQ3_3S.Text = "Si";
            this.sefQ3_3S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ3_3S.UseVisualStyleBackColor = true;
            this.sefQ3_3S.CheckedChanged += new System.EventHandler(this.updatevisibleSEF3);
            // 
            // sefQ3_3N
            // 
            this.sefQ3_3N.AutoSize = true;
            this.sefQ3_3N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ3_3N.Location = new System.Drawing.Point(55, 8);
            this.sefQ3_3N.Name = "sefQ3_3N";
            this.sefQ3_3N.Size = new System.Drawing.Size(47, 22);
            this.sefQ3_3N.TabIndex = 74;
            this.sefQ3_3N.Text = "No";
            this.sefQ3_3N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ3_3N.UseVisualStyleBackColor = true;
            // 
            // groupBox47
            // 
            this.tableLayoutPanel11.SetColumnSpan(this.groupBox47, 2);
            this.groupBox47.Controls.Add(this.sefQ3_2S);
            this.groupBox47.Controls.Add(this.sefQ3_2N);
            this.groupBox47.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox47.Location = new System.Drawing.Point(723, 145);
            this.groupBox47.Name = "groupBox47";
            this.groupBox47.Size = new System.Drawing.Size(104, 30);
            this.groupBox47.TabIndex = 82;
            this.groupBox47.TabStop = false;
            // 
            // sefQ3_2S
            // 
            this.sefQ3_2S.AutoSize = true;
            this.sefQ3_2S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ3_2S.Location = new System.Drawing.Point(0, 8);
            this.sefQ3_2S.Name = "sefQ3_2S";
            this.sefQ3_2S.Size = new System.Drawing.Size(38, 22);
            this.sefQ3_2S.TabIndex = 69;
            this.sefQ3_2S.Text = "Si";
            this.sefQ3_2S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ3_2S.UseVisualStyleBackColor = true;
            this.sefQ3_2S.CheckedChanged += new System.EventHandler(this.updatevisibleSEF3);
            // 
            // sefQ3_2N
            // 
            this.sefQ3_2N.AutoSize = true;
            this.sefQ3_2N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ3_2N.Location = new System.Drawing.Point(55, 8);
            this.sefQ3_2N.Name = "sefQ3_2N";
            this.sefQ3_2N.Size = new System.Drawing.Size(47, 22);
            this.sefQ3_2N.TabIndex = 70;
            this.sefQ3_2N.Text = "No";
            this.sefQ3_2N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ3_2N.UseVisualStyleBackColor = true;
            // 
            // labelC_sefQ3_5
            // 
            this.labelC_sefQ3_5.AutoSize = true;
            this.tableLayoutPanel11.SetColumnSpan(this.labelC_sefQ3_5, 2);
            this.labelC_sefQ3_5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_sefQ3_5.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_sefQ3_5.Location = new System.Drawing.Point(3, 544);
            this.labelC_sefQ3_5.Name = "labelC_sefQ3_5";
            this.labelC_sefQ3_5.Size = new System.Drawing.Size(74, 66);
            this.labelC_sefQ3_5.TabIndex = 76;
            this.labelC_sefQ3_5.Text = "Criterios:";
            this.labelC_sefQ3_5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_sefQ3_5.Visible = false;
            // 
            // labelC_sefQ3_4
            // 
            this.labelC_sefQ3_4.AutoSize = true;
            this.tableLayoutPanel11.SetColumnSpan(this.labelC_sefQ3_4, 2);
            this.labelC_sefQ3_4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_sefQ3_4.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_sefQ3_4.Location = new System.Drawing.Point(3, 422);
            this.labelC_sefQ3_4.Name = "labelC_sefQ3_4";
            this.labelC_sefQ3_4.Size = new System.Drawing.Size(74, 66);
            this.labelC_sefQ3_4.TabIndex = 75;
            this.labelC_sefQ3_4.Text = "Criterios:";
            this.labelC_sefQ3_4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_sefQ3_4.Visible = false;
            // 
            // labelC_sefQ3_3
            // 
            this.labelC_sefQ3_3.AutoSize = true;
            this.tableLayoutPanel11.SetColumnSpan(this.labelC_sefQ3_3, 2);
            this.labelC_sefQ3_3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_sefQ3_3.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_sefQ3_3.Location = new System.Drawing.Point(3, 300);
            this.labelC_sefQ3_3.Name = "labelC_sefQ3_3";
            this.labelC_sefQ3_3.Size = new System.Drawing.Size(74, 66);
            this.labelC_sefQ3_3.TabIndex = 72;
            this.labelC_sefQ3_3.Text = "Criterios:";
            this.labelC_sefQ3_3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_sefQ3_3.Visible = false;
            // 
            // labelC_sefQ3_2
            // 
            this.labelC_sefQ3_2.AutoSize = true;
            this.tableLayoutPanel11.SetColumnSpan(this.labelC_sefQ3_2, 2);
            this.labelC_sefQ3_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_sefQ3_2.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_sefQ3_2.Location = new System.Drawing.Point(3, 178);
            this.labelC_sefQ3_2.Name = "labelC_sefQ3_2";
            this.labelC_sefQ3_2.Size = new System.Drawing.Size(74, 66);
            this.labelC_sefQ3_2.TabIndex = 71;
            this.labelC_sefQ3_2.Text = "Criterios:";
            this.labelC_sefQ3_2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_sefQ3_2.Visible = false;
            // 
            // sefQ3_5C
            // 
            this.tableLayoutPanel11.SetColumnSpan(this.sefQ3_5C, 3);
            this.sefQ3_5C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sefQ3_5C.Location = new System.Drawing.Point(83, 547);
            this.sefQ3_5C.Multiline = true;
            this.sefQ3_5C.Name = "sefQ3_5C";
            this.sefQ3_5C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sefQ3_5C.Size = new System.Drawing.Size(689, 60);
            this.sefQ3_5C.TabIndex = 66;
            this.sefQ3_5C.Visible = false;
            // 
            // checkBox18
            // 
            this.checkBox18.AutoSize = true;
            this.checkBox18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox18.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBox18.Location = new System.Drawing.Point(833, 511);
            this.checkBox18.Name = "checkBox18";
            this.checkBox18.Size = new System.Drawing.Size(4, 30);
            this.checkBox18.TabIndex = 64;
            this.checkBox18.UseVisualStyleBackColor = true;
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.tableLayoutPanel11.SetColumnSpan(this.label122, 4);
            this.label122.Location = new System.Drawing.Point(3, 648);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(0, 16);
            this.label122.TabIndex = 61;
            // 
            // sefQ3_4C
            // 
            this.tableLayoutPanel11.SetColumnSpan(this.sefQ3_4C, 3);
            this.sefQ3_4C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sefQ3_4C.Location = new System.Drawing.Point(83, 425);
            this.sefQ3_4C.Multiline = true;
            this.sefQ3_4C.Name = "sefQ3_4C";
            this.sefQ3_4C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sefQ3_4C.Size = new System.Drawing.Size(689, 60);
            this.sefQ3_4C.TabIndex = 60;
            this.sefQ3_4C.Visible = false;
            // 
            // checkBox19
            // 
            this.checkBox19.AutoSize = true;
            this.checkBox19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox19.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBox19.Location = new System.Drawing.Point(833, 389);
            this.checkBox19.Name = "checkBox19";
            this.checkBox19.Size = new System.Drawing.Size(4, 30);
            this.checkBox19.TabIndex = 58;
            this.checkBox19.UseVisualStyleBackColor = true;
            // 
            // sefQ3_3C
            // 
            this.tableLayoutPanel11.SetColumnSpan(this.sefQ3_3C, 3);
            this.sefQ3_3C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sefQ3_3C.Location = new System.Drawing.Point(83, 303);
            this.sefQ3_3C.Multiline = true;
            this.sefQ3_3C.Name = "sefQ3_3C";
            this.sefQ3_3C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sefQ3_3C.Size = new System.Drawing.Size(689, 60);
            this.sefQ3_3C.TabIndex = 54;
            this.sefQ3_3C.Visible = false;
            // 
            // checkBox20
            // 
            this.checkBox20.AutoSize = true;
            this.checkBox20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox20.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBox20.Location = new System.Drawing.Point(833, 267);
            this.checkBox20.Name = "checkBox20";
            this.checkBox20.Size = new System.Drawing.Size(4, 30);
            this.checkBox20.TabIndex = 52;
            this.checkBox20.UseVisualStyleBackColor = true;
            // 
            // sefQ3_2C
            // 
            this.tableLayoutPanel11.SetColumnSpan(this.sefQ3_2C, 3);
            this.sefQ3_2C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sefQ3_2C.Location = new System.Drawing.Point(83, 181);
            this.sefQ3_2C.Multiline = true;
            this.sefQ3_2C.Name = "sefQ3_2C";
            this.sefQ3_2C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sefQ3_2C.Size = new System.Drawing.Size(689, 60);
            this.sefQ3_2C.TabIndex = 48;
            this.sefQ3_2C.Visible = false;
            // 
            // labelC_sefQ3_1
            // 
            this.labelC_sefQ3_1.AutoSize = true;
            this.tableLayoutPanel11.SetColumnSpan(this.labelC_sefQ3_1, 2);
            this.labelC_sefQ3_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_sefQ3_1.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_sefQ3_1.Location = new System.Drawing.Point(3, 56);
            this.labelC_sefQ3_1.Name = "labelC_sefQ3_1";
            this.labelC_sefQ3_1.Size = new System.Drawing.Size(74, 66);
            this.labelC_sefQ3_1.TabIndex = 45;
            this.labelC_sefQ3_1.Text = "Criterios:";
            this.labelC_sefQ3_1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_sefQ3_1.Visible = false;
            // 
            // sefQ3_1C
            // 
            this.tableLayoutPanel11.SetColumnSpan(this.sefQ3_1C, 3);
            this.sefQ3_1C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sefQ3_1C.Location = new System.Drawing.Point(83, 59);
            this.sefQ3_1C.Multiline = true;
            this.sefQ3_1C.Name = "sefQ3_1C";
            this.sefQ3_1C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sefQ3_1C.Size = new System.Drawing.Size(689, 60);
            this.sefQ3_1C.TabIndex = 32;
            this.sefQ3_1C.Visible = false;
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.tableLayoutPanel11.SetColumnSpan(this.label124, 2);
            this.label124.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label124.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label124.Location = new System.Drawing.Point(43, 508);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(664, 36);
            this.label124.TabIndex = 17;
            this.label124.Text = "¿Estan todos los componentes/dispositivos conservadoramente evaluado (por ejemplo" +
    ", por un factor de 2 o mas)?";
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.tableLayoutPanel11.SetColumnSpan(this.label125, 2);
            this.label125.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label125.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label125.Location = new System.Drawing.Point(43, 386);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(664, 36);
            this.label125.TabIndex = 16;
            this.label125.Text = "¿Las entradas y salidas estan protegidas de los posibles niveles de sobre-tensión" +
    " y sobre-corriente?";
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label126.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label126.Location = new System.Drawing.Point(3, 386);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(34, 36);
            this.label126.TabIndex = 13;
            this.label126.Text = "3.4.";
            this.label126.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label127.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label127.Location = new System.Drawing.Point(3, 264);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(34, 36);
            this.label127.TabIndex = 12;
            this.label127.Text = "3.3.";
            this.label127.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.tableLayoutPanel11.SetColumnSpan(this.label128, 2);
            this.label128.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label128.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label128.Location = new System.Drawing.Point(43, 142);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(664, 36);
            this.label128.TabIndex = 10;
            this.label128.Text = "¿El diseño se basa en tecnicas usadas en equipos que se han utilizado exitosament" +
    "e en el campo por mas de 5 años?";
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label129.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label129.Location = new System.Drawing.Point(3, 142);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(34, 36);
            this.label129.TabIndex = 11;
            this.label129.Text = "3.2.";
            this.label129.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.tableLayoutPanel11.SetColumnSpan(this.label130, 2);
            this.label130.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label130.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label130.Location = new System.Drawing.Point(43, 20);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(664, 36);
            this.label130.TabIndex = 1;
            this.label130.Text = "¿Tiene conexión transversal entre los canales para el intercambio de cualquier in" +
    "formación distinta de la utilizada para las pruebas diagnósticas o fines de la v" +
    "otación?";
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label131.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label131.Location = new System.Drawing.Point(3, 20);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(34, 36);
            this.label131.TabIndex = 9;
            this.label131.Text = "3.1.";
            this.label131.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.tableLayoutPanel11.SetColumnSpan(this.label132, 2);
            this.label132.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label132.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label132.Location = new System.Drawing.Point(43, 264);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(664, 36);
            this.label132.TabIndex = 14;
            this.label132.Text = "¿Hay experiencia de mas de 5 años con el mismo hardware utilizado en condiciones " +
    "similares?";
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label133.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label133.Location = new System.Drawing.Point(3, 508);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(34, 36);
            this.label133.TabIndex = 15;
            this.label133.Text = "3.5.";
            this.label133.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // groupBox48
            // 
            this.tableLayoutPanel11.SetColumnSpan(this.groupBox48, 2);
            this.groupBox48.Controls.Add(this.sefQ3_1S);
            this.groupBox48.Controls.Add(this.sefQ3_1N);
            this.groupBox48.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox48.Location = new System.Drawing.Point(723, 23);
            this.groupBox48.Name = "groupBox48";
            this.groupBox48.Size = new System.Drawing.Size(104, 30);
            this.groupBox48.TabIndex = 81;
            this.groupBox48.TabStop = false;
            // 
            // sefQ3_1S
            // 
            this.sefQ3_1S.AutoSize = true;
            this.sefQ3_1S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ3_1S.Location = new System.Drawing.Point(0, 8);
            this.sefQ3_1S.Name = "sefQ3_1S";
            this.sefQ3_1S.Size = new System.Drawing.Size(38, 22);
            this.sefQ3_1S.TabIndex = 67;
            this.sefQ3_1S.Text = "Si";
            this.sefQ3_1S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ3_1S.UseVisualStyleBackColor = true;
            this.sefQ3_1S.CheckedChanged += new System.EventHandler(this.updatevisibleSEF3);
            // 
            // sefQ3_1N
            // 
            this.sefQ3_1N.AutoSize = true;
            this.sefQ3_1N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ3_1N.Location = new System.Drawing.Point(55, 8);
            this.sefQ3_1N.Name = "sefQ3_1N";
            this.sefQ3_1N.Size = new System.Drawing.Size(47, 22);
            this.sefQ3_1N.TabIndex = 68;
            this.sefQ3_1N.Text = "No";
            this.sefQ3_1N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ3_1N.UseVisualStyleBackColor = true;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.splitContainer12);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(752, 409);
            this.tabPage6.TabIndex = 3;
            this.tabPage6.Text = "Evaluación";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // splitContainer12
            // 
            this.splitContainer12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer12.Location = new System.Drawing.Point(3, 3);
            this.splitContainer12.Name = "splitContainer12";
            this.splitContainer12.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer12.Panel1
            // 
            this.splitContainer12.Panel1.Controls.Add(this.label134);
            // 
            // splitContainer12.Panel2
            // 
            this.splitContainer12.Panel2.AutoScroll = true;
            this.splitContainer12.Panel2.Controls.Add(this.tableLayoutPanel12);
            this.splitContainer12.Size = new System.Drawing.Size(746, 403);
            this.splitContainer12.SplitterDistance = 44;
            this.splitContainer12.TabIndex = 27;
            // 
            // label134
            // 
            this.label134.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label134.Location = new System.Drawing.Point(3, 4);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(828, 42);
            this.label134.TabIndex = 9;
            this.label134.Text = "Para los Sensores y Elementos finales de Control de la SIF, considere los siguien" +
    "tes factores relacionados con la evaluación, análisis y comentarios de los datos" +
    ":";
            this.label134.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.AutoScroll = true;
            this.tableLayoutPanel12.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel12.ColumnCount = 7;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel12.Controls.Add(this.label135, 0, 10);
            this.tableLayoutPanel12.Controls.Add(this.groupBox49, 4, 7);
            this.tableLayoutPanel12.Controls.Add(this.groupBox50, 4, 4);
            this.tableLayoutPanel12.Controls.Add(this.labelC_sefQ4_3, 0, 8);
            this.tableLayoutPanel12.Controls.Add(this.labelC_sefQ4_2, 0, 5);
            this.tableLayoutPanel12.Controls.Add(this.label138, 3, 10);
            this.tableLayoutPanel12.Controls.Add(this.sefQ4_3C, 2, 8);
            this.tableLayoutPanel12.Controls.Add(this.checkBox21, 6, 7);
            this.tableLayoutPanel12.Controls.Add(this.sefQ4_2C, 2, 5);
            this.tableLayoutPanel12.Controls.Add(this.labelC_sefQ4_1, 0, 2);
            this.tableLayoutPanel12.Controls.Add(this.sefQ4_1C, 2, 2);
            this.tableLayoutPanel12.Controls.Add(this.label140, 0, 7);
            this.tableLayoutPanel12.Controls.Add(this.label141, 1, 4);
            this.tableLayoutPanel12.Controls.Add(this.label142, 0, 4);
            this.tableLayoutPanel12.Controls.Add(this.label143, 1, 1);
            this.tableLayoutPanel12.Controls.Add(this.label144, 0, 1);
            this.tableLayoutPanel12.Controls.Add(this.label145, 1, 7);
            this.tableLayoutPanel12.Controls.Add(this.groupBox51, 4, 1);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel12.MinimumSize = new System.Drawing.Size(862, 386);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.tableLayoutPanel12.RowCount = 12;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(862, 386);
            this.tableLayoutPanel12.TabIndex = 2;
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.tableLayoutPanel12.SetColumnSpan(this.label135, 6);
            this.label135.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label135.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label135.Location = new System.Drawing.Point(3, 404);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(824, 18);
            this.label135.TabIndex = 86;
            this.label135.Text = "  CSF Consultoría en Seguridad Funcional   Copyright ©  2019";
            this.label135.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox49
            // 
            this.tableLayoutPanel12.SetColumnSpan(this.groupBox49, 2);
            this.groupBox49.Controls.Add(this.sefQ4_3S);
            this.groupBox49.Controls.Add(this.sefQ4_3N);
            this.groupBox49.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox49.Location = new System.Drawing.Point(723, 285);
            this.groupBox49.Name = "groupBox49";
            this.groupBox49.Size = new System.Drawing.Size(104, 30);
            this.groupBox49.TabIndex = 83;
            this.groupBox49.TabStop = false;
            // 
            // sefQ4_3S
            // 
            this.sefQ4_3S.AutoSize = true;
            this.sefQ4_3S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ4_3S.Location = new System.Drawing.Point(0, 8);
            this.sefQ4_3S.Name = "sefQ4_3S";
            this.sefQ4_3S.Size = new System.Drawing.Size(38, 22);
            this.sefQ4_3S.TabIndex = 73;
            this.sefQ4_3S.Text = "Si";
            this.sefQ4_3S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ4_3S.UseVisualStyleBackColor = true;
            this.sefQ4_3S.CheckedChanged += new System.EventHandler(this.updatevisibleSEF4);
            // 
            // sefQ4_3N
            // 
            this.sefQ4_3N.AutoSize = true;
            this.sefQ4_3N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ4_3N.Location = new System.Drawing.Point(55, 8);
            this.sefQ4_3N.Name = "sefQ4_3N";
            this.sefQ4_3N.Size = new System.Drawing.Size(47, 22);
            this.sefQ4_3N.TabIndex = 74;
            this.sefQ4_3N.Text = "No";
            this.sefQ4_3N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ4_3N.UseVisualStyleBackColor = true;
            // 
            // groupBox50
            // 
            this.tableLayoutPanel12.SetColumnSpan(this.groupBox50, 2);
            this.groupBox50.Controls.Add(this.sefQ4_2S);
            this.groupBox50.Controls.Add(this.sefQ4_2N);
            this.groupBox50.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox50.Location = new System.Drawing.Point(723, 145);
            this.groupBox50.Name = "groupBox50";
            this.groupBox50.Size = new System.Drawing.Size(104, 48);
            this.groupBox50.TabIndex = 82;
            this.groupBox50.TabStop = false;
            // 
            // sefQ4_2S
            // 
            this.sefQ4_2S.AutoSize = true;
            this.sefQ4_2S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ4_2S.Location = new System.Drawing.Point(0, 8);
            this.sefQ4_2S.Name = "sefQ4_2S";
            this.sefQ4_2S.Size = new System.Drawing.Size(38, 22);
            this.sefQ4_2S.TabIndex = 69;
            this.sefQ4_2S.Text = "Si";
            this.sefQ4_2S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ4_2S.UseVisualStyleBackColor = true;
            this.sefQ4_2S.CheckedChanged += new System.EventHandler(this.updatevisibleSEF4);
            // 
            // sefQ4_2N
            // 
            this.sefQ4_2N.AutoSize = true;
            this.sefQ4_2N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ4_2N.Location = new System.Drawing.Point(55, 8);
            this.sefQ4_2N.Name = "sefQ4_2N";
            this.sefQ4_2N.Size = new System.Drawing.Size(47, 22);
            this.sefQ4_2N.TabIndex = 70;
            this.sefQ4_2N.Text = "No";
            this.sefQ4_2N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ4_2N.UseVisualStyleBackColor = true;
            // 
            // labelC_sefQ4_3
            // 
            this.labelC_sefQ4_3.AutoSize = true;
            this.tableLayoutPanel12.SetColumnSpan(this.labelC_sefQ4_3, 2);
            this.labelC_sefQ4_3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_sefQ4_3.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_sefQ4_3.Location = new System.Drawing.Point(3, 318);
            this.labelC_sefQ4_3.Name = "labelC_sefQ4_3";
            this.labelC_sefQ4_3.Size = new System.Drawing.Size(74, 66);
            this.labelC_sefQ4_3.TabIndex = 72;
            this.labelC_sefQ4_3.Text = "Criterios:";
            this.labelC_sefQ4_3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_sefQ4_3.Visible = false;
            // 
            // labelC_sefQ4_2
            // 
            this.labelC_sefQ4_2.AutoSize = true;
            this.tableLayoutPanel12.SetColumnSpan(this.labelC_sefQ4_2, 2);
            this.labelC_sefQ4_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_sefQ4_2.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_sefQ4_2.Location = new System.Drawing.Point(3, 196);
            this.labelC_sefQ4_2.Name = "labelC_sefQ4_2";
            this.labelC_sefQ4_2.Size = new System.Drawing.Size(74, 66);
            this.labelC_sefQ4_2.TabIndex = 71;
            this.labelC_sefQ4_2.Text = "Criterios:";
            this.labelC_sefQ4_2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_sefQ4_2.Visible = false;
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.tableLayoutPanel12.SetColumnSpan(this.label138, 4);
            this.label138.Location = new System.Drawing.Point(3, 422);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(0, 16);
            this.label138.TabIndex = 61;
            // 
            // sefQ4_3C
            // 
            this.tableLayoutPanel12.SetColumnSpan(this.sefQ4_3C, 3);
            this.sefQ4_3C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sefQ4_3C.Location = new System.Drawing.Point(83, 321);
            this.sefQ4_3C.Multiline = true;
            this.sefQ4_3C.Name = "sefQ4_3C";
            this.sefQ4_3C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sefQ4_3C.Size = new System.Drawing.Size(689, 60);
            this.sefQ4_3C.TabIndex = 54;
            this.sefQ4_3C.Visible = false;
            // 
            // checkBox21
            // 
            this.checkBox21.AutoSize = true;
            this.checkBox21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox21.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBox21.Location = new System.Drawing.Point(833, 285);
            this.checkBox21.Name = "checkBox21";
            this.checkBox21.Size = new System.Drawing.Size(4, 30);
            this.checkBox21.TabIndex = 52;
            this.checkBox21.UseVisualStyleBackColor = true;
            // 
            // sefQ4_2C
            // 
            this.tableLayoutPanel12.SetColumnSpan(this.sefQ4_2C, 3);
            this.sefQ4_2C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sefQ4_2C.Location = new System.Drawing.Point(83, 199);
            this.sefQ4_2C.Multiline = true;
            this.sefQ4_2C.Name = "sefQ4_2C";
            this.sefQ4_2C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sefQ4_2C.Size = new System.Drawing.Size(689, 60);
            this.sefQ4_2C.TabIndex = 48;
            this.sefQ4_2C.Visible = false;
            // 
            // labelC_sefQ4_1
            // 
            this.labelC_sefQ4_1.AutoSize = true;
            this.tableLayoutPanel12.SetColumnSpan(this.labelC_sefQ4_1, 2);
            this.labelC_sefQ4_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_sefQ4_1.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_sefQ4_1.Location = new System.Drawing.Point(3, 56);
            this.labelC_sefQ4_1.Name = "labelC_sefQ4_1";
            this.labelC_sefQ4_1.Size = new System.Drawing.Size(74, 66);
            this.labelC_sefQ4_1.TabIndex = 45;
            this.labelC_sefQ4_1.Text = "Criterios:";
            this.labelC_sefQ4_1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_sefQ4_1.Visible = false;
            // 
            // sefQ4_1C
            // 
            this.tableLayoutPanel12.SetColumnSpan(this.sefQ4_1C, 3);
            this.sefQ4_1C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sefQ4_1C.Location = new System.Drawing.Point(83, 59);
            this.sefQ4_1C.Multiline = true;
            this.sefQ4_1C.Name = "sefQ4_1C";
            this.sefQ4_1C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sefQ4_1C.Size = new System.Drawing.Size(689, 60);
            this.sefQ4_1C.TabIndex = 32;
            this.sefQ4_1C.Visible = false;
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label140.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label140.Location = new System.Drawing.Point(3, 282);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(34, 36);
            this.label140.TabIndex = 12;
            this.label140.Text = "4.3.";
            this.label140.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.tableLayoutPanel12.SetColumnSpan(this.label141, 2);
            this.label141.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label141.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label141.Location = new System.Drawing.Point(43, 142);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(664, 54);
            this.label141.TabIndex = 10;
            this.label141.Text = "¿Fueron consideradas las CCF en las revisiones del diseño con los resultados real" +
    "imentado en el diseño? (Se requieren las pruebas documentales de la actividad de" +
    " revisión de diseño.)";
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label142.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label142.Location = new System.Drawing.Point(3, 142);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(34, 54);
            this.label142.TabIndex = 11;
            this.label142.Text = "4.2.";
            this.label142.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.tableLayoutPanel12.SetColumnSpan(this.label143, 2);
            this.label143.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label143.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label143.Location = new System.Drawing.Point(43, 20);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(664, 36);
            this.label143.TabIndex = 1;
            this.label143.Text = "¿Han sido examinados los resultados de la FMEA o FTA para establecer las fuentes " +
    "de CCF y se han determinado las fuentes de CCF que han sido eliminadas por el di" +
    "seño?";
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label144.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label144.Location = new System.Drawing.Point(3, 20);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(34, 36);
            this.label144.TabIndex = 9;
            this.label144.Text = "4.1.";
            this.label144.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.tableLayoutPanel12.SetColumnSpan(this.label145, 2);
            this.label145.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label145.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label145.Location = new System.Drawing.Point(43, 282);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(664, 36);
            this.label145.TabIndex = 14;
            this.label145.Text = "¿Estan todas las fallas de campo completamente analizadas con comentarios en el d" +
    "iseño? (Se requieren las pruebas del procedimiento)";
            // 
            // groupBox51
            // 
            this.tableLayoutPanel12.SetColumnSpan(this.groupBox51, 2);
            this.groupBox51.Controls.Add(this.sefQ4_1S);
            this.groupBox51.Controls.Add(this.sefQ4_1N);
            this.groupBox51.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox51.Location = new System.Drawing.Point(723, 23);
            this.groupBox51.Name = "groupBox51";
            this.groupBox51.Size = new System.Drawing.Size(104, 30);
            this.groupBox51.TabIndex = 81;
            this.groupBox51.TabStop = false;
            // 
            // sefQ4_1S
            // 
            this.sefQ4_1S.AutoSize = true;
            this.sefQ4_1S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ4_1S.Location = new System.Drawing.Point(0, 8);
            this.sefQ4_1S.Name = "sefQ4_1S";
            this.sefQ4_1S.Size = new System.Drawing.Size(38, 22);
            this.sefQ4_1S.TabIndex = 67;
            this.sefQ4_1S.Text = "Si";
            this.sefQ4_1S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ4_1S.UseVisualStyleBackColor = true;
            this.sefQ4_1S.CheckedChanged += new System.EventHandler(this.updatevisibleSEF4);
            // 
            // sefQ4_1N
            // 
            this.sefQ4_1N.AutoSize = true;
            this.sefQ4_1N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ4_1N.Location = new System.Drawing.Point(55, 8);
            this.sefQ4_1N.Name = "sefQ4_1N";
            this.sefQ4_1N.Size = new System.Drawing.Size(47, 22);
            this.sefQ4_1N.TabIndex = 68;
            this.sefQ4_1N.Text = "No";
            this.sefQ4_1N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ4_1N.UseVisualStyleBackColor = true;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.splitContainer13);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(752, 409);
            this.tabPage7.TabIndex = 4;
            this.tabPage7.Text = "Procedimientos";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // splitContainer13
            // 
            this.splitContainer13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer13.Location = new System.Drawing.Point(3, 3);
            this.splitContainer13.Name = "splitContainer13";
            this.splitContainer13.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer13.Panel1
            // 
            this.splitContainer13.Panel1.Controls.Add(this.label146);
            // 
            // splitContainer13.Panel2
            // 
            this.splitContainer13.Panel2.AutoScroll = true;
            this.splitContainer13.Panel2.Controls.Add(this.tableLayoutPanel13);
            this.splitContainer13.Size = new System.Drawing.Size(746, 403);
            this.splitContainer13.SplitterDistance = 44;
            this.splitContainer13.TabIndex = 27;
            // 
            // label146
            // 
            this.label146.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label146.Location = new System.Drawing.Point(3, 4);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(828, 42);
            this.label146.TabIndex = 9;
            this.label146.Text = "Para los Sensores y Elementos finales de Control de la SIF, considere los siguien" +
    "tes factores relacionados con los procedimientos y el error humano:";
            this.label146.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.AutoScroll = true;
            this.tableLayoutPanel13.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel13.ColumnCount = 7;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel13.Controls.Add(this.label153, 0, 16);
            this.tableLayoutPanel13.Controls.Add(this.groupBox54, 4, 13);
            this.tableLayoutPanel13.Controls.Add(this.groupBox55, 4, 10);
            this.tableLayoutPanel13.Controls.Add(this.groupBox56, 4, 7);
            this.tableLayoutPanel13.Controls.Add(this.groupBox57, 4, 4);
            this.tableLayoutPanel13.Controls.Add(this.labelC_sefQ5_5, 0, 14);
            this.tableLayoutPanel13.Controls.Add(this.labelC_sefQ5_4, 0, 11);
            this.tableLayoutPanel13.Controls.Add(this.labelC_sefQ5_3, 0, 8);
            this.tableLayoutPanel13.Controls.Add(this.labelC_sefQ5_2, 0, 5);
            this.tableLayoutPanel13.Controls.Add(this.sefQ5_5C, 2, 14);
            this.tableLayoutPanel13.Controls.Add(this.checkBox22, 6, 13);
            this.tableLayoutPanel13.Controls.Add(this.label158, 3, 16);
            this.tableLayoutPanel13.Controls.Add(this.sefQ5_4C, 2, 11);
            this.tableLayoutPanel13.Controls.Add(this.checkBox23, 6, 10);
            this.tableLayoutPanel13.Controls.Add(this.sefQ5_3C, 2, 8);
            this.tableLayoutPanel13.Controls.Add(this.checkBox24, 6, 7);
            this.tableLayoutPanel13.Controls.Add(this.sefQ5_2C, 2, 5);
            this.tableLayoutPanel13.Controls.Add(this.labelC_sefQ5_1, 0, 2);
            this.tableLayoutPanel13.Controls.Add(this.sefQ5_1C, 2, 2);
            this.tableLayoutPanel13.Controls.Add(this.label160, 1, 13);
            this.tableLayoutPanel13.Controls.Add(this.label161, 1, 10);
            this.tableLayoutPanel13.Controls.Add(this.label162, 0, 10);
            this.tableLayoutPanel13.Controls.Add(this.label163, 0, 7);
            this.tableLayoutPanel13.Controls.Add(this.label164, 1, 4);
            this.tableLayoutPanel13.Controls.Add(this.label165, 0, 4);
            this.tableLayoutPanel13.Controls.Add(this.label166, 1, 1);
            this.tableLayoutPanel13.Controls.Add(this.label167, 0, 1);
            this.tableLayoutPanel13.Controls.Add(this.label168, 1, 7);
            this.tableLayoutPanel13.Controls.Add(this.label169, 0, 13);
            this.tableLayoutPanel13.Controls.Add(this.groupBox58, 4, 1);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel13.MinimumSize = new System.Drawing.Size(862, 386);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.tableLayoutPanel13.RowCount = 18;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(862, 386);
            this.tableLayoutPanel13.TabIndex = 2;
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.tableLayoutPanel13.SetColumnSpan(this.label153, 6);
            this.label153.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label153.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label153.Location = new System.Drawing.Point(3, 738);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(824, 18);
            this.label153.TabIndex = 86;
            this.label153.Text = "  CSF Consultoría en Seguridad Funcional   Copyright ©  2019";
            this.label153.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox54
            // 
            this.tableLayoutPanel13.SetColumnSpan(this.groupBox54, 2);
            this.groupBox54.Controls.Add(this.sefQ5_5N);
            this.groupBox54.Controls.Add(this.sefQ5_5S);
            this.groupBox54.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox54.Location = new System.Drawing.Point(723, 619);
            this.groupBox54.Name = "groupBox54";
            this.groupBox54.Size = new System.Drawing.Size(104, 30);
            this.groupBox54.TabIndex = 85;
            this.groupBox54.TabStop = false;
            // 
            // sefQ5_5N
            // 
            this.sefQ5_5N.AutoSize = true;
            this.sefQ5_5N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ5_5N.Location = new System.Drawing.Point(55, 8);
            this.sefQ5_5N.Name = "sefQ5_5N";
            this.sefQ5_5N.Size = new System.Drawing.Size(47, 22);
            this.sefQ5_5N.TabIndex = 80;
            this.sefQ5_5N.Text = "No";
            this.sefQ5_5N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ5_5N.UseVisualStyleBackColor = true;
            // 
            // sefQ5_5S
            // 
            this.sefQ5_5S.AutoSize = true;
            this.sefQ5_5S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ5_5S.Location = new System.Drawing.Point(0, 8);
            this.sefQ5_5S.Name = "sefQ5_5S";
            this.sefQ5_5S.Size = new System.Drawing.Size(38, 22);
            this.sefQ5_5S.TabIndex = 78;
            this.sefQ5_5S.Text = "Si";
            this.sefQ5_5S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ5_5S.UseVisualStyleBackColor = true;
            this.sefQ5_5S.CheckedChanged += new System.EventHandler(this.updatevisibleSEF5);
            // 
            // groupBox55
            // 
            this.tableLayoutPanel13.SetColumnSpan(this.groupBox55, 2);
            this.groupBox55.Controls.Add(this.sefQ5_4N);
            this.groupBox55.Controls.Add(this.sefQ5_4S);
            this.groupBox55.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox55.Location = new System.Drawing.Point(723, 479);
            this.groupBox55.Name = "groupBox55";
            this.groupBox55.Size = new System.Drawing.Size(104, 48);
            this.groupBox55.TabIndex = 84;
            this.groupBox55.TabStop = false;
            // 
            // sefQ5_4N
            // 
            this.sefQ5_4N.AutoSize = true;
            this.sefQ5_4N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ5_4N.Location = new System.Drawing.Point(55, 8);
            this.sefQ5_4N.Name = "sefQ5_4N";
            this.sefQ5_4N.Size = new System.Drawing.Size(47, 22);
            this.sefQ5_4N.TabIndex = 79;
            this.sefQ5_4N.Text = "No";
            this.sefQ5_4N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ5_4N.UseVisualStyleBackColor = true;
            // 
            // sefQ5_4S
            // 
            this.sefQ5_4S.AutoSize = true;
            this.sefQ5_4S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ5_4S.Location = new System.Drawing.Point(0, 8);
            this.sefQ5_4S.Name = "sefQ5_4S";
            this.sefQ5_4S.Size = new System.Drawing.Size(38, 22);
            this.sefQ5_4S.TabIndex = 77;
            this.sefQ5_4S.Text = "Si";
            this.sefQ5_4S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ5_4S.UseVisualStyleBackColor = true;
            this.sefQ5_4S.CheckedChanged += new System.EventHandler(this.updatevisibleSEF5);
            // 
            // groupBox56
            // 
            this.tableLayoutPanel13.SetColumnSpan(this.groupBox56, 2);
            this.groupBox56.Controls.Add(this.sefQ5_3S);
            this.groupBox56.Controls.Add(this.sefQ5_3N);
            this.groupBox56.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox56.Location = new System.Drawing.Point(723, 339);
            this.groupBox56.Name = "groupBox56";
            this.groupBox56.Size = new System.Drawing.Size(104, 48);
            this.groupBox56.TabIndex = 83;
            this.groupBox56.TabStop = false;
            // 
            // sefQ5_3S
            // 
            this.sefQ5_3S.AutoSize = true;
            this.sefQ5_3S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ5_3S.Location = new System.Drawing.Point(0, 8);
            this.sefQ5_3S.Name = "sefQ5_3S";
            this.sefQ5_3S.Size = new System.Drawing.Size(38, 22);
            this.sefQ5_3S.TabIndex = 73;
            this.sefQ5_3S.Text = "Si";
            this.sefQ5_3S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ5_3S.UseVisualStyleBackColor = true;
            this.sefQ5_3S.CheckedChanged += new System.EventHandler(this.updatevisibleSEF5);
            // 
            // sefQ5_3N
            // 
            this.sefQ5_3N.AutoSize = true;
            this.sefQ5_3N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ5_3N.Location = new System.Drawing.Point(55, 8);
            this.sefQ5_3N.Name = "sefQ5_3N";
            this.sefQ5_3N.Size = new System.Drawing.Size(47, 22);
            this.sefQ5_3N.TabIndex = 74;
            this.sefQ5_3N.Text = "No";
            this.sefQ5_3N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ5_3N.UseVisualStyleBackColor = true;
            // 
            // groupBox57
            // 
            this.tableLayoutPanel13.SetColumnSpan(this.groupBox57, 2);
            this.groupBox57.Controls.Add(this.sefQ5_2S);
            this.groupBox57.Controls.Add(this.sefQ5_2N);
            this.groupBox57.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox57.Location = new System.Drawing.Point(723, 163);
            this.groupBox57.Name = "groupBox57";
            this.groupBox57.Size = new System.Drawing.Size(104, 84);
            this.groupBox57.TabIndex = 82;
            this.groupBox57.TabStop = false;
            // 
            // sefQ5_2S
            // 
            this.sefQ5_2S.AutoSize = true;
            this.sefQ5_2S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ5_2S.Location = new System.Drawing.Point(0, 8);
            this.sefQ5_2S.Name = "sefQ5_2S";
            this.sefQ5_2S.Size = new System.Drawing.Size(38, 22);
            this.sefQ5_2S.TabIndex = 69;
            this.sefQ5_2S.Text = "Si";
            this.sefQ5_2S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ5_2S.UseVisualStyleBackColor = true;
            this.sefQ5_2S.CheckedChanged += new System.EventHandler(this.updatevisibleSEF5);
            // 
            // sefQ5_2N
            // 
            this.sefQ5_2N.AutoSize = true;
            this.sefQ5_2N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ5_2N.Location = new System.Drawing.Point(55, 8);
            this.sefQ5_2N.Name = "sefQ5_2N";
            this.sefQ5_2N.Size = new System.Drawing.Size(47, 22);
            this.sefQ5_2N.TabIndex = 70;
            this.sefQ5_2N.Text = "No";
            this.sefQ5_2N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ5_2N.UseVisualStyleBackColor = true;
            // 
            // labelC_sefQ5_5
            // 
            this.labelC_sefQ5_5.AutoSize = true;
            this.tableLayoutPanel13.SetColumnSpan(this.labelC_sefQ5_5, 2);
            this.labelC_sefQ5_5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_sefQ5_5.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_sefQ5_5.Location = new System.Drawing.Point(3, 652);
            this.labelC_sefQ5_5.Name = "labelC_sefQ5_5";
            this.labelC_sefQ5_5.Size = new System.Drawing.Size(74, 66);
            this.labelC_sefQ5_5.TabIndex = 76;
            this.labelC_sefQ5_5.Text = "Criterios:";
            this.labelC_sefQ5_5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_sefQ5_5.Visible = false;
            // 
            // labelC_sefQ5_4
            // 
            this.labelC_sefQ5_4.AutoSize = true;
            this.tableLayoutPanel13.SetColumnSpan(this.labelC_sefQ5_4, 2);
            this.labelC_sefQ5_4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_sefQ5_4.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_sefQ5_4.Location = new System.Drawing.Point(3, 530);
            this.labelC_sefQ5_4.Name = "labelC_sefQ5_4";
            this.labelC_sefQ5_4.Size = new System.Drawing.Size(74, 66);
            this.labelC_sefQ5_4.TabIndex = 75;
            this.labelC_sefQ5_4.Text = "Criterios:";
            this.labelC_sefQ5_4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_sefQ5_4.Visible = false;
            // 
            // labelC_sefQ5_3
            // 
            this.labelC_sefQ5_3.AutoSize = true;
            this.tableLayoutPanel13.SetColumnSpan(this.labelC_sefQ5_3, 2);
            this.labelC_sefQ5_3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_sefQ5_3.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_sefQ5_3.Location = new System.Drawing.Point(3, 390);
            this.labelC_sefQ5_3.Name = "labelC_sefQ5_3";
            this.labelC_sefQ5_3.Size = new System.Drawing.Size(74, 66);
            this.labelC_sefQ5_3.TabIndex = 72;
            this.labelC_sefQ5_3.Text = "Criterios:";
            this.labelC_sefQ5_3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_sefQ5_3.Visible = false;
            // 
            // labelC_sefQ5_2
            // 
            this.labelC_sefQ5_2.AutoSize = true;
            this.tableLayoutPanel13.SetColumnSpan(this.labelC_sefQ5_2, 2);
            this.labelC_sefQ5_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_sefQ5_2.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_sefQ5_2.Location = new System.Drawing.Point(3, 250);
            this.labelC_sefQ5_2.Name = "labelC_sefQ5_2";
            this.labelC_sefQ5_2.Size = new System.Drawing.Size(74, 66);
            this.labelC_sefQ5_2.TabIndex = 71;
            this.labelC_sefQ5_2.Text = "Criterios:";
            this.labelC_sefQ5_2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_sefQ5_2.Visible = false;
            // 
            // sefQ5_5C
            // 
            this.tableLayoutPanel13.SetColumnSpan(this.sefQ5_5C, 3);
            this.sefQ5_5C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sefQ5_5C.Location = new System.Drawing.Point(83, 655);
            this.sefQ5_5C.Multiline = true;
            this.sefQ5_5C.Name = "sefQ5_5C";
            this.sefQ5_5C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sefQ5_5C.Size = new System.Drawing.Size(689, 60);
            this.sefQ5_5C.TabIndex = 66;
            this.sefQ5_5C.Visible = false;
            // 
            // checkBox22
            // 
            this.checkBox22.AutoSize = true;
            this.checkBox22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox22.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBox22.Location = new System.Drawing.Point(833, 619);
            this.checkBox22.Name = "checkBox22";
            this.checkBox22.Size = new System.Drawing.Size(4, 30);
            this.checkBox22.TabIndex = 64;
            this.checkBox22.UseVisualStyleBackColor = true;
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.tableLayoutPanel13.SetColumnSpan(this.label158, 4);
            this.label158.Location = new System.Drawing.Point(3, 756);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(0, 16);
            this.label158.TabIndex = 61;
            // 
            // sefQ5_4C
            // 
            this.tableLayoutPanel13.SetColumnSpan(this.sefQ5_4C, 3);
            this.sefQ5_4C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sefQ5_4C.Location = new System.Drawing.Point(83, 533);
            this.sefQ5_4C.Multiline = true;
            this.sefQ5_4C.Name = "sefQ5_4C";
            this.sefQ5_4C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sefQ5_4C.Size = new System.Drawing.Size(689, 60);
            this.sefQ5_4C.TabIndex = 60;
            this.sefQ5_4C.Visible = false;
            // 
            // checkBox23
            // 
            this.checkBox23.AutoSize = true;
            this.checkBox23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox23.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBox23.Location = new System.Drawing.Point(833, 479);
            this.checkBox23.Name = "checkBox23";
            this.checkBox23.Size = new System.Drawing.Size(4, 48);
            this.checkBox23.TabIndex = 58;
            this.checkBox23.UseVisualStyleBackColor = true;
            // 
            // sefQ5_3C
            // 
            this.tableLayoutPanel13.SetColumnSpan(this.sefQ5_3C, 3);
            this.sefQ5_3C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sefQ5_3C.Location = new System.Drawing.Point(83, 393);
            this.sefQ5_3C.Multiline = true;
            this.sefQ5_3C.Name = "sefQ5_3C";
            this.sefQ5_3C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sefQ5_3C.Size = new System.Drawing.Size(689, 60);
            this.sefQ5_3C.TabIndex = 54;
            this.sefQ5_3C.Visible = false;
            // 
            // checkBox24
            // 
            this.checkBox24.AutoSize = true;
            this.checkBox24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox24.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBox24.Location = new System.Drawing.Point(833, 339);
            this.checkBox24.Name = "checkBox24";
            this.checkBox24.Size = new System.Drawing.Size(4, 48);
            this.checkBox24.TabIndex = 52;
            this.checkBox24.UseVisualStyleBackColor = true;
            // 
            // sefQ5_2C
            // 
            this.tableLayoutPanel13.SetColumnSpan(this.sefQ5_2C, 3);
            this.sefQ5_2C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sefQ5_2C.Location = new System.Drawing.Point(83, 253);
            this.sefQ5_2C.Multiline = true;
            this.sefQ5_2C.Name = "sefQ5_2C";
            this.sefQ5_2C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sefQ5_2C.Size = new System.Drawing.Size(689, 60);
            this.sefQ5_2C.TabIndex = 48;
            this.sefQ5_2C.Visible = false;
            // 
            // labelC_sefQ5_1
            // 
            this.labelC_sefQ5_1.AutoSize = true;
            this.tableLayoutPanel13.SetColumnSpan(this.labelC_sefQ5_1, 2);
            this.labelC_sefQ5_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_sefQ5_1.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_sefQ5_1.Location = new System.Drawing.Point(3, 74);
            this.labelC_sefQ5_1.Name = "labelC_sefQ5_1";
            this.labelC_sefQ5_1.Size = new System.Drawing.Size(74, 66);
            this.labelC_sefQ5_1.TabIndex = 45;
            this.labelC_sefQ5_1.Text = "Criterios:";
            this.labelC_sefQ5_1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_sefQ5_1.Visible = false;
            // 
            // sefQ5_1C
            // 
            this.tableLayoutPanel13.SetColumnSpan(this.sefQ5_1C, 3);
            this.sefQ5_1C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sefQ5_1C.Location = new System.Drawing.Point(83, 77);
            this.sefQ5_1C.Multiline = true;
            this.sefQ5_1C.Name = "sefQ5_1C";
            this.sefQ5_1C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sefQ5_1C.Size = new System.Drawing.Size(689, 60);
            this.sefQ5_1C.TabIndex = 32;
            this.sefQ5_1C.Visible = false;
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.tableLayoutPanel13.SetColumnSpan(this.label160, 2);
            this.label160.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label160.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label160.Location = new System.Drawing.Point(43, 616);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(664, 36);
            this.label160.TabIndex = 17;
            this.label160.Text = "¿El sistema de pruebas diagnóstica y reporta fallas a nivel de un módulo reemplaz" +
    "able en campo?";
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.tableLayoutPanel13.SetColumnSpan(this.label161, 2);
            this.label161.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label161.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label161.Location = new System.Drawing.Point(43, 476);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(664, 54);
            this.label161.TabIndex = 16;
            this.label161.Text = resources.GetString("label161.Text");
            // 
            // label162
            // 
            this.label162.AutoSize = true;
            this.label162.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label162.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label162.Location = new System.Drawing.Point(3, 476);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(34, 54);
            this.label162.TabIndex = 13;
            this.label162.Text = "5.4.";
            this.label162.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label163.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label163.Location = new System.Drawing.Point(3, 336);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(34, 54);
            this.label163.TabIndex = 12;
            this.label163.Text = "5.3.";
            this.label163.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.tableLayoutPanel13.SetColumnSpan(this.label164, 2);
            this.label164.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label164.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label164.Location = new System.Drawing.Point(43, 160);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(664, 90);
            this.label164.TabIndex = 10;
            this.label164.Text = resources.GetString("label164.Text");
            // 
            // label165
            // 
            this.label165.AutoSize = true;
            this.label165.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label165.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label165.Location = new System.Drawing.Point(3, 160);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(34, 90);
            this.label165.TabIndex = 11;
            this.label165.Text = "5.2.";
            this.label165.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.tableLayoutPanel13.SetColumnSpan(this.label166, 2);
            this.label166.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label166.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label166.Location = new System.Drawing.Point(43, 20);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(664, 54);
            this.label166.TabIndex = 1;
            this.label166.Text = resources.GetString("label166.Text");
            // 
            // label167
            // 
            this.label167.AutoSize = true;
            this.label167.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label167.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label167.Location = new System.Drawing.Point(3, 20);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(34, 54);
            this.label167.TabIndex = 9;
            this.label167.Text = "5.1.";
            this.label167.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.tableLayoutPanel13.SetColumnSpan(this.label168, 2);
            this.label168.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label168.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label168.Location = new System.Drawing.Point(43, 336);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(664, 54);
            this.label168.TabIndex = 14;
            this.label168.Text = resources.GetString("label168.Text");
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label169.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label169.Location = new System.Drawing.Point(3, 616);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(34, 36);
            this.label169.TabIndex = 15;
            this.label169.Text = "5.5.";
            this.label169.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // groupBox58
            // 
            this.tableLayoutPanel13.SetColumnSpan(this.groupBox58, 2);
            this.groupBox58.Controls.Add(this.sefQ5_1S);
            this.groupBox58.Controls.Add(this.sefQ5_1N);
            this.groupBox58.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox58.Location = new System.Drawing.Point(723, 23);
            this.groupBox58.Name = "groupBox58";
            this.groupBox58.Size = new System.Drawing.Size(104, 48);
            this.groupBox58.TabIndex = 81;
            this.groupBox58.TabStop = false;
            // 
            // sefQ5_1S
            // 
            this.sefQ5_1S.AutoSize = true;
            this.sefQ5_1S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ5_1S.Location = new System.Drawing.Point(0, 8);
            this.sefQ5_1S.Name = "sefQ5_1S";
            this.sefQ5_1S.Size = new System.Drawing.Size(38, 22);
            this.sefQ5_1S.TabIndex = 67;
            this.sefQ5_1S.Text = "Si";
            this.sefQ5_1S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ5_1S.UseVisualStyleBackColor = true;
            this.sefQ5_1S.CheckedChanged += new System.EventHandler(this.updatevisibleSEF5);
            // 
            // sefQ5_1N
            // 
            this.sefQ5_1N.AutoSize = true;
            this.sefQ5_1N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ5_1N.Location = new System.Drawing.Point(55, 8);
            this.sefQ5_1N.Name = "sefQ5_1N";
            this.sefQ5_1N.Size = new System.Drawing.Size(47, 22);
            this.sefQ5_1N.TabIndex = 68;
            this.sefQ5_1N.Text = "No";
            this.sefQ5_1N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ5_1N.UseVisualStyleBackColor = true;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.splitContainer14);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(752, 409);
            this.tabPage8.TabIndex = 5;
            this.tabPage8.Text = "Entrenamiento";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // splitContainer14
            // 
            this.splitContainer14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer14.Location = new System.Drawing.Point(3, 3);
            this.splitContainer14.Name = "splitContainer14";
            this.splitContainer14.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer14.Panel1
            // 
            this.splitContainer14.Panel1.Controls.Add(this.label170);
            // 
            // splitContainer14.Panel2
            // 
            this.splitContainer14.Panel2.AutoScroll = true;
            this.splitContainer14.Panel2.Controls.Add(this.tableLayoutPanel14);
            this.splitContainer14.Size = new System.Drawing.Size(746, 403);
            this.splitContainer14.SplitterDistance = 44;
            this.splitContainer14.TabIndex = 26;
            // 
            // label170
            // 
            this.label170.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label170.Location = new System.Drawing.Point(3, 4);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(828, 42);
            this.label170.TabIndex = 9;
            this.label170.Text = "Para los Sensores y Elementos finales de Control de la SIF, considere los siguien" +
    "tes factores relacionados con las competencias del personal, su capacitación y l" +
    "a cultura de seguridad de la empresa:";
            this.label170.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.AutoScroll = true;
            this.tableLayoutPanel14.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel14.ColumnCount = 7;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel14.Controls.Add(this.label171, 0, 7);
            this.tableLayoutPanel14.Controls.Add(this.groupBox59, 4, 4);
            this.tableLayoutPanel14.Controls.Add(this.labelC_sefQ6_2, 0, 5);
            this.tableLayoutPanel14.Controls.Add(this.sefQ6_2C, 2, 5);
            this.tableLayoutPanel14.Controls.Add(this.labelC_sefQ6_1, 0, 2);
            this.tableLayoutPanel14.Controls.Add(this.sefQ6_1C, 2, 2);
            this.tableLayoutPanel14.Controls.Add(this.label174, 1, 4);
            this.tableLayoutPanel14.Controls.Add(this.label175, 0, 4);
            this.tableLayoutPanel14.Controls.Add(this.label176, 1, 1);
            this.tableLayoutPanel14.Controls.Add(this.label177, 0, 1);
            this.tableLayoutPanel14.Controls.Add(this.groupBox60, 4, 1);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel14.MinimumSize = new System.Drawing.Size(862, 386);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.tableLayoutPanel14.RowCount = 9;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(862, 386);
            this.tableLayoutPanel14.TabIndex = 2;
            // 
            // label171
            // 
            this.label171.AutoSize = true;
            this.tableLayoutPanel14.SetColumnSpan(this.label171, 6);
            this.label171.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label171.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label171.Location = new System.Drawing.Point(3, 264);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(841, 18);
            this.label171.TabIndex = 86;
            this.label171.Text = "  CSF Consultoría en Seguridad Funcional   Copyright ©  2019";
            this.label171.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox59
            // 
            this.tableLayoutPanel14.SetColumnSpan(this.groupBox59, 2);
            this.groupBox59.Controls.Add(this.sefQ6_2S);
            this.groupBox59.Controls.Add(this.sefQ6_2N);
            this.groupBox59.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox59.Location = new System.Drawing.Point(740, 145);
            this.groupBox59.Name = "groupBox59";
            this.groupBox59.Size = new System.Drawing.Size(104, 30);
            this.groupBox59.TabIndex = 82;
            this.groupBox59.TabStop = false;
            // 
            // sefQ6_2S
            // 
            this.sefQ6_2S.AutoSize = true;
            this.sefQ6_2S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ6_2S.Location = new System.Drawing.Point(0, 8);
            this.sefQ6_2S.Name = "sefQ6_2S";
            this.sefQ6_2S.Size = new System.Drawing.Size(38, 22);
            this.sefQ6_2S.TabIndex = 69;
            this.sefQ6_2S.Text = "Si";
            this.sefQ6_2S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ6_2S.UseVisualStyleBackColor = true;
            this.sefQ6_2S.CheckedChanged += new System.EventHandler(this.updatevisibleSEF6);
            // 
            // sefQ6_2N
            // 
            this.sefQ6_2N.AutoSize = true;
            this.sefQ6_2N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ6_2N.Location = new System.Drawing.Point(55, 8);
            this.sefQ6_2N.Name = "sefQ6_2N";
            this.sefQ6_2N.Size = new System.Drawing.Size(47, 22);
            this.sefQ6_2N.TabIndex = 70;
            this.sefQ6_2N.Text = "No";
            this.sefQ6_2N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ6_2N.UseVisualStyleBackColor = true;
            // 
            // labelC_sefQ6_2
            // 
            this.labelC_sefQ6_2.AutoSize = true;
            this.tableLayoutPanel14.SetColumnSpan(this.labelC_sefQ6_2, 2);
            this.labelC_sefQ6_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_sefQ6_2.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_sefQ6_2.Location = new System.Drawing.Point(3, 178);
            this.labelC_sefQ6_2.Name = "labelC_sefQ6_2";
            this.labelC_sefQ6_2.Size = new System.Drawing.Size(74, 66);
            this.labelC_sefQ6_2.TabIndex = 71;
            this.labelC_sefQ6_2.Text = "Criterios:";
            this.labelC_sefQ6_2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_sefQ6_2.Visible = false;
            // 
            // sefQ6_2C
            // 
            this.tableLayoutPanel14.SetColumnSpan(this.sefQ6_2C, 3);
            this.sefQ6_2C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sefQ6_2C.Location = new System.Drawing.Point(83, 181);
            this.sefQ6_2C.Multiline = true;
            this.sefQ6_2C.Name = "sefQ6_2C";
            this.sefQ6_2C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sefQ6_2C.Size = new System.Drawing.Size(706, 60);
            this.sefQ6_2C.TabIndex = 48;
            this.sefQ6_2C.Visible = false;
            // 
            // labelC_sefQ6_1
            // 
            this.labelC_sefQ6_1.AutoSize = true;
            this.tableLayoutPanel14.SetColumnSpan(this.labelC_sefQ6_1, 2);
            this.labelC_sefQ6_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_sefQ6_1.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_sefQ6_1.Location = new System.Drawing.Point(3, 56);
            this.labelC_sefQ6_1.Name = "labelC_sefQ6_1";
            this.labelC_sefQ6_1.Size = new System.Drawing.Size(74, 66);
            this.labelC_sefQ6_1.TabIndex = 45;
            this.labelC_sefQ6_1.Text = "Criterios:";
            this.labelC_sefQ6_1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_sefQ6_1.Visible = false;
            // 
            // sefQ6_1C
            // 
            this.tableLayoutPanel14.SetColumnSpan(this.sefQ6_1C, 3);
            this.sefQ6_1C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sefQ6_1C.Location = new System.Drawing.Point(83, 59);
            this.sefQ6_1C.Multiline = true;
            this.sefQ6_1C.Name = "sefQ6_1C";
            this.sefQ6_1C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sefQ6_1C.Size = new System.Drawing.Size(706, 60);
            this.sefQ6_1C.TabIndex = 32;
            this.sefQ6_1C.Visible = false;
            // 
            // label174
            // 
            this.label174.AutoSize = true;
            this.tableLayoutPanel14.SetColumnSpan(this.label174, 2);
            this.label174.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label174.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label174.Location = new System.Drawing.Point(43, 142);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(681, 36);
            this.label174.TabIndex = 10;
            this.label174.Text = "¿Ha sido entrenado el personal de mantenimiento (con la documentación de formació" +
    "n) para comprender las causas y consecuencias de las fallas de causa común?";
            // 
            // label175
            // 
            this.label175.AutoSize = true;
            this.label175.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label175.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label175.Location = new System.Drawing.Point(3, 142);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(34, 36);
            this.label175.TabIndex = 11;
            this.label175.Text = "6.2.";
            this.label175.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label176
            // 
            this.label176.AutoSize = true;
            this.tableLayoutPanel14.SetColumnSpan(this.label176, 2);
            this.label176.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label176.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label176.Location = new System.Drawing.Point(43, 20);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(681, 36);
            this.label176.TabIndex = 1;
            this.label176.Text = "¿Han sido entrenados los diseñadores (con la documentación de formación) para com" +
    "prender las causas y consecuencias de las fallas de causa común?";
            // 
            // label177
            // 
            this.label177.AutoSize = true;
            this.label177.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label177.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label177.Location = new System.Drawing.Point(3, 20);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(34, 36);
            this.label177.TabIndex = 9;
            this.label177.Text = "6.1.";
            this.label177.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // groupBox60
            // 
            this.tableLayoutPanel14.SetColumnSpan(this.groupBox60, 2);
            this.groupBox60.Controls.Add(this.sefQ6_1S);
            this.groupBox60.Controls.Add(this.sefQ6_1N);
            this.groupBox60.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox60.Location = new System.Drawing.Point(740, 23);
            this.groupBox60.Name = "groupBox60";
            this.groupBox60.Size = new System.Drawing.Size(104, 30);
            this.groupBox60.TabIndex = 81;
            this.groupBox60.TabStop = false;
            // 
            // sefQ6_1S
            // 
            this.sefQ6_1S.AutoSize = true;
            this.sefQ6_1S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ6_1S.Location = new System.Drawing.Point(0, 8);
            this.sefQ6_1S.Name = "sefQ6_1S";
            this.sefQ6_1S.Size = new System.Drawing.Size(38, 22);
            this.sefQ6_1S.TabIndex = 67;
            this.sefQ6_1S.Text = "Si";
            this.sefQ6_1S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ6_1S.UseVisualStyleBackColor = true;
            this.sefQ6_1S.CheckedChanged += new System.EventHandler(this.updatevisibleSEF6);
            // 
            // sefQ6_1N
            // 
            this.sefQ6_1N.AutoSize = true;
            this.sefQ6_1N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ6_1N.Location = new System.Drawing.Point(55, 8);
            this.sefQ6_1N.Name = "sefQ6_1N";
            this.sefQ6_1N.Size = new System.Drawing.Size(47, 22);
            this.sefQ6_1N.TabIndex = 68;
            this.sefQ6_1N.Text = "No";
            this.sefQ6_1N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ6_1N.UseVisualStyleBackColor = true;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.splitContainer16);
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(752, 409);
            this.tabPage9.TabIndex = 6;
            this.tabPage9.Text = "Ambiente";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // splitContainer16
            // 
            this.splitContainer16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer16.Location = new System.Drawing.Point(3, 3);
            this.splitContainer16.Name = "splitContainer16";
            this.splitContainer16.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer16.Panel1
            // 
            this.splitContainer16.Panel1.Controls.Add(this.label178);
            // 
            // splitContainer16.Panel2
            // 
            this.splitContainer16.Panel2.AutoScroll = true;
            this.splitContainer16.Panel2.Controls.Add(this.tableLayoutPanel15);
            this.splitContainer16.Size = new System.Drawing.Size(746, 403);
            this.splitContainer16.SplitterDistance = 44;
            this.splitContainer16.TabIndex = 28;
            // 
            // label178
            // 
            this.label178.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label178.Location = new System.Drawing.Point(3, 4);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(828, 42);
            this.label178.TabIndex = 9;
            this.label178.Text = "Para los Sensores y Elementos finales de Control de la SIF, considere los siguien" +
    "tes factores relacionados el control y pruebas ambientales del sistema:";
            this.label178.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.AutoScroll = true;
            this.tableLayoutPanel15.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel15.ColumnCount = 7;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel15.Controls.Add(this.label179, 0, 13);
            this.tableLayoutPanel15.Controls.Add(this.groupBox61, 4, 10);
            this.tableLayoutPanel15.Controls.Add(this.groupBox62, 4, 7);
            this.tableLayoutPanel15.Controls.Add(this.groupBox63, 4, 4);
            this.tableLayoutPanel15.Controls.Add(this.labelC_sefQ7_4, 0, 11);
            this.tableLayoutPanel15.Controls.Add(this.labelC_sefQ7_3, 0, 8);
            this.tableLayoutPanel15.Controls.Add(this.labelC_sefQ7_2, 0, 5);
            this.tableLayoutPanel15.Controls.Add(this.label183, 3, 13);
            this.tableLayoutPanel15.Controls.Add(this.sefQ7_4C, 2, 11);
            this.tableLayoutPanel15.Controls.Add(this.checkBox25, 6, 10);
            this.tableLayoutPanel15.Controls.Add(this.sefQ7_3C, 2, 8);
            this.tableLayoutPanel15.Controls.Add(this.checkBox26, 6, 7);
            this.tableLayoutPanel15.Controls.Add(this.sefQ7_2C, 2, 5);
            this.tableLayoutPanel15.Controls.Add(this.labelC_sefQ7_1, 0, 2);
            this.tableLayoutPanel15.Controls.Add(this.sefQ7_1C, 2, 2);
            this.tableLayoutPanel15.Controls.Add(this.label185, 1, 10);
            this.tableLayoutPanel15.Controls.Add(this.label186, 0, 10);
            this.tableLayoutPanel15.Controls.Add(this.label187, 0, 7);
            this.tableLayoutPanel15.Controls.Add(this.label188, 1, 4);
            this.tableLayoutPanel15.Controls.Add(this.label189, 0, 4);
            this.tableLayoutPanel15.Controls.Add(this.label190, 1, 1);
            this.tableLayoutPanel15.Controls.Add(this.label191, 0, 1);
            this.tableLayoutPanel15.Controls.Add(this.label192, 1, 7);
            this.tableLayoutPanel15.Controls.Add(this.groupBox64, 4, 1);
            this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel15.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel15.MinimumSize = new System.Drawing.Size(862, 386);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.Padding = new System.Windows.Forms.Padding(0, 0, 5, 0);
            this.tableLayoutPanel15.RowCount = 15;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(862, 386);
            this.tableLayoutPanel15.TabIndex = 2;
            // 
            // label179
            // 
            this.label179.AutoSize = true;
            this.tableLayoutPanel15.SetColumnSpan(this.label179, 6);
            this.label179.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label179.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label179.Location = new System.Drawing.Point(3, 544);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(824, 18);
            this.label179.TabIndex = 86;
            this.label179.Text = "  CSF Consultoría en Seguridad Funcional   Copyright ©  2019";
            this.label179.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox61
            // 
            this.tableLayoutPanel15.SetColumnSpan(this.groupBox61, 2);
            this.groupBox61.Controls.Add(this.sefQ7_4N);
            this.groupBox61.Controls.Add(this.sefQ7_4S);
            this.groupBox61.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox61.Location = new System.Drawing.Point(723, 407);
            this.groupBox61.Name = "groupBox61";
            this.groupBox61.Size = new System.Drawing.Size(104, 48);
            this.groupBox61.TabIndex = 84;
            this.groupBox61.TabStop = false;
            // 
            // sefQ7_4N
            // 
            this.sefQ7_4N.AutoSize = true;
            this.sefQ7_4N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ7_4N.Location = new System.Drawing.Point(55, 8);
            this.sefQ7_4N.Name = "sefQ7_4N";
            this.sefQ7_4N.Size = new System.Drawing.Size(47, 22);
            this.sefQ7_4N.TabIndex = 79;
            this.sefQ7_4N.Text = "No";
            this.sefQ7_4N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ7_4N.UseVisualStyleBackColor = true;
            // 
            // sefQ7_4S
            // 
            this.sefQ7_4S.AutoSize = true;
            this.sefQ7_4S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ7_4S.Location = new System.Drawing.Point(0, 8);
            this.sefQ7_4S.Name = "sefQ7_4S";
            this.sefQ7_4S.Size = new System.Drawing.Size(38, 22);
            this.sefQ7_4S.TabIndex = 77;
            this.sefQ7_4S.Text = "Si";
            this.sefQ7_4S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ7_4S.UseVisualStyleBackColor = true;
            this.sefQ7_4S.CheckedChanged += new System.EventHandler(this.updatevisibleSEF7);
            // 
            // groupBox62
            // 
            this.tableLayoutPanel15.SetColumnSpan(this.groupBox62, 2);
            this.groupBox62.Controls.Add(this.sefQ7_3S);
            this.groupBox62.Controls.Add(this.sefQ7_3N);
            this.groupBox62.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox62.Location = new System.Drawing.Point(723, 285);
            this.groupBox62.Name = "groupBox62";
            this.groupBox62.Size = new System.Drawing.Size(104, 30);
            this.groupBox62.TabIndex = 83;
            this.groupBox62.TabStop = false;
            // 
            // sefQ7_3S
            // 
            this.sefQ7_3S.AutoSize = true;
            this.sefQ7_3S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ7_3S.Location = new System.Drawing.Point(0, 8);
            this.sefQ7_3S.Name = "sefQ7_3S";
            this.sefQ7_3S.Size = new System.Drawing.Size(38, 22);
            this.sefQ7_3S.TabIndex = 73;
            this.sefQ7_3S.Text = "Si";
            this.sefQ7_3S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ7_3S.UseVisualStyleBackColor = true;
            this.sefQ7_3S.CheckedChanged += new System.EventHandler(this.updatevisibleSEF7);
            // 
            // sefQ7_3N
            // 
            this.sefQ7_3N.AutoSize = true;
            this.sefQ7_3N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ7_3N.Location = new System.Drawing.Point(55, 8);
            this.sefQ7_3N.Name = "sefQ7_3N";
            this.sefQ7_3N.Size = new System.Drawing.Size(47, 22);
            this.sefQ7_3N.TabIndex = 74;
            this.sefQ7_3N.Text = "No";
            this.sefQ7_3N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ7_3N.UseVisualStyleBackColor = true;
            // 
            // groupBox63
            // 
            this.tableLayoutPanel15.SetColumnSpan(this.groupBox63, 2);
            this.groupBox63.Controls.Add(this.sefQ7_2S);
            this.groupBox63.Controls.Add(this.sefQ7_2N);
            this.groupBox63.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox63.Location = new System.Drawing.Point(723, 145);
            this.groupBox63.Name = "groupBox63";
            this.groupBox63.Size = new System.Drawing.Size(104, 48);
            this.groupBox63.TabIndex = 82;
            this.groupBox63.TabStop = false;
            // 
            // sefQ7_2S
            // 
            this.sefQ7_2S.AutoSize = true;
            this.sefQ7_2S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ7_2S.Location = new System.Drawing.Point(0, 8);
            this.sefQ7_2S.Name = "sefQ7_2S";
            this.sefQ7_2S.Size = new System.Drawing.Size(38, 22);
            this.sefQ7_2S.TabIndex = 69;
            this.sefQ7_2S.Text = "Si";
            this.sefQ7_2S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ7_2S.UseVisualStyleBackColor = true;
            this.sefQ7_2S.CheckedChanged += new System.EventHandler(this.updatevisibleSEF7);
            // 
            // sefQ7_2N
            // 
            this.sefQ7_2N.AutoSize = true;
            this.sefQ7_2N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ7_2N.Location = new System.Drawing.Point(55, 8);
            this.sefQ7_2N.Name = "sefQ7_2N";
            this.sefQ7_2N.Size = new System.Drawing.Size(47, 22);
            this.sefQ7_2N.TabIndex = 70;
            this.sefQ7_2N.Text = "No";
            this.sefQ7_2N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ7_2N.UseVisualStyleBackColor = true;
            // 
            // labelC_sefQ7_4
            // 
            this.labelC_sefQ7_4.AutoSize = true;
            this.tableLayoutPanel15.SetColumnSpan(this.labelC_sefQ7_4, 2);
            this.labelC_sefQ7_4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_sefQ7_4.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_sefQ7_4.Location = new System.Drawing.Point(3, 458);
            this.labelC_sefQ7_4.Name = "labelC_sefQ7_4";
            this.labelC_sefQ7_4.Size = new System.Drawing.Size(74, 66);
            this.labelC_sefQ7_4.TabIndex = 75;
            this.labelC_sefQ7_4.Text = "Criterios:";
            this.labelC_sefQ7_4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_sefQ7_4.Visible = false;
            // 
            // labelC_sefQ7_3
            // 
            this.labelC_sefQ7_3.AutoSize = true;
            this.tableLayoutPanel15.SetColumnSpan(this.labelC_sefQ7_3, 2);
            this.labelC_sefQ7_3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_sefQ7_3.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_sefQ7_3.Location = new System.Drawing.Point(3, 318);
            this.labelC_sefQ7_3.Name = "labelC_sefQ7_3";
            this.labelC_sefQ7_3.Size = new System.Drawing.Size(74, 66);
            this.labelC_sefQ7_3.TabIndex = 72;
            this.labelC_sefQ7_3.Text = "Criterios:";
            this.labelC_sefQ7_3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_sefQ7_3.Visible = false;
            // 
            // labelC_sefQ7_2
            // 
            this.labelC_sefQ7_2.AutoSize = true;
            this.tableLayoutPanel15.SetColumnSpan(this.labelC_sefQ7_2, 2);
            this.labelC_sefQ7_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_sefQ7_2.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_sefQ7_2.Location = new System.Drawing.Point(3, 196);
            this.labelC_sefQ7_2.Name = "labelC_sefQ7_2";
            this.labelC_sefQ7_2.Size = new System.Drawing.Size(74, 66);
            this.labelC_sefQ7_2.TabIndex = 71;
            this.labelC_sefQ7_2.Text = "Criterios:";
            this.labelC_sefQ7_2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_sefQ7_2.Visible = false;
            // 
            // label183
            // 
            this.label183.AutoSize = true;
            this.tableLayoutPanel15.SetColumnSpan(this.label183, 4);
            this.label183.Location = new System.Drawing.Point(3, 562);
            this.label183.Name = "label183";
            this.label183.Size = new System.Drawing.Size(0, 16);
            this.label183.TabIndex = 61;
            // 
            // sefQ7_4C
            // 
            this.tableLayoutPanel15.SetColumnSpan(this.sefQ7_4C, 3);
            this.sefQ7_4C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sefQ7_4C.Location = new System.Drawing.Point(83, 461);
            this.sefQ7_4C.Multiline = true;
            this.sefQ7_4C.Name = "sefQ7_4C";
            this.sefQ7_4C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sefQ7_4C.Size = new System.Drawing.Size(689, 60);
            this.sefQ7_4C.TabIndex = 60;
            this.sefQ7_4C.Visible = false;
            // 
            // checkBox25
            // 
            this.checkBox25.AutoSize = true;
            this.checkBox25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox25.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBox25.Location = new System.Drawing.Point(833, 407);
            this.checkBox25.Name = "checkBox25";
            this.checkBox25.Size = new System.Drawing.Size(4, 48);
            this.checkBox25.TabIndex = 58;
            this.checkBox25.UseVisualStyleBackColor = true;
            // 
            // sefQ7_3C
            // 
            this.tableLayoutPanel15.SetColumnSpan(this.sefQ7_3C, 3);
            this.sefQ7_3C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sefQ7_3C.Location = new System.Drawing.Point(83, 321);
            this.sefQ7_3C.Multiline = true;
            this.sefQ7_3C.Name = "sefQ7_3C";
            this.sefQ7_3C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sefQ7_3C.Size = new System.Drawing.Size(689, 60);
            this.sefQ7_3C.TabIndex = 54;
            this.sefQ7_3C.Visible = false;
            // 
            // checkBox26
            // 
            this.checkBox26.AutoSize = true;
            this.checkBox26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox26.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.checkBox26.Location = new System.Drawing.Point(833, 285);
            this.checkBox26.Name = "checkBox26";
            this.checkBox26.Size = new System.Drawing.Size(4, 30);
            this.checkBox26.TabIndex = 52;
            this.checkBox26.UseVisualStyleBackColor = true;
            // 
            // sefQ7_2C
            // 
            this.tableLayoutPanel15.SetColumnSpan(this.sefQ7_2C, 3);
            this.sefQ7_2C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sefQ7_2C.Location = new System.Drawing.Point(83, 199);
            this.sefQ7_2C.Multiline = true;
            this.sefQ7_2C.Name = "sefQ7_2C";
            this.sefQ7_2C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sefQ7_2C.Size = new System.Drawing.Size(689, 60);
            this.sefQ7_2C.TabIndex = 48;
            this.sefQ7_2C.Visible = false;
            // 
            // labelC_sefQ7_1
            // 
            this.labelC_sefQ7_1.AutoSize = true;
            this.tableLayoutPanel15.SetColumnSpan(this.labelC_sefQ7_1, 2);
            this.labelC_sefQ7_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelC_sefQ7_1.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelC_sefQ7_1.Location = new System.Drawing.Point(3, 57);
            this.labelC_sefQ7_1.Name = "labelC_sefQ7_1";
            this.labelC_sefQ7_1.Size = new System.Drawing.Size(74, 66);
            this.labelC_sefQ7_1.TabIndex = 45;
            this.labelC_sefQ7_1.Text = "Criterios:";
            this.labelC_sefQ7_1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelC_sefQ7_1.Visible = false;
            // 
            // sefQ7_1C
            // 
            this.tableLayoutPanel15.SetColumnSpan(this.sefQ7_1C, 3);
            this.sefQ7_1C.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sefQ7_1C.Location = new System.Drawing.Point(83, 60);
            this.sefQ7_1C.Multiline = true;
            this.sefQ7_1C.Name = "sefQ7_1C";
            this.sefQ7_1C.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.sefQ7_1C.Size = new System.Drawing.Size(689, 60);
            this.sefQ7_1C.TabIndex = 32;
            this.sefQ7_1C.Visible = false;
            // 
            // label185
            // 
            this.label185.AutoSize = true;
            this.tableLayoutPanel15.SetColumnSpan(this.label185, 2);
            this.label185.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label185.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label185.Location = new System.Drawing.Point(43, 404);
            this.label185.Name = "label185";
            this.label185.Size = new System.Drawing.Size(664, 54);
            this.label185.TabIndex = 16;
            this.label185.Text = resources.GetString("label185.Text");
            // 
            // label186
            // 
            this.label186.AutoSize = true;
            this.label186.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label186.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label186.Location = new System.Drawing.Point(3, 404);
            this.label186.Name = "label186";
            this.label186.Size = new System.Drawing.Size(34, 54);
            this.label186.TabIndex = 13;
            this.label186.Text = "7.4.";
            this.label186.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label187
            // 
            this.label187.AutoSize = true;
            this.label187.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label187.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label187.Location = new System.Drawing.Point(3, 282);
            this.label187.Name = "label187";
            this.label187.Size = new System.Drawing.Size(34, 36);
            this.label187.TabIndex = 12;
            this.label187.Text = "7.3.";
            this.label187.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label188
            // 
            this.label188.AutoSize = true;
            this.tableLayoutPanel15.SetColumnSpan(this.label188, 2);
            this.label188.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label188.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label188.Location = new System.Drawing.Point(43, 142);
            this.label188.Name = "label188";
            this.label188.Size = new System.Drawing.Size(664, 54);
            this.label188.TabIndex = 10;
            this.label188.Text = "¿El sistema puede operar siempre dentro del rango de temperatura, humedad, corros" +
    "ión, polvo, vibraciones, etc, sobre el cual ha sido probado sin el uso del contr" +
    "ol del medio ambiente externo?";
            // 
            // label189
            // 
            this.label189.AutoSize = true;
            this.label189.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label189.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label189.Location = new System.Drawing.Point(3, 142);
            this.label189.Name = "label189";
            this.label189.Size = new System.Drawing.Size(34, 54);
            this.label189.TabIndex = 11;
            this.label189.Text = "7.2.";
            this.label189.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label190
            // 
            this.label190.AutoSize = true;
            this.tableLayoutPanel15.SetColumnSpan(this.label190, 2);
            this.label190.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label190.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label190.Location = new System.Drawing.Point(43, 21);
            this.label190.Name = "label190";
            this.label190.Size = new System.Drawing.Size(664, 36);
            this.label190.TabIndex = 1;
            this.label190.Text = "¿Esta limitado el acceso del personal (por ejemplo, gabinetes cerrados, posición " +
    "inaccesible)?";
            // 
            // label191
            // 
            this.label191.AutoSize = true;
            this.label191.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label191.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label191.Location = new System.Drawing.Point(3, 21);
            this.label191.Name = "label191";
            this.label191.Size = new System.Drawing.Size(34, 36);
            this.label191.TabIndex = 9;
            this.label191.Text = "7.1.";
            this.label191.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label192
            // 
            this.label192.AutoSize = true;
            this.tableLayoutPanel15.SetColumnSpan(this.label192, 2);
            this.label192.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label192.Font = new System.Drawing.Font("Raleway", 11.25F, System.Drawing.FontStyle.Bold);
            this.label192.Location = new System.Drawing.Point(43, 282);
            this.label192.Name = "label192";
            this.label192.Size = new System.Drawing.Size(664, 36);
            this.label192.TabIndex = 14;
            this.label192.Text = "¿Estan todos los cables de señal y alimentación separados en todas las posiciones" +
    "?";
            // 
            // groupBox64
            // 
            this.tableLayoutPanel15.SetColumnSpan(this.groupBox64, 2);
            this.groupBox64.Controls.Add(this.sefQ7_1S);
            this.groupBox64.Controls.Add(this.sefQ7_1N);
            this.groupBox64.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox64.Location = new System.Drawing.Point(723, 24);
            this.groupBox64.Name = "groupBox64";
            this.groupBox64.Size = new System.Drawing.Size(104, 30);
            this.groupBox64.TabIndex = 81;
            this.groupBox64.TabStop = false;
            // 
            // sefQ7_1S
            // 
            this.sefQ7_1S.AutoSize = true;
            this.sefQ7_1S.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ7_1S.Location = new System.Drawing.Point(0, 8);
            this.sefQ7_1S.Name = "sefQ7_1S";
            this.sefQ7_1S.Size = new System.Drawing.Size(38, 22);
            this.sefQ7_1S.TabIndex = 67;
            this.sefQ7_1S.Text = "Si";
            this.sefQ7_1S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ7_1S.UseVisualStyleBackColor = true;
            this.sefQ7_1S.CheckedChanged += new System.EventHandler(this.updatevisibleSEF7);
            // 
            // sefQ7_1N
            // 
            this.sefQ7_1N.AutoSize = true;
            this.sefQ7_1N.Font = new System.Drawing.Font("Raleway", 11.25F);
            this.sefQ7_1N.Location = new System.Drawing.Point(55, 8);
            this.sefQ7_1N.Name = "sefQ7_1N";
            this.sefQ7_1N.Size = new System.Drawing.Size(47, 22);
            this.sefQ7_1N.TabIndex = 68;
            this.sefQ7_1N.Text = "No";
            this.sefQ7_1N.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.sefQ7_1N.UseVisualStyleBackColor = true;
            // 
            // tabPage10
            // 
            this.tabPage10.Location = new System.Drawing.Point(4, 22);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Size = new System.Drawing.Size(752, 409);
            this.tabPage10.TabIndex = 7;
            this.tabPage10.Text = "Datos";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // tabPage11
            // 
            this.tabPage11.Location = new System.Drawing.Point(4, 22);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Size = new System.Drawing.Size(752, 409);
            this.tabPage11.TabIndex = 8;
            this.tabPage11.Text = "Resultados";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // menuStrip1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.menuStrip1, 4);
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem,
            this.metodoToolStripMenuItem,
            this.reporteToolStripMenuItem,
            this.ayudaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(50, 548);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(242, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuevoToolStripMenuItem,
            this.abrirToolStripMenuItem,
            this.guardarToolStripMenuItem,
            this.guardarComoToolStripMenuItem,
            this.cerrarToolStripMenuItem});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.archivoToolStripMenuItem.Text = "Archivo";
            // 
            // nuevoToolStripMenuItem
            // 
            this.nuevoToolStripMenuItem.Name = "nuevoToolStripMenuItem";
            this.nuevoToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.nuevoToolStripMenuItem.Text = "Nuevo";
            // 
            // abrirToolStripMenuItem
            // 
            this.abrirToolStripMenuItem.Name = "abrirToolStripMenuItem";
            this.abrirToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.abrirToolStripMenuItem.Text = "Abrir";
            // 
            // guardarToolStripMenuItem
            // 
            this.guardarToolStripMenuItem.Name = "guardarToolStripMenuItem";
            this.guardarToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.guardarToolStripMenuItem.Text = "Guardar";
            // 
            // guardarComoToolStripMenuItem
            // 
            this.guardarComoToolStripMenuItem.Name = "guardarComoToolStripMenuItem";
            this.guardarComoToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.guardarComoToolStripMenuItem.Text = "Guardar como";
            // 
            // cerrarToolStripMenuItem
            // 
            this.cerrarToolStripMenuItem.Name = "cerrarToolStripMenuItem";
            this.cerrarToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.cerrarToolStripMenuItem.Text = "Cerrar";
            // 
            // metodoToolStripMenuItem
            // 
            this.metodoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.iSA6150820106ToolStripMenuItem,
            this.uPMToolStripMenuItem,
            this.pDSToolStripMenuItem});
            this.metodoToolStripMenuItem.Name = "metodoToolStripMenuItem";
            this.metodoToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.metodoToolStripMenuItem.Text = "Metodo";
            // 
            // iSA6150820106ToolStripMenuItem
            // 
            this.iSA6150820106ToolStripMenuItem.Checked = true;
            this.iSA6150820106ToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.iSA6150820106ToolStripMenuItem.Name = "iSA6150820106ToolStripMenuItem";
            this.iSA6150820106ToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.iSA6150820106ToolStripMenuItem.Text = "ISA 61508-6:2010";
            // 
            // uPMToolStripMenuItem
            // 
            this.uPMToolStripMenuItem.Enabled = false;
            this.uPMToolStripMenuItem.Name = "uPMToolStripMenuItem";
            this.uPMToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.uPMToolStripMenuItem.Text = "UPM";
            // 
            // pDSToolStripMenuItem
            // 
            this.pDSToolStripMenuItem.Enabled = false;
            this.pDSToolStripMenuItem.Name = "pDSToolStripMenuItem";
            this.pDSToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.pDSToolStripMenuItem.Text = "PDS";
            // 
            // reporteToolStripMenuItem
            // 
            this.reporteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.generarReporteToolStripMenuItem});
            this.reporteToolStripMenuItem.Name = "reporteToolStripMenuItem";
            this.reporteToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.reporteToolStripMenuItem.Text = "Reporte";
            // 
            // generarReporteToolStripMenuItem
            // 
            this.generarReporteToolStripMenuItem.Name = "generarReporteToolStripMenuItem";
            this.generarReporteToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.generarReporteToolStripMenuItem.Text = "Generar Reporte";
            // 
            // ayudaToolStripMenuItem
            // 
            this.ayudaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.acercaDeToolStripMenuItem});
            this.ayudaToolStripMenuItem.Name = "ayudaToolStripMenuItem";
            this.ayudaToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.ayudaToolStripMenuItem.Text = "Ayuda";
            // 
            // acercaDeToolStripMenuItem
            // 
            this.acercaDeToolStripMenuItem.Name = "acercaDeToolStripMenuItem";
            this.acercaDeToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.acercaDeToolStripMenuItem.Text = "Acerca de";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(920, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // S3NIS_BETA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(920, 598);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "S3NIS_BETA";
            this.Text = "S3NIS BETA";
            this.Load += new System.EventHandler(this.S3NIS_BETA_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tabControl_ls_sensor.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.Separacion.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.Redundancia.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.Complejidad.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.Evaluacion.ResumeLayout(false);
            this.splitContainer5.Panel1.ResumeLayout(false);
            this.splitContainer5.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).EndInit();
            this.splitContainer5.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.groupBox22.ResumeLayout(false);
            this.groupBox22.PerformLayout();
            this.groupBox23.ResumeLayout(false);
            this.groupBox23.PerformLayout();
            this.Procedimientos.ResumeLayout(false);
            this.splitContainer6.Panel1.ResumeLayout(false);
            this.splitContainer6.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer6)).EndInit();
            this.splitContainer6.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            this.groupBox24.ResumeLayout(false);
            this.groupBox24.PerformLayout();
            this.groupBox25.ResumeLayout(false);
            this.groupBox25.PerformLayout();
            this.groupBox26.ResumeLayout(false);
            this.groupBox26.PerformLayout();
            this.Entrenamiento.ResumeLayout(false);
            this.splitContainer7.Panel1.ResumeLayout(false);
            this.splitContainer7.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer7)).EndInit();
            this.splitContainer7.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.groupBox28.ResumeLayout(false);
            this.groupBox28.PerformLayout();
            this.groupBox29.ResumeLayout(false);
            this.groupBox29.PerformLayout();
            this.Ambiente.ResumeLayout(false);
            this.splitContainer15.Panel1.ResumeLayout(false);
            this.splitContainer15.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer15)).EndInit();
            this.splitContainer15.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.groupBox31.ResumeLayout(false);
            this.groupBox31.PerformLayout();
            this.groupBox32.ResumeLayout(false);
            this.groupBox32.PerformLayout();
            this.groupBox33.ResumeLayout(false);
            this.groupBox33.PerformLayout();
            this.groupBox34.ResumeLayout(false);
            this.groupBox34.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.splitContainer8.Panel1.ResumeLayout(false);
            this.splitContainer8.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer8)).EndInit();
            this.splitContainer8.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.splitContainer9.Panel1.ResumeLayout(false);
            this.splitContainer9.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer9)).EndInit();
            this.splitContainer9.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.groupBox27.ResumeLayout(false);
            this.groupBox27.PerformLayout();
            this.groupBox30.ResumeLayout(false);
            this.groupBox30.PerformLayout();
            this.groupBox35.ResumeLayout(false);
            this.groupBox35.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.splitContainer10.Panel1.ResumeLayout(false);
            this.splitContainer10.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer10)).EndInit();
            this.splitContainer10.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.groupBox39.ResumeLayout(false);
            this.groupBox39.PerformLayout();
            this.groupBox40.ResumeLayout(false);
            this.groupBox40.PerformLayout();
            this.groupBox41.ResumeLayout(false);
            this.groupBox41.PerformLayout();
            this.groupBox42.ResumeLayout(false);
            this.groupBox42.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.splitContainer11.Panel1.ResumeLayout(false);
            this.splitContainer11.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer11)).EndInit();
            this.splitContainer11.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.groupBox44.ResumeLayout(false);
            this.groupBox44.PerformLayout();
            this.groupBox45.ResumeLayout(false);
            this.groupBox45.PerformLayout();
            this.groupBox46.ResumeLayout(false);
            this.groupBox46.PerformLayout();
            this.groupBox47.ResumeLayout(false);
            this.groupBox47.PerformLayout();
            this.groupBox48.ResumeLayout(false);
            this.groupBox48.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.splitContainer12.Panel1.ResumeLayout(false);
            this.splitContainer12.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer12)).EndInit();
            this.splitContainer12.ResumeLayout(false);
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            this.groupBox49.ResumeLayout(false);
            this.groupBox49.PerformLayout();
            this.groupBox50.ResumeLayout(false);
            this.groupBox50.PerformLayout();
            this.groupBox51.ResumeLayout(false);
            this.groupBox51.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.splitContainer13.Panel1.ResumeLayout(false);
            this.splitContainer13.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer13)).EndInit();
            this.splitContainer13.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.groupBox54.ResumeLayout(false);
            this.groupBox54.PerformLayout();
            this.groupBox55.ResumeLayout(false);
            this.groupBox55.PerformLayout();
            this.groupBox56.ResumeLayout(false);
            this.groupBox56.PerformLayout();
            this.groupBox57.ResumeLayout(false);
            this.groupBox57.PerformLayout();
            this.groupBox58.ResumeLayout(false);
            this.groupBox58.PerformLayout();
            this.tabPage8.ResumeLayout(false);
            this.splitContainer14.Panel1.ResumeLayout(false);
            this.splitContainer14.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer14)).EndInit();
            this.splitContainer14.ResumeLayout(false);
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel14.PerformLayout();
            this.groupBox59.ResumeLayout(false);
            this.groupBox59.PerformLayout();
            this.groupBox60.ResumeLayout(false);
            this.groupBox60.PerformLayout();
            this.tabPage9.ResumeLayout(false);
            this.splitContainer16.Panel1.ResumeLayout(false);
            this.splitContainer16.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer16)).EndInit();
            this.splitContainer16.ResumeLayout(false);
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel15.PerformLayout();
            this.groupBox61.ResumeLayout(false);
            this.groupBox61.PerformLayout();
            this.groupBox62.ResumeLayout(false);
            this.groupBox62.PerformLayout();
            this.groupBox63.ResumeLayout(false);
            this.groupBox63.PerformLayout();
            this.groupBox64.ResumeLayout(false);
            this.groupBox64.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem metodoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ayudaToolStripMenuItem;
        private System.Windows.Forms.Button cancelar;
        private System.Windows.Forms.Button aceptar;
        private System.Windows.Forms.ToolStripMenuItem iSA6150820106ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uPMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pDSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reporteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generarReporteToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl_ls_sensor;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.SplitContainer splitContainer8;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.SplitContainer splitContainer9;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.RadioButton sefQ1_3S;
        private System.Windows.Forms.RadioButton sefQ1_3N;
        private System.Windows.Forms.GroupBox groupBox30;
        private System.Windows.Forms.RadioButton sefQ1_2S;
        private System.Windows.Forms.RadioButton sefQ1_2N;
        private System.Windows.Forms.Label labelC_sefQ1_3;
        private System.Windows.Forms.Label labelC_sefQ1_2;
        private System.Windows.Forms.TextBox sefQ1_3C;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.TextBox sefQ1_2C;
        private System.Windows.Forms.Label labelC_sefQ1_1;
        private System.Windows.Forms.TextBox sefQ1_1C;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.GroupBox groupBox35;
        private System.Windows.Forms.RadioButton sefQ1_1S;
        private System.Windows.Forms.RadioButton sefQ1_1N;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.SplitContainer splitContainer10;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.GroupBox groupBox39;
        private System.Windows.Forms.RadioButton sefQ2_4N;
        private System.Windows.Forms.RadioButton sefQ2_4S;
        private System.Windows.Forms.GroupBox groupBox40;
        private System.Windows.Forms.RadioButton sefQ2_3S;
        private System.Windows.Forms.RadioButton sefQ2_3N;
        private System.Windows.Forms.GroupBox groupBox41;
        private System.Windows.Forms.RadioButton sefQ2_2S;
        private System.Windows.Forms.RadioButton sefQ2_2N;
        private System.Windows.Forms.Label labelC_sefQ2_4;
        private System.Windows.Forms.Label labelC_sefQ2_3;
        private System.Windows.Forms.Label labelC_sefQ2_2;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.TextBox sefQ2_4C;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.TextBox sefQ2_3C;
        private System.Windows.Forms.CheckBox checkBox17;
        private System.Windows.Forms.TextBox sefQ2_2C;
        private System.Windows.Forms.Label labelC_sefQ2_1;
        private System.Windows.Forms.TextBox sefQ2_1C;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.GroupBox groupBox42;
        private System.Windows.Forms.RadioButton sefQ2_1S;
        private System.Windows.Forms.RadioButton sefQ2_1N;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.SplitContainer splitContainer11;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.GroupBox groupBox44;
        private System.Windows.Forms.RadioButton sefQ3_5N;
        private System.Windows.Forms.RadioButton sefQ3_5S;
        private System.Windows.Forms.GroupBox groupBox45;
        private System.Windows.Forms.RadioButton sefQ3_4N;
        private System.Windows.Forms.RadioButton sefQ3_4S;
        private System.Windows.Forms.GroupBox groupBox46;
        private System.Windows.Forms.RadioButton sefQ3_3S;
        private System.Windows.Forms.RadioButton sefQ3_3N;
        private System.Windows.Forms.GroupBox groupBox47;
        private System.Windows.Forms.RadioButton sefQ3_2S;
        private System.Windows.Forms.RadioButton sefQ3_2N;
        private System.Windows.Forms.Label labelC_sefQ3_5;
        private System.Windows.Forms.Label labelC_sefQ3_4;
        private System.Windows.Forms.Label labelC_sefQ3_3;
        private System.Windows.Forms.Label labelC_sefQ3_2;
        private System.Windows.Forms.TextBox sefQ3_5C;
        private System.Windows.Forms.CheckBox checkBox18;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.TextBox sefQ3_4C;
        private System.Windows.Forms.CheckBox checkBox19;
        private System.Windows.Forms.TextBox sefQ3_3C;
        private System.Windows.Forms.CheckBox checkBox20;
        private System.Windows.Forms.TextBox sefQ3_2C;
        private System.Windows.Forms.Label labelC_sefQ3_1;
        private System.Windows.Forms.TextBox sefQ3_1C;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.GroupBox groupBox48;
        private System.Windows.Forms.RadioButton sefQ3_1S;
        private System.Windows.Forms.RadioButton sefQ3_1N;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.SplitContainer splitContainer12;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.GroupBox groupBox49;
        private System.Windows.Forms.RadioButton sefQ4_3S;
        private System.Windows.Forms.RadioButton sefQ4_3N;
        private System.Windows.Forms.GroupBox groupBox50;
        private System.Windows.Forms.RadioButton sefQ4_2S;
        private System.Windows.Forms.RadioButton sefQ4_2N;
        private System.Windows.Forms.Label labelC_sefQ4_3;
        private System.Windows.Forms.Label labelC_sefQ4_2;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.TextBox sefQ4_3C;
        private System.Windows.Forms.CheckBox checkBox21;
        private System.Windows.Forms.TextBox sefQ4_2C;
        private System.Windows.Forms.Label labelC_sefQ4_1;
        private System.Windows.Forms.TextBox sefQ4_1C;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.GroupBox groupBox51;
        private System.Windows.Forms.RadioButton sefQ4_1S;
        private System.Windows.Forms.RadioButton sefQ4_1N;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.SplitContainer splitContainer13;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.GroupBox groupBox54;
        private System.Windows.Forms.RadioButton sefQ5_5N;
        private System.Windows.Forms.RadioButton sefQ5_5S;
        private System.Windows.Forms.GroupBox groupBox55;
        private System.Windows.Forms.RadioButton sefQ5_4N;
        private System.Windows.Forms.RadioButton sefQ5_4S;
        private System.Windows.Forms.GroupBox groupBox56;
        private System.Windows.Forms.RadioButton sefQ5_3S;
        private System.Windows.Forms.RadioButton sefQ5_3N;
        private System.Windows.Forms.GroupBox groupBox57;
        private System.Windows.Forms.RadioButton sefQ5_2S;
        private System.Windows.Forms.RadioButton sefQ5_2N;
        private System.Windows.Forms.Label labelC_sefQ5_5;
        private System.Windows.Forms.Label labelC_sefQ5_4;
        private System.Windows.Forms.Label labelC_sefQ5_3;
        private System.Windows.Forms.Label labelC_sefQ5_2;
        private System.Windows.Forms.TextBox sefQ5_5C;
        private System.Windows.Forms.CheckBox checkBox22;
        private System.Windows.Forms.Label label158;
        private System.Windows.Forms.TextBox sefQ5_4C;
        private System.Windows.Forms.CheckBox checkBox23;
        private System.Windows.Forms.TextBox sefQ5_3C;
        private System.Windows.Forms.CheckBox checkBox24;
        private System.Windows.Forms.TextBox sefQ5_2C;
        private System.Windows.Forms.Label labelC_sefQ5_1;
        private System.Windows.Forms.TextBox sefQ5_1C;
        private System.Windows.Forms.Label label160;
        private System.Windows.Forms.Label label161;
        private System.Windows.Forms.Label label162;
        private System.Windows.Forms.Label label163;
        private System.Windows.Forms.Label label164;
        private System.Windows.Forms.Label label165;
        private System.Windows.Forms.Label label166;
        private System.Windows.Forms.Label label167;
        private System.Windows.Forms.Label label168;
        private System.Windows.Forms.Label label169;
        private System.Windows.Forms.GroupBox groupBox58;
        private System.Windows.Forms.RadioButton sefQ5_1S;
        private System.Windows.Forms.RadioButton sefQ5_1N;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.SplitContainer splitContainer14;
        private System.Windows.Forms.Label label170;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.Label label171;
        private System.Windows.Forms.GroupBox groupBox59;
        private System.Windows.Forms.RadioButton sefQ6_2S;
        private System.Windows.Forms.RadioButton sefQ6_2N;
        private System.Windows.Forms.Label labelC_sefQ6_2;
        private System.Windows.Forms.TextBox sefQ6_2C;
        private System.Windows.Forms.Label labelC_sefQ6_1;
        private System.Windows.Forms.TextBox sefQ6_1C;
        private System.Windows.Forms.Label label174;
        private System.Windows.Forms.Label label175;
        private System.Windows.Forms.Label label176;
        private System.Windows.Forms.Label label177;
        private System.Windows.Forms.GroupBox groupBox60;
        private System.Windows.Forms.RadioButton sefQ6_1S;
        private System.Windows.Forms.RadioButton sefQ6_1N;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.SplitContainer splitContainer16;
        private System.Windows.Forms.Label label178;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private System.Windows.Forms.Label label179;
        private System.Windows.Forms.GroupBox groupBox61;
        private System.Windows.Forms.RadioButton sefQ7_4N;
        private System.Windows.Forms.RadioButton sefQ7_4S;
        private System.Windows.Forms.GroupBox groupBox62;
        private System.Windows.Forms.RadioButton sefQ7_3S;
        private System.Windows.Forms.RadioButton sefQ7_3N;
        private System.Windows.Forms.GroupBox groupBox63;
        private System.Windows.Forms.RadioButton sefQ7_2S;
        private System.Windows.Forms.RadioButton sefQ7_2N;
        private System.Windows.Forms.Label labelC_sefQ7_4;
        private System.Windows.Forms.Label labelC_sefQ7_3;
        private System.Windows.Forms.Label labelC_sefQ7_2;
        private System.Windows.Forms.Label label183;
        private System.Windows.Forms.TextBox sefQ7_4C;
        private System.Windows.Forms.CheckBox checkBox25;
        private System.Windows.Forms.TextBox sefQ7_3C;
        private System.Windows.Forms.CheckBox checkBox26;
        private System.Windows.Forms.TextBox sefQ7_2C;
        private System.Windows.Forms.Label labelC_sefQ7_1;
        private System.Windows.Forms.TextBox sefQ7_1C;
        private System.Windows.Forms.Label label185;
        private System.Windows.Forms.Label label186;
        private System.Windows.Forms.Label label187;
        private System.Windows.Forms.Label label188;
        private System.Windows.Forms.Label label189;
        private System.Windows.Forms.Label label190;
        private System.Windows.Forms.Label label191;
        private System.Windows.Forms.Label label192;
        private System.Windows.Forms.GroupBox groupBox64;
        private System.Windows.Forms.RadioButton sefQ7_1S;
        private System.Windows.Forms.RadioButton sefQ7_1N;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.ToolStripMenuItem nuevoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem abrirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem guardarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem guardarComoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cerrarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem acercaDeToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage Separacion;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton lsQ1_3S;
        private System.Windows.Forms.RadioButton lsQ1_3N;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton lsQ1_2S;
        private System.Windows.Forms.RadioButton lsQ1_2N;
        private System.Windows.Forms.Label labelC_lsQ1_3;
        private System.Windows.Forms.Label labelC_lsQ1_2;
        private System.Windows.Forms.TextBox lsQ1_3C;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.TextBox lsQ1_2C;
        private System.Windows.Forms.Label labelC_lsQ1_1;
        private System.Windows.Forms.TextBox lsQ1_1C;
        private System.Windows.Forms.Label labelN_Q1_3;
        private System.Windows.Forms.Label label_Q1_2;
        private System.Windows.Forms.Label labelN_Q1_2;
        private System.Windows.Forms.Label label_Q1_1;
        private System.Windows.Forms.Label labelN_Q1_1;
        private System.Windows.Forms.Label label_Q1_3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton lsQ1_1S;
        private System.Windows.Forms.RadioButton lsQ1_1N;
        private System.Windows.Forms.TabPage Redundancia;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox lsQ2_7C;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.RadioButton lsQ2_7N;
        private System.Windows.Forms.RadioButton lsQ2_7S;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label labelC_lsQ2_7;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox lsQ2_6C;
        private System.Windows.Forms.Label labelC_lsQ2_6;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.RadioButton lsQ2_6N;
        private System.Windows.Forms.RadioButton lsQ2_6S;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.RadioButton lsQ2_5N;
        private System.Windows.Forms.RadioButton lsQ2_5S;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.RadioButton lsQ2_4N;
        private System.Windows.Forms.RadioButton lsQ2_4S;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.RadioButton lsQ2_3S;
        private System.Windows.Forms.RadioButton lsQ2_3N;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.RadioButton lsQ2_2S;
        private System.Windows.Forms.RadioButton lsQ2_2N;
        private System.Windows.Forms.Label labelC_lsQ2_5;
        private System.Windows.Forms.Label labelC_lsQ2_4;
        private System.Windows.Forms.Label labelC_lsQ2_3;
        private System.Windows.Forms.Label labelC_lsQ2_2;
        private System.Windows.Forms.TextBox lsQ2_5C;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox lsQ2_4C;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.TextBox lsQ2_3C;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.TextBox lsQ2_2C;
        private System.Windows.Forms.Label labelC_lsQ2_1;
        private System.Windows.Forms.TextBox lsQ2_1C;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.RadioButton lsQ2_1S;
        private System.Windows.Forms.RadioButton lsQ2_1N;
        private System.Windows.Forms.TabPage Complejidad;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TextBox lsQ3_6C;
        private System.Windows.Forms.Label labelC_lsQ3_6;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.RadioButton lsQ3_6N;
        private System.Windows.Forms.RadioButton lsQ3_6S;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.RadioButton lsQ3_5N;
        private System.Windows.Forms.RadioButton lsQ3_5S;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.RadioButton lsQ3_4N;
        private System.Windows.Forms.RadioButton lsQ3_4S;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.RadioButton lsQ3_3S;
        private System.Windows.Forms.RadioButton lsQ3_3N;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.RadioButton lsQ3_2S;
        private System.Windows.Forms.RadioButton lsQ3_2N;
        private System.Windows.Forms.Label labelC_lsQ3_5;
        private System.Windows.Forms.Label labelC_lsQ3_4;
        private System.Windows.Forms.Label labelC_lsQ3_3;
        private System.Windows.Forms.Label labelC_lsQ3_2;
        private System.Windows.Forms.TextBox lsQ3_5C;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox lsQ3_4C;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.TextBox lsQ3_3C;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.TextBox lsQ3_2C;
        private System.Windows.Forms.Label labelC_lsQ3_1;
        private System.Windows.Forms.TextBox lsQ3_1C;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.RadioButton lsQ3_1S;
        private System.Windows.Forms.RadioButton lsQ3_1N;
        private System.Windows.Forms.TabPage Evaluacion;
        private System.Windows.Forms.SplitContainer splitContainer5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.RadioButton lsQ4_3S;
        private System.Windows.Forms.RadioButton lsQ4_3N;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.RadioButton lsQ4_2S;
        private System.Windows.Forms.RadioButton lsQ4_2N;
        private System.Windows.Forms.Label labelC_lsQ4_3;
        private System.Windows.Forms.Label labelC_lsQ4_2;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox lsQ4_3C;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.TextBox lsQ4_2C;
        private System.Windows.Forms.Label labelC_lsQ4_1;
        private System.Windows.Forms.TextBox lsQ4_1C;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.RadioButton lsQ4_1S;
        private System.Windows.Forms.RadioButton lsQ4_1N;
        private System.Windows.Forms.TabPage Procedimientos;
        private System.Windows.Forms.SplitContainer splitContainer6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TextBox lsQ5_7C;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton lsQ5_7N;
        private System.Windows.Forms.RadioButton lsQ5_7S;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelC_lsQ5_7;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox lsQ5_6C;
        private System.Windows.Forms.Label labelC_lsQ5_6;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton lsQ5_6N;
        private System.Windows.Forms.RadioButton lsQ5_6S;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton lsQ5_5N;
        private System.Windows.Forms.RadioButton lsQ5_5S;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.RadioButton lsQ5_4N;
        private System.Windows.Forms.RadioButton lsQ5_4S;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.RadioButton lsQ5_3S;
        private System.Windows.Forms.RadioButton lsQ5_3N;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.RadioButton lsQ5_2S;
        private System.Windows.Forms.RadioButton lsQ5_2N;
        private System.Windows.Forms.Label labelC_lsQ5_5;
        private System.Windows.Forms.Label labelC_lsQ5_4;
        private System.Windows.Forms.Label labelC_lsQ5_3;
        private System.Windows.Forms.Label labelC_lsQ5_2;
        private System.Windows.Forms.TextBox lsQ5_5C;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox lsQ5_4C;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.TextBox lsQ5_3C;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.TextBox lsQ5_2C;
        private System.Windows.Forms.Label labelC_lsQ5_1;
        private System.Windows.Forms.TextBox lsQ5_1C;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.GroupBox groupBox26;
        private System.Windows.Forms.RadioButton lsQ5_1S;
        private System.Windows.Forms.RadioButton lsQ5_1N;
        private System.Windows.Forms.TabPage Entrenamiento;
        private System.Windows.Forms.SplitContainer splitContainer7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.GroupBox groupBox28;
        private System.Windows.Forms.RadioButton lsQ6_2S;
        private System.Windows.Forms.RadioButton lsQ6_2N;
        private System.Windows.Forms.Label labelC_lsQ6_2;
        private System.Windows.Forms.TextBox lsQ6_2C;
        private System.Windows.Forms.Label labelC_lsQ6_1;
        private System.Windows.Forms.TextBox lsQ6_1C;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.GroupBox groupBox29;
        private System.Windows.Forms.RadioButton lsQ6_1S;
        private System.Windows.Forms.RadioButton lsQ6_1N;
        private System.Windows.Forms.TabPage Ambiente;
        private System.Windows.Forms.SplitContainer splitContainer15;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.GroupBox groupBox31;
        private System.Windows.Forms.RadioButton lsQ7_4N;
        private System.Windows.Forms.RadioButton lsQ7_4S;
        private System.Windows.Forms.GroupBox groupBox32;
        private System.Windows.Forms.RadioButton lsQ7_3S;
        private System.Windows.Forms.RadioButton lsQ7_3N;
        private System.Windows.Forms.GroupBox groupBox33;
        private System.Windows.Forms.RadioButton lsQ7_2S;
        private System.Windows.Forms.RadioButton lsQ7_2N;
        private System.Windows.Forms.Label labelC_lsQ7_4;
        private System.Windows.Forms.Label labelC_lsQ7_3;
        private System.Windows.Forms.Label labelC_lsQ7_2;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.TextBox lsQ7_4C;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.TextBox lsQ7_3C;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.TextBox lsQ7_2C;
        private System.Windows.Forms.Label labelC_lsQ7_1;
        private System.Windows.Forms.TextBox lsQ7_1C;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.GroupBox groupBox34;
        private System.Windows.Forms.RadioButton lsQ7_1S;
        private System.Windows.Forms.RadioButton lsQ7_1N;
        private System.Windows.Forms.TabPage Datos;
        private System.Windows.Forms.TabPage Resultados;
    }
}

