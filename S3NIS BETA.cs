﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace S3NIS_BETA
{
    public partial class S3NIS_BETA : Form
    {
        public S3NIS_BETA()
        {
            //Estructura para las respuestas.

            InitializeComponent();
        }

        private void S3NIS_BETA_Load(object sender, EventArgs e)
        {
            
        }

        private void updatevisibleLS1(object sender, EventArgs e)
        {    
            
            //Primera Pregunta
            labelC_lsQ1_1.Visible = lsQ1_1S.Checked;
            lsQ1_1C.Visible = lsQ1_1S.Checked;
            //Segunda Pregunta
            labelC_lsQ1_2.Visible = lsQ1_2S.Checked;
            lsQ1_2C.Visible = lsQ1_2S.Checked;
            //Tercera Pregunta
            labelC_lsQ1_3.Visible = lsQ1_3S.Checked;
            lsQ1_3C.Visible = lsQ1_3S.Checked;
        }
        private void updatevisibleLS2(object sender, EventArgs e)
        {

            //Primera Pregunta
            labelC_lsQ2_1.Visible = lsQ2_1S.Checked;
            lsQ2_1C.Visible = lsQ2_1S.Checked;
            //Segunda Pregunta
            labelC_lsQ2_2.Visible = lsQ2_2S.Checked;
            lsQ2_2C.Visible = lsQ2_2S.Checked;
            //Tercera Pregunta
            labelC_lsQ2_3.Visible = lsQ2_3S.Checked;
            lsQ2_3C.Visible = lsQ2_3S.Checked;
            //Cuarta Pregunta
            labelC_lsQ2_4.Visible = lsQ2_4S.Checked;
            lsQ2_4C.Visible = lsQ2_4S.Checked;
            //Quinta Pregunta
            labelC_lsQ2_5.Visible = lsQ2_5S.Checked;
            lsQ2_5C.Visible = lsQ2_5S.Checked;
            //Sexta Pregunta
            labelC_lsQ2_6.Visible = lsQ2_6S.Checked;
            lsQ2_6C.Visible = lsQ2_6S.Checked;
            //Septima Pregunta
            labelC_lsQ2_7.Visible = lsQ2_7S.Checked;
            lsQ2_7C.Visible = lsQ2_7S.Checked;
        }
        private void updatevisibleLS3(object sender, EventArgs e)
        {

            //Primera Pregunta
            labelC_lsQ3_1.Visible = lsQ3_1S.Checked;
            lsQ3_1C.Visible = lsQ3_1S.Checked;
            //Segunda Pregunta
            labelC_lsQ3_2.Visible = lsQ3_2S.Checked;
            lsQ3_2C.Visible = lsQ3_2S.Checked;
            //Tercera Pregunta
            labelC_lsQ3_3.Visible = lsQ3_3S.Checked;
            lsQ3_3C.Visible = lsQ3_3S.Checked;
            //Cuarta Pregunta
            labelC_lsQ3_4.Visible = lsQ3_4S.Checked;
            lsQ3_4C.Visible = lsQ3_4S.Checked;
            //Quinta Pregunta
            labelC_lsQ3_5.Visible = lsQ3_5S.Checked;
            lsQ3_5C.Visible = lsQ3_5S.Checked;
            //Sexta Pregunta
            labelC_lsQ3_6.Visible = lsQ3_6S.Checked;
            lsQ3_6C.Visible = lsQ3_6S.Checked;
        }

        private void updatevisibleLS4(object sender, EventArgs e)
        {

            //Primera Pregunta
            labelC_lsQ4_1.Visible = lsQ4_1S.Checked;
            lsQ4_1C.Visible = lsQ4_1S.Checked;
            //Segunda Pregunta
            labelC_lsQ4_2.Visible = lsQ4_2S.Checked;
            lsQ4_2C.Visible = lsQ4_2S.Checked;
            //Tercera Pregunta
            labelC_lsQ4_3.Visible = lsQ4_3S.Checked;
            lsQ4_3C.Visible = lsQ4_3S.Checked;
            ///mensae de prueba para revertir
            ///otro mensje de prueba
        }

        private void updatevisibleLS5(object sender, EventArgs e)
        {

            //Primera Pregunta
            labelC_lsQ5_1.Visible = lsQ5_1S.Checked;
            lsQ5_1C.Visible = lsQ5_1S.Checked;
            //Segunda Pregunta
            labelC_lsQ5_2.Visible = lsQ5_2S.Checked;
            lsQ5_2C.Visible = lsQ5_2S.Checked;
            //Tercera Pregunta
            labelC_lsQ5_3.Visible = lsQ5_3S.Checked;
            lsQ5_3C.Visible = lsQ5_3S.Checked;
            //Cuarta Pregunta
            labelC_lsQ5_4.Visible = lsQ5_4S.Checked;
            lsQ5_4C.Visible = lsQ5_4S.Checked;
            //Quinta Pregunta
            labelC_lsQ5_5.Visible = lsQ5_5S.Checked;
            lsQ5_5C.Visible = lsQ5_5S.Checked;
            //Sexta Pregunta
            labelC_lsQ5_6.Visible = lsQ5_6S.Checked;
            lsQ5_6C.Visible = lsQ5_6S.Checked;
            //Septima Pregunta
            labelC_lsQ5_7.Visible = lsQ5_7S.Checked;
            lsQ5_7C.Visible = lsQ5_7S.Checked;
        }

        private void updatevisibleLS6(object sender, EventArgs e)
        {

            //Primera Pregunta
            labelC_lsQ6_1.Visible = lsQ6_1S.Checked;
            lsQ6_1C.Visible = lsQ6_1S.Checked;
            //Segunda Pregunta
            labelC_lsQ6_2.Visible = lsQ6_2S.Checked;
            lsQ6_2C.Visible = lsQ6_2S.Checked;
        }

        private void updatevisibleLS7(object sender, EventArgs e)
        {

            //Primera Pregunta
            labelC_lsQ7_1.Visible = lsQ7_1S.Checked;
            lsQ7_1C.Visible = lsQ7_1S.Checked;
            //Segunda Pregunta
            labelC_lsQ7_2.Visible = lsQ7_2S.Checked;
            lsQ7_2C.Visible = lsQ7_2S.Checked;
            //Tercera Pregunta
            labelC_lsQ7_3.Visible = lsQ7_3S.Checked;
            lsQ7_3C.Visible = lsQ7_3S.Checked;
            //Cuarta Pregunta
            labelC_lsQ7_4.Visible = lsQ7_4S.Checked;
            lsQ7_4C.Visible = lsQ7_4S.Checked;
            
        }

        private void updatevisibleSEF1(object sender, EventArgs e)
        {

            //Primera Pregunta
            labelC_sefQ1_1.Visible = sefQ1_1S.Checked;
            sefQ1_1C.Visible = sefQ1_1S.Checked;
            //Segunda Pregunta
            labelC_sefQ1_2.Visible = sefQ1_2S.Checked;
            sefQ1_2C.Visible = sefQ1_2S.Checked;
            //Tercera Pregunta
            labelC_sefQ1_3.Visible = sefQ1_3S.Checked;
            sefQ1_3C.Visible = sefQ1_3S.Checked;
        }
        private void updatevisibleSEF2(object sender, EventArgs e)
        {

            //Primera Pregunta
            labelC_sefQ2_1.Visible = sefQ2_1S.Checked;
            sefQ2_1C.Visible = sefQ2_1S.Checked;
            //Segunda Pregunta
            labelC_sefQ2_2.Visible = sefQ2_2S.Checked;
            sefQ2_2C.Visible = sefQ2_2S.Checked;
            //Tercera Pregunta
            labelC_sefQ2_3.Visible = sefQ2_3S.Checked;
            sefQ2_3C.Visible = sefQ2_3S.Checked;
            //Cuarta Pregunta
            labelC_sefQ2_4.Visible = sefQ2_4S.Checked;
            sefQ2_4C.Visible = sefQ2_4S.Checked;
            
        }
        private void updatevisibleSEF3(object sender, EventArgs e)
        {

            //Primera Pregunta
            labelC_sefQ3_1.Visible = sefQ3_1S.Checked;
            sefQ3_1C.Visible = sefQ3_1S.Checked;
            //Segunda Pregunta
            labelC_sefQ3_2.Visible = sefQ3_2S.Checked;
            sefQ3_2C.Visible = sefQ3_2S.Checked;
            //Tercera Pregunta
            labelC_sefQ3_3.Visible = sefQ3_3S.Checked;
            sefQ3_3C.Visible = sefQ3_3S.Checked;
            //Cuarta Pregunta
            labelC_sefQ3_4.Visible = sefQ3_4S.Checked;
            sefQ3_4C.Visible = sefQ3_4S.Checked;
            //Quinta Pregunta
            labelC_sefQ3_5.Visible = sefQ3_5S.Checked;
            sefQ3_5C.Visible = sefQ3_5S.Checked;
           
        }

        private void updatevisibleSEF4(object sender, EventArgs e)
        {

            //Primera Pregunta
            labelC_sefQ4_1.Visible = sefQ4_1S.Checked;
            sefQ4_1C.Visible = sefQ4_1S.Checked;
            //Segunda Pregunta
            labelC_sefQ4_2.Visible = sefQ4_2S.Checked;
            sefQ4_2C.Visible = sefQ4_2S.Checked;
            //Tercera Pregunta
            labelC_sefQ4_3.Visible = sefQ4_3S.Checked;
            sefQ4_3C.Visible = sefQ4_3S.Checked;
        }

        private void updatevisibleSEF5(object sender, EventArgs e)
        {

            //Primera Pregunta
            labelC_sefQ5_1.Visible = sefQ5_1S.Checked;
            sefQ5_1C.Visible = sefQ5_1S.Checked;
            //Segunda Pregunta
            labelC_sefQ5_2.Visible = sefQ5_2S.Checked;
            sefQ5_2C.Visible = sefQ5_2S.Checked;
            //Tercera Pregunta
            labelC_sefQ5_3.Visible = sefQ5_3S.Checked;
            sefQ5_3C.Visible = sefQ5_3S.Checked;
            //Cuarta Pregunta
            labelC_sefQ5_4.Visible = sefQ5_4S.Checked;
            sefQ5_4C.Visible = sefQ5_4S.Checked;
            //Quinta Pregunta
            labelC_sefQ5_5.Visible = sefQ5_5S.Checked;
            sefQ5_5C.Visible = sefQ5_5S.Checked;
           
        }

        private void updatevisibleSEF6(object sender, EventArgs e)
        {

            //Primera Pregunta
            labelC_sefQ6_1.Visible = sefQ6_1S.Checked;
            sefQ6_1C.Visible = sefQ6_1S.Checked;
            //Segunda Pregunta
            labelC_sefQ6_2.Visible = sefQ6_2S.Checked;
            sefQ6_2C.Visible = sefQ6_2S.Checked;
        }

        private void updatevisibleSEF7(object sender, EventArgs e)
        {

            //Primera Pregunta
            labelC_sefQ7_1.Visible = sefQ7_1S.Checked;
            sefQ7_1C.Visible = sefQ7_1S.Checked;
            //Segunda Pregunta
            labelC_sefQ7_2.Visible = sefQ7_2S.Checked;
            sefQ7_2C.Visible = sefQ7_2S.Checked;
            //Tercera Pregunta
            labelC_sefQ7_3.Visible = sefQ7_3S.Checked;
            sefQ7_3C.Visible = sefQ7_3S.Checked;
            //Cuarta Pregunta
            labelC_sefQ7_4.Visible = sefQ7_4S.Checked;
            sefQ7_4C.Visible = sefQ7_4S.Checked;

        }

        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {

        }
    }
    public class Beta
    {
        public string arq; //Arquitectura del componente (1oo1, 1oo2, 1oo3, 2oo2, 2oo3, etc) 
        public double ti;  //intervalo de pruebas (hr)
        public double di;  //intervalo de diagnostivo (hr)
        public double beta;  //Factor de causa comun
        public double sd;    //Tiempo de parada requerido para restaurar operaciones
        public double ldf;   // Probabilidad de fallas sistematicas (ISTR840002:2002)
        public bool separado;  // Indica que se consideran otros componentes en serie como accesorios, etc.
        public Beta()
        {

        }
    }



}